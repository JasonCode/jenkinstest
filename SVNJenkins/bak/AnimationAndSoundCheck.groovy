@Library(['SwordGamePipelineLibrary', 'checkLib']) _
import java.text.*
import java.util.Date
def GTimestamp

properties([
  parameters([
        choice(choices: ['Artist', 'Master', 'Release'], description: '检查版本', name: 'CHECKER_BRANCH'),
        string(defaultValue: 'fukai@kingsoft.com lidi@kingsoft.com huyi2@kingsoft.com luohuan@kingsoft.com wuqianqian2@kingsoft.com zhaofangyu@kingsoft.com wangshiyu@kingsoft.com LUANCHUNYANG@kingsoft.com',description:'邮件通知的收件人',name: 'MAIL_RECLPITNTS',trim: true),
        booleanParam(defaultValue: true, description: '是否POST Error 数据到服务器', name: 'POST_DATA'),
        string(defaultValue: 'Checkers',description:'post data 到服务器时的一级分类',name:'POST_DATA_TABLE_NAME',trim:true),
        string(defaultValue: 'AnimationAndSound',description:'post data 到服务器时的二级分类',name:'POST_DATA_NAME',trim:true),
    ])
])

pipeline {
  options{
    timeout(time: 6, unit: 'HOURS')
  }

  agent {
    node {
      label 'PC_AutoTest'
    }
  }
  stages {
    stage('Update AutoTest Tools'){
      steps{
        script{
          checkerLib.UpdateAutoTestTools()
        }
      }
    }
    stage('Svn Update') {
      steps {
        parallel(
          UpdateArtist:{
            script{
              checkerLib.SvnUpdate("${env.ARTIST_ROOT_PATH}")
            }
          },
          UpdateBranchContent:{
            script{
              if(params.CHECKER_BRANCH == "Master")
              {
                checkerLib.SvnUpdate("${env.MASTERBUILD_ROOT_PATH}/Content")
              }
              else if(params.CHECKER_BRANCH == "Release")
              {
                checkerLib.SvnUpdate("${env.RELEASEBUILD_ROOT_PATH}/Content")
              }
            }
          }
        )
      }
    }
    stage('Make Checker Env') {
      steps {
        script{
          Date currentBuildTime = new Date(currentBuild.startTimeInMillis)
          SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
          GTimestamp = dateFormatter.format(currentBuildTime)

          def szBranchPath = env.MASTERBUILD_ROOT_PATH
          if(params.CHECKER_BRANCH == "Release")
          {
            szBranchPath = env.RELEASEBUILD_ROOT_PATH
          }
          checkerLib.MakeCheckerEnv("${szBranchPath}")
        }
      }
    }
    // stage('Check Wwise') {
    //   steps {
    //     script{
    //       def szBranchPath = env.MASTERBUILD_ROOT_PATH
    //       if(params.CHECKER_BRANCH == "Release")
    //       {
    //         szBranchPath = env.RELEASEBUILD_ROOT_PATH
    //       }
          
    //       powershell (script: """
    //       \$process = (start -FilePath ${env.WORKSPACE}/AutoTest/Eunomia/Eunomia.exe "${env.ARTIST_ROOT_PATH}/Engine/Binaries/Win64/UE4Editor.exe ${szBranchPath}/SwordGame.uproject -featureleveles31 -game -LoginUrlPC=http://120.92.158.166:8101 -AutoTest=PlayWwiseEvent -AutoTestServer=稳定版本体验服 -WARNINGSASERRORS -stdout -culture=en" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "${env.WORKSPACE}/PlayWwiseEvent.txt")

    //       Get-Content -encoding UTF8 ${env.WORKSPACE}/PlayWwiseEvent.txt

    //       \$strErrorCount = \$process.ExitCode.ToString()
    //       "PlayWwiseEvent | Error Count : \$strErrorCount" | Out-File ${env.WORKSPACE}/errorcountsave.txt

    //       if(\$process.ExitCode -ne 0)
    //       {
    //         exit 1
    //       }
    //       else
    //       {
    //         exit 0
    //       } 
    //       """, returnStatus: true, encoding: 'UTF-8')
    //     }
    //   }
    // }

    stage('Check Animation') {
      steps {
        script{
          def szBranchPath = env.MASTERBUILD_ROOT_PATH
          if(params.CHECKER_BRANCH == "Release")
          {
            szBranchPath = env.RELEASEBUILD_ROOT_PATH
          }

          powershell (script: """
          \$process = (start -FilePath ${env.WORKSPACE}/AutoTest/Eunomia/Eunomia.exe "${env.ARTIST_ROOT_PATH}/Engine/Binaries/Win64/UE4Editor.exe ${szBranchPath}/SwordGame.uproject -featureleveles31 -game -LoginUrlPC=http://120.92.158.166:8101 -AutoTest=Animation -AutoTestServer=稳定版本体验服 -WARNINGSASERRORS -stdout -culture=en -buildmachine" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "${env.WORKSPACE}/CheckAnimation.txt")

          Get-Content -encoding UTF8 ${env:WORKSPACE}/CheckAnimation.txt

          \$strErrorCount = \$process.ExitCode.ToString()
          "Animation | Error Count : \$strErrorCount" | Out-File -Append ${env.WORKSPACE}/errorcountsave.txt
          
          if(\$process.ExitCode -ne 0)
          {
            exit 1
          }
          else
          {
            exit 0
          }
          """, returnStatus: true, encoding: 'UTF-8')
        }
      }
    }

    stage('Check Sound') {
      steps {
        script{
          def szBranchPath = env.MASTERBUILD_ROOT_PATH
          if(params.CHECKER_BRANCH == "Release")
          {
            szBranchPath = env.RELEASEBUILD_ROOT_PATH
          }        
          powershell (script: """
          \$process = (start -FilePath ${env.WORKSPACE}/AutoTest/Eunomia/Eunomia.exe "${env.ARTIST_ROOT_PATH}/Engine/Binaries/Win64/UE4Editor.exe ${szBranchPath}/SwordGame.uproject -featureleveles31 -game -LoginUrlPC=http://120.92.158.166:8101 -AutoTest=Sound -AutoTestServer=稳定版本体验服 -WARNINGSASERRORS -stdout -culture=en" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "${env.WORKSPACE}/CheckSound.txt")

          Get-Content -encoding UTF8 ${env.WORKSPACE}/CheckSound.txt

          \$strErrorCount = \$process.ExitCode.ToString()
          "Sound | Error Count : \$strErrorCount" | Out-File -Append ${env.WORKSPACE}/errorcountsave.txt

          if(\$process.ExitCode -ne 0)
          {
            exit 1
          }
          else
          {
            exit 0
          }
          """, returnStatus: true, encoding: 'UTF-8')
        }
      }
    }

    stage('Upload to FTP'){
      steps {
          script{    
            def szBranchPath = env.MASTERBUILD_ROOT_PATH
            if(params.CHECKER_BRANCH == "Release")
            {
              szBranchPath = env.RELEASEBUILD_ROOT_PATH
            }

            echo "ftpDir : ${env.FTP_PATH}/${szBranchPath}/${GTimestamp}"
            new packageLib().PutToFtp('ShenZhenFTP', "${env.FTP_PATH}/${szBranchPath}/${GTimestamp}", "PlayWwiseEvent.txt", false, false)
            new packageLib().PutToFtp('ShenZhenFTP', "${env.FTP_PATH}/${szBranchPath}/${GTimestamp}", "CheckSound.txt", false, false)
            new packageLib().PutToFtp('ShenZhenFTP', "${env.FTP_PATH}/${szBranchPath}/${GTimestamp}", "CheckAnimation.txt", false, false)
            dir("${szBranchPath}/Content/WwiseAudio/Windows"){
              new packageLib().PutToFtp('ShenZhenFTP', "${env.FTP_PATH}/${szBranchPath}/${GTimestamp}", "PlayWwiseEvent.prof", false, false)
            }
          }
      }
    }
    stage('Post Data') {
      steps
      {
        script
        {
          checkerLib.PostMetabaseinfoData()
        }
      }
    }
  }
  post('Send a message')
  {
    always {
      script {
        checkerLib.SendMessage(params.CHECKER_BRANCH)
      }
    }
  }
  environment {
    SVN_CREDENTIALS = credentials('092907a7-dcbf-425b-bc2d-c61715b73536')
    FTP_CREDENTIALS = credentials('3a51894f-d6f5-43a6-85be-e23daacc197b')
    FTP_PATH = "LogCollect/AnimationAndSoundCheck"
  }
}