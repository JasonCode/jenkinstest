properties([
parameters([
  choice(choices: ['PC_AutoTest', 'PC_Build2'], description: '', name: 'NODE'),
  string(defaultValue: '',description:'上传包FTP地址',name :'PackageFtpPath',trim:true),
  string(defaultValue: '',description:'更新包FTP地址',name :'PatchFtpPath',trim:true),
  choice(choices: ['Android','IOS','Windows'], description: '', name: 'Platform')
 ])
])     

def GPatchName = ''
def GPackageName = ''
def GPatchSize = ''
def GPackageSize = ''
def GPatchMD5 = ''
def GPackageMD5 = ''
def buildResult = ''
    node("${params.NODE}") {
        stage("Download"){
            def PatchVersion = ''
            PatchVersion = GetLongVersion()
            //先清理下，保证环境干净
            Cleanup()
            sh "curl ${PatchFtpPath} -o 'Patch_${params.Platform}_${PatchVersion}.zip' "
            if (PackageFtpPath != ''){
                sh "curl -O ${PackageFtpPath}"
            }
        }

        stage('MD5Sum'){
            FileList = sh(returnStdout: true, script: "ls")
            FileList = FileList.split('\n')

            for (int i=0; i < FileList.size(); ++i){
                if(FileList[i] =~ ".zip"){
                    GPatchName = FileList[i]
                    patchMD5List = sh(returnStdout: true, script: "md5sum ${GPatchName}").tokenize(' ')
                    GPatchMD5 = patchMD5List[0]
                    patchSizeList = sh(returnStdout: true, script: "du -b ${GPatchName}").tokenize('\t')
                    GPatchSize = patchSizeList[0]
                }else{
                    GPackageName = FileList[i]
                    packageMD5List = sh(returnStdout: true, script: "md5sum ${GPackageName}").tokenize(' ')
                    GPackageMD5 = packageMD5List[0]
                    packageSizeList = sh(returnStdout: true, script: "du -b ${GPackageName}").tokenize('\t')
                    GPackageSize = packageSizeList[0]
                }
            }
        }


        stage('Upload Tencent FTP'){
            ftpPublisher alwaysPublishFromMaster: false, continueOnError: false, failOnError: false, publishers: [[configName: 'TencentFTP', transfers: [[asciiMode: false, cleanRemote: false, excludes: '', flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '/client/Patch', remoteDirectorySDF: false, removePrefix: '', sourceFiles: "${GPatchName}"]], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false]]
            if ( PackageFtpPath != ''){
                ftpPublisher alwaysPublishFromMaster: false, continueOnError: false, failOnError: false, publishers: [[configName: 'TencentFTP', transfers: [[asciiMode: false, cleanRemote: false, excludes: '', flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '/client/Release', remoteDirectorySDF: false, removePrefix: '', sourceFiles: "${GPackageName}"]], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false]]
            }
        }


        stage("Cleanup"){
            //删除文件
            Cleanup()
        }


        stage("Post Upload"){
            if (buildResult == 'FAILURE'){
                script{
                    //失败了删除文件
                    Cleanup()
                    def body= """
                    {
                        "title": "${env.JOB_NAME}版本上传失败#${env.BUILD_NUMBER}",
                        "text" : "结果：失败\\n<a href=\\\"${env.BUILD_URL}/console\\\">jenkins log 连接</a>\\n${GPatchName}\\n${GPackageName}"
                    }
                    """
                    echo body
                    httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: "${env.LARK_CUSTOMBOT_URL_JIANGEXING}"
                }
                script{
                    emailext(
                        subject: "${env.JOB_NAME}版本上传失败#${env.BUILD_NUMBER}",
                        recipientProviders: [requestor()],
                        to: "${params.MAIL_NAME}",
                        body: """
                        <p>Job Name: ${env.JOB_NAME}</p> 
                        <p><a href="${env.BUILD_URL}console">jenkins log 连接</a></p>
                        <p><font color="#ff0000">结果：失败</font></p>
                        """,
                    )
                }
            }else{
                script {
                    body= """
                    {
                        "title": " ${env.JOB_NAME}版本上传成功#${env.BUILD_NUMBER}",   
                        "text" : "结果：成功\\nPatch名:${GPatchName}\\nPatchMD5:${GPatchMD5}\\nPatch文件大小:${GPatchSize}\\n包名:${GPackageName}\\n安装包大小:${GPackageSize}字节\\n安装包MD5:${GPackageMD5}\\n<a href=\\\"${env.BUILD_URL}/console\\\">jenkins log 连接</a>"
                    }
                    """
                    if (PackageFtpPath == ''){
                        body= """
                        {
                            "title": " ${env.JOB_NAME}版本上传成功#${env.BUILD_NUMBER}",   
                            "text" : "结果：成功\\nPatch名:${GPatchName}\\nPatchMD5:${GPatchMD5}\\nPatch文件大小:${GPatchSize}字节\\n<a href=\\\"${env.BUILD_URL}/console\\\">jenkins log 连接</a>"
                        }
                        """
                    }
                    println body
                    httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: "${env.LARK_CUSTOMBOT_URL_JIANGEXING}"
                    
                }
                script{
                    emailext(
                        subject: "${env.JOB_NAME}版本上传成功#${env.BUILD_NUMBER}",
                        recipientProviders: [requestor()],
                        to: "${params.MAIL_NAME}",
                        body: """
                        <p>Job Name: ${env.JOB_NAME}</p> 
                        <p>Build ID: ${env.BUILD_NUMBER}</p>
                        <p>Patch名:${GPatchName}</p>
                        <p>PatchMD5:${GPatchMD5}</p>
                        <p>Patch文件大小:${GPatchSize}</p>
                        <p>包名:${GPackageName}</p>
                        <p>安装包MD5:${GPackageMD5}</p>
                        <p>安装包大小:${GPackageSize}</p>
                        <p><font color="#008000">Build status: success</font></p>
                        """,
                    )
                }
            }
        }
    }



@NonCPS
def GetLongVersion() {
    //正则匹配版本号
    def reg = /(V\d+\.\d+\.\d+\.\d+)/
    def matcher = PatchFtpPath =~ reg
    if (matcher){
        PatchVersion = matcher[0][0]
    }
    return PatchVersion
}

void Cleanup(){
    dir ("${env.WORKSPACE}"){
        sh "rm -rf *"
    }
}