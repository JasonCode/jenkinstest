@Library("SwordGamePipelineLibrary") _

properties([
  parameters([
        choice(choices: ['Artist', 'Master', 'Release'], description: '检查版本', name: 'CHECKER_BRANCH'),
        string(defaultValue: 'lidi@kingsoft/com LUANCHUNYANG@kingsoft.com',description:'邮件通知的收件人',name: 'MAIL_RECLPITNTS',trim: true),
    ])
])

import packageLib
import java.text.*
import java.util.Date
def GTimestamp
def body = ""
def wCount = ""
def eCount = ""
pipeline {
    options{
        timeout(time: 2, unit: 'HOURS')
    }
    agent {
        node {
            label 'PC_AutoTest'
        }
    }
    stages{      
        stage('Svn Update') {
            steps{
                script{
                    parallel(
                        Updatejx2m:{
                            script{
                                checkerLib.SvnUpdate("${env.WWISE_PROJECT_SVN_PATH}")
                            }
                        },
                        UpdateBranchContent:{
                            script{
                                if(params.CHECKER_BRANCH == "Master")
                                {
                                checkerLib.SvnUpdate("${env.MASTERBUILD_ROOT_PATH}/Content")
                                }
                                else if(params.CHECKER_BRANCH == "Release")
                                {
                                checkerLib.SvnUpdate("${env.RELEASEBUILD_ROOT_PATH}/Content")
                                }
                            }
                        }
                    )
                }
            
            }
        }
        stage('Build'){
            steps{
                script{
                    def szBranchPath = env.MASTERBUILD_ROOT_PATH
                    if(params.CHECKER_BRANCH == "Release")
                    {
                        szBranchPath = env.RELEASEBUILD_ROOT_PATH
                    }                   
                    def BuildResult = ""       
                    def WwisePath = "${szBranchPath}/Content/Wwise/Authoring/x64/Release/bin" 
                    Date currentBuildTime = new Date(currentBuild.startTimeInMillis)
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
                    GTimestamp = dateFormatter.format(currentBuildTime)               
                    dir(WwisePath){
                        try {            
                            sh "./WwiseCLI.exe ${env.WWISE_PROJECT_SVN_PATH}/jx2m.wproj -GenerateSoundBanks -Language \"English(US)\" > WwiseLog.txt"                                          
                        } catch (any) {
                            BuildResult = 'FAILURE'                       
                            println '异常输出'              
                        }                       
                        wCount = sh(returnStdout: true, script: 'grep "^Warning" WwiseLog.txt | wc -l').trim() 
                        eCount = sh(returnStdout: true, script: 'grep "^Error" WwiseLog.txt | wc -l').trim()
                        println "Warning=${wCount} Error=${eCount}"            
                    }        
                }
            }                    
        } 
        stage("Copy to WwiseAudio"){
            steps{
                script{
                    if(currentBuild.result == 'ABORTED') {
                        println "异常退出"
                        return
                    }   
                    def szBranchPath = env.MASTERBUILD_ROOT_PATH
                    if(params.CHECKER_BRANCH == "Release")
                    {
                        szBranchPath = env.RELEASEBUILD_ROOT_PATH
                    }          
                    sh"""
                        rm -rf ${szBranchPath}/Content/WwiseAudio/*
                        cp -r ${env.WWISE_PROJECT_SVN_PATH}/GeneratedSoundBanks/. ${szBranchPath}/Content/WwiseAudio
                    """
                }
            }            
        } 
        stage("Commit"){
            steps{
                script{
                    def Describe = "SubmitWwise"
                    if(currentBuild.result == 'ABORTED') {
                        println "异常退出"
                        return
                    }
                    def szBranchPath = env.MASTERBUILD_ROOT_PATH
                    if(params.CHECKER_BRANCH == "Release")
                    {
                        szBranchPath = env.RELEASEBUILD_ROOT_PATH
                    }              
                    sh """
                        cd ${szBranchPath}/Content/WwiseAudio
                        svn add . --force           
                        ${ARTIST_ROOT_PATH}/Engine/svndel.bat                                              
                        svn commit -m \"${Describe}\" 
                    """   
                }
            }                    
        }       
        stage("Metabase"){
            steps{
                script{
                    if(currentBuild.result == 'ABORTED') {
                        println "异常退出"
                        return
                    }
                    def strVersion = currentBuild.startTimeInMillis.toString()                        
                    new packageLib().UploadMetabaseInfo("WwiseBankGeneration", "WwiseBankGeneration", "Error","${eCount}" , strVersion)
                    new packageLib().UploadMetabaseInfo("WwiseBankGeneration", "WwiseBankGeneration", "Warning","${wCount}" , strVersion)
                }
            }             
        }                   
    }
    post('Send a message'){        
        success{
            script{                                              
                body= """
                {
                    "title": "Wwise_${params.CHECKER_BRANCH}_${GTimestamp}",   
                    "text" : "结果：成功，查看\\n<a href=\\\"${env.BUILD_URL}/console\\\">Wwise_${params.CHECKER_BRANCH}_${GTimestamp}启动log链接</a>"
                }
                """
                httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: "${env.LARK_CUSTOMBOT_URL_RESOURCECHECK}"
            } 
            emailext(
                subject: "Wwise_${params.CHECKER_BRANCH}_${GTimestamp}",
                to: "${params.MAIL_NAME}",
                body: """
                <p>Job Name: ${JOB_BASE_NAME}</p>
                <p>Build ID: ${env.BUILD_NUMBER}</p>
                <p>Build Status: ${currentBuild.result}</p>
                <p>LOG URL:&QUOT;<a href='${env.BUILD_URL}'>Error Count Message </a>&QUOT;</p>
                <p>Build Summary:</p>
                """,
            )                           
        } 
        failure {
            script {                   
                body= """
                {
                    "title": "Wwise_${params.CHECKER_BRANCH}_${GTimestamp}",   
                    "text" : "结果：失败，查看\\n<a href=\\\"${env.BUILD_URL}/console\\\">Wwise_${params.CHECKER_BRANCH}_${GTimestamp}启动log链接</a>"
                }
                """
                httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: "${env.LARK_CUSTOMBOT_URL_RESOURCECHECK}"
            }
            emailext(
            subject: "Wwise_${params.CHECKER_BRANCH}_${GTimestamp}",
            to: "${params.MAIL_NAME}",
            body: """
            <p>Job Name: ${JOB_BASE_NAME}</p>
            <p>Build ID: ${env.BUILD_NUMBER}</p>
            <p>Build Status: ${currentBuild.result}</p>
            <p>LOG URL:&QUOT;<a href='${env.BUILD_URL}'>Error Count Message </a>&QUOT;</p>
            <p>Build Summary:</p>
            """,
            ) 
        }       
    }
    environment {
        SVN_CREDENTIALS = credentials('092907a7-dcbf-425b-bc2d-c61715b73536')
        FTP_CREDENTIALS = credentials('3a51894f-d6f5-43a6-85be-e23daacc197b')
    }
}
