


import os
import sys
import shutil
import webbrowser
import os
import time
os.system('net use \\\\10.0.200.5\HeroFileServer "ZQcPE3QrDCYA4aE&" /user:YXHY\JgProPack')

import subprocess
site_package = r'\\10.0.200.5\HeroFileServer\ProjectX\Scripts\Python37\Lib\site-packages'
if site_package not in sys.path:
    sys.path.append(site_package)

def copy_tree(src, dst, symlinks=False, ignore=None):
    """
    Avoid the OSError raised by makedirs cmd in shutil.copytree
    symlinks will be ignore
    """
    names = os.listdir(src)
    if ignore:
        ignored_names = ignore(src, names)
    else:
        ignored_names = []
    try:
        if not os.path.exists(dst):
            os.makedirs(dst)
    except:
        print(dst)
        print(os.path.exists(dst))
        if not os.path.exists(dst):
            os.makedirs(dst)

    for item in names:
        if item in ignored_names:
            continue
        if symlinks:
            continue
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copy_tree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def removeTree(fold):
    for root, dirs, files in os.walk(fold):
        for file in files:
            os.remove(os.path.join(root, file))


def upBinaries(proj_dir):
    server_dir = r"\\10.0.200.5\HeroFileServer\ProjectX\Art\Packages\ProjectX_UE5"
    local_dir = proj_dir
    old_fold = os.path.join(os.path.dirname(server_dir), os.path.basename(server_dir)+"_old")
    if os.path.exists(server_dir):
        if os.path.exists(old_fold):
            removeTree(old_fold)
        time.sleep(1)
        copy_tree(server_dir, old_fold)
        removeTree(server_dir)
    time.sleep(1)
    copy_tree(local_dir, server_dir)


if __name__ == '__main__':
    import sys
    upBinaries(sys.argv[1])
