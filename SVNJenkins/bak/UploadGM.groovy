@Library("SwordGamePipelineLibrary") _

properties([
  parameters([
        choice(choices: ['PC_Build9'], description: '', name: 'NODE'),
        string(defaultValue: 'BuildNum', description: 'Build号', name: 'BUILD_NUMBER', trim: true)
    ])
])
node("${params.NODE}") {
    withEnv ([
    "SWORDGAME_HOME=D:/Jenkins/workspace/DailyBuild/ScheduledDailyBuildPC_Master/SwordGame"
    ]){
        stage('GM Generate'){              
            script{
                dir("${env.SWORDGAME_HOME}/Source/CMakeModules"){
                    sh"bin/LuaTools/bin/lua.exe RunGmTool.lua"  
                    sh"bin/LuaTools/bin/lua.exe GenerateDataTableHtml.lua"
                }                                           
            }                                             
        }
        stage('Upload'){
            script{
                dir("${SWORDGAME_HOME}"){
                    new packageLib().PutToFtp('ShenZhenFTP', "GMData/${BUILD_NUMBER}", "GMData.html", false, false)
                    }
                dir("${SWORDGAME_HOME}/Saved/DataTableHtml"){
                    new packageLib().PutToFtp('ShenZhenFTP', "GMData/${BUILD_NUMBER}/DataTableHtml", "*.html", false, false)
                }
            }
        }      
    }
}