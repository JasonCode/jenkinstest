@Library("PLPipelineLibrary") _

pipeline {
	parameters{
		choice(choices: ['WinBuild-11.152','WinBuild-13.37'], description: '选择打包机', name: 'NODE')

		booleanParam(defaultValue: true, description: '更新服务器?', name: 'bUpdateServer')
		booleanParam(defaultValue: true, description: '更新配表?', name: 'bReplaceConfig')
		booleanParam(defaultValue: true, description: '是否关闭服务器?', name: 'bShutDownServer')
		booleanParam(defaultValue: true, description: '重启服务器?', name: 'bRestartServer')
	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		ConfigDataRoot = 'C:/Des'
		ConfigDataWorkSpace = 'XBuild01_Des'
		DeployFolder = 'C:/PLDeploy'
	}

	agent{
        node{
            label "${params.NODE}"
        }
    }

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
				}
			}
		}

		stage("Update Config"){
			steps{
				script{
					if(params.bReplaceConfig)
						commonUtil.SyncWorkspace(ConfigDataWorkSpace, ConfigDataRoot)
				}
			}
		}

		stage("ShutDownServer"){
			steps{
				script{
					if(params.bShutDownServer)
					{
						dir(localUtil.GetServerRootOnWin()){
							try{
									bat 'taskkill /F /IM XGameServer.exe'
								}    
							catch (err){
								echo "kill XGameServer failed"
							}
							
						}
					}
					
					
					// localUtil.ShutdownLocalServer(CommonConfig.OS_Windows)
				}
			}
		}

		stage("Backup Log"){
			steps{
				script{
					echo "We Pass Backup Log Step"
					//localUtil.BackupServerLog(CommonConfig.OS_Windows)
				}
			}
		}

		stage("Update Server"){
			steps{
				script{
					if(params.bUpdateServer){
						echo 'update server from svn'
						dir(localUtil.GetServerRootOnWin()){
							sh 'svn cleanup'
							sh 'svn up'
						}
					}
					
				}
			}
		}

		stage("Replace Config"){
			steps{
				script{
					if(params.bReplaceConfig){
						//清理目录
						String serverFolder = localUtil.GetServerRootOnWin() + "/XGame/Content/ServerData"
						dir(serverFolder){ deleteDir() }
						String clientFolder = localUtil.GetServerRootOnWin() + "/XGame/Content/Script/ClientData"
						dir(clientFolder){ deleteDir() }

						//拷贝文件
						sh "cp -r ${ConfigDataRoot}/kf/ExportData/client/cfg/ ${clientFolder}"
						sh "cp -r ${ConfigDataRoot}/kf/ExportData/server/cfg/xls ${serverFolder}"
					}
				}
			}
		}

		stage("StartServer"){
			steps{
				script{
					if(params.bRestartServer)
					{
						withEnv(['JENKINS_NODE_COOKIE=dontkillme']) 
						{
	                        dir(localUtil.GetServerRootOnWin()){
								bat "start XGameServer.exe"
							}
                    	}

						
						// localUtil.StartLocalerver(CommonConfig.OS_Windows)
					}
				}
			}
		}


	}
//--------- end of pipeline	
}
