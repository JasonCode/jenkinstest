@Library("PLPipelineLibrary") _

def String GetServerFolder() 
{
	if(params.ServerBranch == "dev")
		return '/home/a/server/Svn_LinuxServer_dev'
	else if(params.ServerBranch == "stable")
		return '/home/a/server/Svn_LinuxServer_stable'
}


pipeline {
	parameters{
		choice(choices: ['Linux01'], description: '选择打包机', name: 'NODE')


		booleanParam(defaultValue: true, description: 'kill服务器?', name: 'bKillServer')
		booleanParam(defaultValue: true, description: '更新服务器?', name: 'bUpdateServer')
		booleanParam(defaultValue: true, description: '重启服务器?', name: 'bRestartServer')

		choice(choices: ['dev','stable'], description: '选择分支', name: 'ServerBranch')
	}


	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		ServerFolder = GetServerFolder()
	}

	agent{
        node{
            label "${params.NODE}"
        }
    }

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
				}
			}
		}


		stage("ShutDownServer"){
			steps{
				script{
					dir(ServerFolder)
					{
						if(params.bKillServer)
						{
							try{
									sh "pkill -9 XGame"
								}    
							catch (err){
								echo "kill XGameServer failed"
							}
							// sh "${ServerFolder}/KillServer.sh &"
						}
					}
				}
			}
		}

		stage("Backup Log"){
			steps{
				script{
					echo "We Pass Backup Log Step"
					//localUtil.BackupServerLog(CommonConfig.OS_Windows)
				}
			}
		}

		stage("Update Server"){
			steps{
				script{
					if(params.bUpdateServer){
						echo 'update server from svn'
						dir(ServerFolder){
							sh 'svn cleanup'
							sh 'svn up'
							// sh 'sleep 5'
						}
					}
					
				}
			}
		}


		stage("StartServer"){
			steps{
				script{
					if(params.bRestartServer)
						dir(ServerFolder){
							sh "chmod -R 777 ${ServerFolder}"
							sh '${ServerFolder}/XGameServer.sh &'
							sh "JENKINS_NODE_COOKIE=dontKillMe ${ServerFolder}/XGameServer.sh &"
						}
						
				}
			}
		}


	}
//--------- end of pipeline	
}
