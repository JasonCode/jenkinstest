@Library("PLPipelineLibrary") _


pipeline {
	//options {
		//timeout(time: 30, unit: 'SECONDS')
	//}

	parameters{
		choice(choices: ['WinBuild-11.152','WinBuild-13.37'], description: '选择打包机', name: 'NODE')

		booleanParam(defaultValue: false, description: '使用Pak?', name: 'bPak')
		booleanParam(defaultValue: true, description: '是否打包服务器', name: 'bPackageServer')
		booleanParam(defaultValue: true, description: '是否打包客户端', name: 'bPackageClient')
		booleanParam(defaultValue: false, description: '是否包含Linux服务器', name: 'bIncludeLinux')

		booleanParam(defaultValue: true, description: '更新?', name: 'bUpdate')
		booleanParam(defaultValue: false, description: '编译引擎?', name: 'bCompileEngine')

		booleanParam(defaultValue: true, description: '编译游戏?', name: 'bCompileGame')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package阶段?', name: 'bPackage')
		booleanParam(defaultValue: true, description: '上传Svn?', name: 'bUploadSvn')
		// booleanParam(defaultValue: false, description: '上传FTP?', name: 'bUploadFtp')
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		EngineFolder = 'C:/XBuild01_ProjectX_Dev'
		GameFolder = 'C:/XBuild01_ProjectX_Dev/ProjectX'
		EngineWorkSpace = 'XBuild01_ProjectX_Dev'

		DeployFolder = "C:/PLDeploy/pc_dev"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
					packageUtil.Reset()
					packageUtil.SetGameFolder(GameFolder)
					packageUtil.SetEngineFolder(EngineFolder)
					packageUtil.SetTargetPlatform(CommonConfig.Plat_Win64)
					packageUtil.SetPackageClient(params.bPackageClient)
					packageUtil.SetUeExeName("UnrealEditor-Cmd.exe")

					if (params.bIncludeLinux){
						packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Linux)
					}else{
						packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Win64)
					}
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetPak(params.bPak)
					packageUtil.SetPackageServer(params.bPackageServer)
					packageUtil.SetAchive(false)
					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
					if(params.bUpdate)
						commonUtil.SyncWorkspace(EngineWorkSpace, EngineFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompileEngine)
						commonUtil.CompileEngineOn(CommonConfig.OS_Windows, EngineFolder,"UE5", "UnrealEditor")
				}
			}
		}


		stage("Compile Game"){
			steps{
				script{
					if(params.bCompileGame)
						commonUtil.CompileGameOn(CommonConfig.OS_Windows, EngineFolder, GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackage)packageUtil.StartPackage()
				}
			}
		}

		stage("UploadSVN"){
			steps{
				script{
					if(params.bUploadSvn)
						commonUtil.UploadToSvn(packageUtil.GetPackagedPath(), DeployFolder, packageUtil.GetOSPlatform(),params.bPackageServer,params.bPackageClient,params.bIncludeLinux)
				}
			}
		}

		// stage("UploadLinuxFTP"){
		// 	steps{
		// 		script{
		// 			if(params.bIncludeLinux)
		// 				commonUtil.UploadLinuxServerFolderToFtp(packageUtil.GetPackagedPath() + "/LinuxServer", packageUtil.GetServerTargetPlatform())
		// 		}
		// 	}
		// }

		// stage("UploadFTP"){
		// 	steps{
		// 		script{
		// 			if(params.bUploadFtp)
		// 				commonUtil.UploadFolderToFtp(packageUtil.GetPackagedPath(), packageUtil.GetTargetPlatform())
		// 		}
		// 	}
		// }
	}

	post{
		//success {script {messageUtil.SendWXMsg("打包", "main", "Success")}}
		failure {script {messageUtil.SendWXMsg("打包", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("打包", "main", "Unstable")}
		}
	}

//--------- end of pipeline	
}
