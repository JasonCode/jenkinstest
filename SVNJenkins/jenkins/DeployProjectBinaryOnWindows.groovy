@Library("PLPipelineLibrary") _

pipeline {
	parameters{
		choice(choices: ['WinBuild01', 'master'], description: '选择打包机', name: 'NODE')
	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		GameFolder = 'C:/ProjectX_Binaries'
	}

	agent{
        node{
            label "${params.NODE}"
        }
    }

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
				}
			}
		}

		stage("上传二进制"){
			steps{
				script{
					echo "update binary"
					String cmd = "python ./third_tool/PyUpBinaries.py ${GameFolder}"
	                sh "${cmd}"
				}
			}
		}
	}
//--------- end of pipeline	
}
