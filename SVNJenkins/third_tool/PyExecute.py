#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author: 785415581@qq.com
Date: 2022/4/7 17:44
"""
import re
import os
import sys
import json
import argparse
import requests
import datetime
import traceback
import subprocess

member = {"qinjiaxin": "秦家鑫", "chenghuanhuan": "程缓缓", "cunwenshuang": "崔文爽", "liuhangyu": "刘航雨",
          "tengzhenyi": "腾哥", "wangjunjie": "俊杰大佬", "wangruonan": "若男君", "wuyang": "武洋", "xuelong": "龙哥"}


def CheckTextureNameRule(textureName):
    if not textureName.startswith('T_'):
        return False
    return True


def CheckTextureResolution(sizeX, sizeY):
    if sizeX and isinstance(sizeX, str):
        sizeX = int(sizeX)
    if sizeY and isinstance(sizeY, str):
        sizeY = int(sizeY)
    maxSize = max(sizeX, sizeY)
    if maxSize > 4096:
        return False
    return True


def getJsonPath():
    document = os.path.expanduser("~") + "\\Documents"
    configPath = document + "\\__tempConfig"
    if os.path.isdir(configPath):
        return configPath
    else:
        os.makedirs(configPath)
        return configPath


def CheckTextureCompressionSetting(assetName, CompressionSetting):
    if assetName.endswith("ORM") or assetName.endswith("ORH") or assetName.endswith("OGS") or assetName.endswith("Mask") or assetName.endswith("_M"):
        if "TC_MASKS" not in CompressionSetting:
            return False
    return True


def cleanJson(jsonPath):
    num = len(os.listdir(jsonPath))
    if num > 100:
        for i in range(num):
            os.remove(os.path.join(jsonPath, os.listdir(jsonPath)[i]))


def runCommandLine(cmd):
    process = subprocess.run(cmd, shell=True)


def initP4():
    os.popen('p4 set P4PORT=10.0.201.12:1666')
    os.popen('p4 set P4USER=XBuild01')
    os.popen('p4 set P4PASSWD=XBuild_01')


def getSubmitFile(changelist):
    fileLists = []

    cmd = "p4 files @={changeList}".format(changeList=changelist)
    p4Process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    res_files, err = p4Process.communicate()
    for res in res_files.decode('utf-8', "ignore").split('\r\n'):
        if not res or "delete" in res:
            continue
        res = re.sub(r'#\d.*[)]', "", res)
        fileLists.append(res)

    return fileLists


def findErrorFile(fileLists, JsonFile):
    checkOutput = {}
    with open(JsonFile, 'r') as fp:
        data = json.load(fp)
    for file in fileLists:
        fileName = os.path.basename(file)
        NoExtName = re.sub('.uasset', '', fileName)

        if NoExtName in data:
            fileInfo = data.get(NoExtName, '')
            assetName = fileInfo.get("asset_name", '')
            packageName = fileInfo.get("package_name", '')
            sizeX = fileInfo.get("sizeX", '')
            sizeY = fileInfo.get("sizeY", '')
            Compression_setting = fileInfo.get("Compression_setting")

            checkOutput[packageName] = {}
            checkOutput["invalidate"] = False
            if not CheckTextureNameRule(assetName):
                checkOutput["invalidate"] = True
            if not CheckTextureResolution(sizeX, sizeY):
                checkOutput["invalidate"] = True
            checkOutput[packageName].update({"nameRule": CheckTextureNameRule(assetName)})
            checkOutput[packageName].update({"resolutionRule": CheckTextureResolution(sizeX, sizeY)})
            checkOutput[packageName].update({"Compression_setting": CheckTextureCompressionSetting(assetName, Compression_setting)})
    return checkOutput


def AssemblyWXMessage(checkOutput, changelist, user):
    print("checkOutput = {}".format(checkOutput))
    if checkOutput.get("invalidate", False):
        checkOutput.pop("invalidate")
        NTextureName = ""
        NresTextureName = ""
        NCompressionSetting = ""
        for key, value in checkOutput.items():
            if not value.get("nameRule"):
                NTextureName = ">" + NTextureName + str(key) + "\n"
            if not value.get("resolutionRule"):
                NresTextureName = ">" + NresTextureName + str(key) + "\n"
            if not value.get("Compression_setting"):
                NCompressionSetting = ">" + NCompressionSetting + str(key) + "\n"

        msg = """
        # 流水线触发，提交Changelist：{changelist} 提交者 {user}\n
        命名不规范文件：\n
        {TextureName}\n
        分辨率文件不正确文件(超过4K分辨率)：\n
        {resTextureName}\n
        压缩格式应为Mask文件：\n
        {NCompressionSetting}\n
        """.format(changelist=changelist, user=member.get(user, ""), TextureName=NTextureName,
                   resTextureName=NresTextureName, NCompressionSetting=NCompressionSetting)
        return msg
    else:
        return None


def sendMessage(msg):
    # wx_bot = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=0c480dc0-061b-48d3-ae3d-f31a1215c38a"
    wx_bot = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=8922528e-76d7-4624-800d-bc7ebdfa598d"
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    data_markdown = json.dumps(
        {
            "msgtype": "markdown",
            "markdown":
                {
                    "content": msg,
                }
        }
    )

    requests.packages.urllib3.disable_warnings()
    proxies = {"http": None, "https": None}
    try:
        requests.post(wx_bot, data_markdown, auth=('Content-Type', 'application/json'), verify=False, proxies=proxies)
    except ConnectionError as e:
        error = traceback.print_exc()




parser = argparse.ArgumentParser(usage='传递参数', description="参数说明")

parser.add_argument('-c', '--changelist', dest='changelist', type=int, default=1, help="本次提交的changelist号")

parser.add_argument('-e', '--uexe', dest='UExe', type=str,
                    default='D:/workSpace/Unreal/sourceCode/UE5Preview2/Engine/Binaries/Win64/UnrealEditor-Cmd.exe',
                    help='UE的exe文件路径')

parser.add_argument('-p', '--uproject', dest='uproject',
                    default='D:/P4workSpace/qinjiaxin_01yxhy1235_dev_personal_qinjiaxin_8966/MyProject/MyProject.uproject',
                    type=str, help='项目的UProject的位置')


parser.add_argument('-u', '--user', dest='user', default='Root', type=str, help='用户名')

args = parser.parse_args()

UExe = args.UExe

UProject = args.uproject

changelist = args.changelist
JsonPath = getJsonPath()
user = args.user
JsonFile = os.path.join(JsonPath, "checkResult_" + str(changelist) + ".json")

cleanJson(JsonPath)

# --------------------------------assembly command line

script = """import unreal
import json
EditorAssetLib = unreal.EditorAssetLibrary()
AllAssets = EditorAssetLib.list_assets('/Game/', recursive=True)
res = {}
for Asset in AllAssets:
    AssetData = EditorAssetLib.find_asset_data(Asset)
    GetAsset = AssetData.get_asset()
    AssetObj = EditorAssetLib.load_asset(Asset)
    if AssetData.asset_class == 'Texture2D':
        res[str(AssetData.asset_name)] = {}
        res[str(AssetData.asset_name)].update({'asset_name': str(AssetData.asset_name)})
        res[str(AssetData.asset_name)].update({'object_path': str(AssetData.object_path)})
        res[str(AssetData.asset_name)].update({'package_name': str(AssetData.package_name)})
        res[str(AssetData.asset_name)].update({'package_path': str(AssetData.package_path)})
        res[str(AssetData.asset_name)].update({'sizeX': str(GetAsset.blueprint_get_size_x())})
        res[str(AssetData.asset_name)].update({'sizeY': str(GetAsset.blueprint_get_size_y())})
        res[str(AssetData.asset_name)].update({'sRGB': str(GetAsset.get_editor_property('sRGB'))})
        res[str(AssetData.asset_name)].update({'Compression_setting': str(GetAsset.get_editor_property('compressionsettings'))})

with open('%s', 'w') as fp:
    json.dump(res, fp, indent=4)
""" % JsonFile

cmd = UExe + " "
cmd += UProject + " "
cmd += "-run=pythonscript "

cmd += "-script="

cmd += script.__repr__()

# --------------------------------run command line
runCommandLine(cmd)

# --------------------------------init perforce
initP4()

# --------------------------------get submit file
fileLists = getSubmitFile(changelist)

# --------------------------------find name error file
checkOutput = findErrorFile(fileLists, JsonFile)

# --------------------------------assembly wx message
msg = AssemblyWXMessage(checkOutput, changelist, user)
if msg:
    # --------------------------------send message
    sendMessage(msg)



