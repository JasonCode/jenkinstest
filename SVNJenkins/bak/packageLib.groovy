import groovy.transform.Field
import java.text.*
import java.util.Date
import groovy.json.JsonOutput


def params

def FTPSelect

//"UnrealEngine"
def ENGINE_FOLDER_NAME

// 工程目录
def BuildWorkingAbsPath

//Engine路径
def EngineParentAbsPath
def EngineAbsPath
//UE4Editor.exe
def EngineEditorAbsPath
//patch版本号文件地址
def PreVersionAbsPath
//SwordGame.uproject
def SwordGameUProjectAbsPath
//工程目录
def SwordGameAbsPath

// 代码分支和资源分支确定的唯一名称
GBranchWorkingFolderName = ""

//content目录
def SwordGameContentAbsPath
//SwordGame.sln
def SwordGameSolutionAbsPath
// 打包地址
def SwordGameInstallerAbsPath
//打包地址与包名
def GSwordGamePackageAbsPath
def SymbolFilePath
//SymbolFile地址
def SymbolFliePath
//客户端CommonConfig地址
def CommonConfigFileName
//cook
String GCookOrSkipFlag = ""

//login地址
def LoginUrLString = ""
//Extrapackage参数
String GExtraPackageArg
//打包基础参数
def BasicsParameter
//Release参数
def ReleaseParameter
//分包参数
def GAddPackageParameter

//版本号
String GShortVersion
String GLongVersion

def BaseVersion

//Package名字
def GPackageName
//patch名字
def GPackagePatchName
//windows分包名字
def GNsisbinFileName = ""
//minafest文件名字
def GManifestFileName = ""
//ChunkPatch名字
GChunkPatchName = ""
// Chunk Pak 所在路径
GPakChunkAbsPath = ""
// Chunk3 Pak 原始名字
GChunk3PakOriginalName = ""
// 平台名字
GPlatformName = ""
// Chunk 中使用的平台名，与 GPlatformName 有些不同
// TODO: 统一两者
GChunkPlatformName = ""

// Chunk3 的 Pak 上传FTP的名字
GChunk3PakPackageName = ""


// 符号文件 上传FTP名称
GSymbolFileName = ""


//Releases备份名字
def GReleasesBackupName
// Shipping, Development, Debug 标识
GCompileFlag = "Development"

//patch名字
def GPatchName
//distrbution参数
GDistributionFlag = ""

// Android 架构名（64Bit或者32Bit）
GAndroidArchName = "arm64"

// 分支对应的云Chunk根目录
// chunk/BuddyBuild/JXda9339
GCloudBranchPath = ""

// 分支对应的区分版本的Chunk目录
// Patch/V3.21
GPatchPatchVersionPrefix = ""

// 带版本号和平台名的 Chunk 目录相对路径
// Patch/V3.21/Patch/Android
GPatchPathWithVersion = ""

// 本地 Patch 压缩用临时路径
// CloudDir/BuddyBuild/JXda9339/Patch/V3.21/Patch/Android
GLocalPatchZipDir = ""

// 是否将Patch上传过腾讯云
GAlreadyUploadPatchToCloud = false


//平台
def Platform
@Field final PLATFORM_ANDROID = 0
@Field final PLATFORM_IOS = 1
@Field final PLATFORM_WINDOWS = 2

// UE4 生成的 Log 位置
GSwordGameLogAbsPath = ""
// 导表 Log 临时备份位置
GSwordGameExportTableLogAbsPath = ""
// 导图 Log 临时备份位置
GSwordGameExportMapLogAbsPath = ""
// Cook Log 临时备份位置
GSwordGameCookLogAbsPath = ""
// Cook CSV 目录上传名称
GCookOutputName = ""
// Cook Log 上传FTP名称
GCookLogUploadName = ""
// 导表 Log 上传FTP名称
GTablesLogUploadName = ""
// 导图 Log 上传FTP名称
GMapLogUploadName = ""

// 本次打包中，是否已经Cook 过
boolean GAlreadyCooked

// Patch 生成目录位置
String GPatchFolderAbsPath = ""
// Patch 打包 Zip 文件路径
String GPatchZipFileAbsPath = ""

//Releases 备份 Zip 文件路径
String GReleasesBackupZipAbsPath = ""

//Publish参数
String GPublisher = ""

// TODO: 命名规范，全局变量增加



// 是否支持 Apple SignIn
boolean GiOSWithAppleSignInEnabled = true

@NonCPS
def GetSvnInfoFromSvnUrl(svnPath) {
    // 计算 SVN 地址参数
    def matcher = svnPath =~ '(https://[a-zA-Z0-9.-_]+/repos/SwordGame)(/.*)'

    if(matcher) {
        // SVN地址 主机名
        def svnUrlHost = matcher[0][1]
        // SVN地址 分支名
        def svnUrlBranch = matcher[0][2]

        return [ true, svnUrlHost, svnUrlBranch ]
    } else {
        // SVN 分析失败
        return [ false, "", "" ]
    }
}

def PrepareEnv(Map inParams, int platform) {

    // Set Build Name with WORK_NAME
    buildName params.WORK_NAME + "#${BUILD_NUMBER}"

    Platform = platform

    ENGINE_FOLDER_NAME = "UnrealEngine"

    params = inParams
    FTPSelect = "${params.FTP}"

    GAlreadyUploadPatchToCloud = false

    // Choose the Engine path
    if (params.GIT_ENGINE_BRANCH == 'release') {
        EngineParentAbsPath = env.ENGINE_PATH_RELEASE
    } else {
        EngineParentAbsPath = env.ENGINE_PATH_MUTABLE
    }

    // 计算 SVN 地址参数
    boolean validSvnUrl
    String svnUrlHost
    String svnUrlBranch

    (validSvnUrl, svnUrlHost, svnUrlBranch) = GetSvnInfoFromSvnUrl(params.SVN_PATH)

    if(!validSvnUrl) {
        // SVN 分析失败
        echo "SVN path is invalid!"
        sh "exit 1"
    }


    // 判断是否需要复写版本号
    if(params.VERSION_OVERRIDE != null && params.VERSION_OVERRIDE != '') {
        echo "Need Override Current Version: " + params.VERSION_OVERRIDE

        def reg = /(\d+)\.(\d+)\.(\d+)\.(\d+)/
        def matcher = params.VERSION_OVERRIDE =~reg

        if(matcher == null){
            error("Parameter VERSION_OVERRIDE is invalid!")
        }

        // 主版本号
        GMajorVersionOverride = matcher[0][1]
        // 周版本号
        GMinorVersionOverride = matcher[0][2]
        // 集成版本号
        GLogicVersionOverride = matcher[0][3]
        // 客户端构建版本号
        GBuildVersionOverride = matcher[0][4]

        GLongVersionOverride = matcher[0][0]
        GShortVersionOverride = GMajorVersionOverride + "." + GMinorVersionOverride
    }

    // Shipping 优先级最高
    if (params.SHIPPING == true) {
        GCompileFlag = "Shipping"
    } else {
        if (params.FORCEDEBUG == true ){
            GCompileFlag = "Debug"
        } else {
            GCompileFlag = "Development"
        }
    }

    if(params.SKIP_COOK == true) {
        GCookOrSkipFlag = "-skipcook"
    } else {
        GCookOrSkipFlag = "-cook"
    }

    // 开始时，设置为从未 cook 过
    GAlreadyCooked = false

    if(params.PUBLISHER != ""){
        GPublisher = "-Publisher=${params.PUBLISHER}"
    }

    // if (params.LoginUrL == "") {
    //     LoginUrLString = ""
    //     echo "No LoginUrL In."
    // } else {
    //     echo "LoginUrL In: " + params.LoginUrL
    //     LoginUrLString = "-LoginUrlIOSWX=${params.LoginUrL} -LoginUrlIOSQQ=${params.LoginUrL} -LoginUrlPC=${params.LoginUrL} -LoginUrlAndroidWX=${params.LoginUrL} -LoginUrlAndroidQQ=${params.LoginUrL} -EnableRemotedLogger=1"
    // }

    if (Platform == PLATFORM_ANDROID){
        GPlatformName = "Android"
        GChunkPlatformName = "Android"

        if(params.USE_32BIT) {
            GAndroidArchName = "armv7"
        } else {
            GAndroidArchName = "arm64"
        }

    }else if(Platform == PLATFORM_IOS) {
        GPlatformName = "IOS"
        GChunkPlatformName = "IOS"
    }else if(Platform == PLATFORM_WINDOWS) {

        // TODO: 为什么有这么多版本的 Windows 名字
        GPlatformName = "Windows"
        GChunkPlatformName = "Windows"
    }
    
    GPatchName = "Cloud_${GPlatformName}"
    
    GExtraPackageArg = ""

    if (params.REBUILD_PACKAGE == true) {
        GExtraPackageArg = GExtraPackageArg + " -clean"
    }
    if (params.MALLOC_PROFILER == true) {
        GExtraPackageArg = GExtraPackageArg + " -mallocprofiler"
    }
    if (params.LOG_INSHIPPING == true) {
        GExtraPackageArg = GExtraPackageArg + " -loginshipping"
    }
    if (params.CHECK_INSHIPPING == true) {
        GExtraPackageArg = GExtraPackageArg +  " -checkinshipping"
    }

    echo GExtraPackageArg

    if (params.ADDRESOURCE_PACKAGE == true){
        GAddPackageParameter = ""
    }else{
        GAddPackageParameter = "-ini:Engine:[/Script/Engine.SkipPackageChunk]:-ChunkID=3"
    }

    if (Platform == PLATFORM_IOS) {
        // iOS 的 GDistributionFlag 与 签名有关，与 GCompileFlag 无关
        if (params.SIGNING == "ROGDevelopment" || params.SIGNING == "eFunDevelopment" || params.SIGNING == "TencentDev"){
            GDistributionFlag = ""
        } else {
            GDistributionFlag = "-distribution"
        }
    } else {
        // Android 和 Windows 的 GDistributionFlag 与 GCompileFlag 有关
        if (params.FORCEDEBUG == true ){
            GDistributionFlag = ""
        } else {
            GDistributionFlag = "-distribution"
        }
    }


    // Remove all the back slashes in path
    EngineParentAbsPath = EngineParentAbsPath.replaceAll(~/[\\]/, "/")

    EngineAbsPath = EngineParentAbsPath + "/" + ENGINE_FOLDER_NAME
    echo "Engine Path: " + EngineAbsPath


    // Make workspace directory
    // TODO: 使用 SVN 分支而不是 完整 SVN Url
    // writeFile(file: 'BuildFingerPrint.txt', text: "${svnUrlBranch}+${params.BRANCH_NAME}")

    String fingerPrintString = "${params.SVN_PATH}+${params.BRANCH_NAME}"
    // TODO: 临时兼容 Prerelease 的打包处理
    fingerPrintString = fingerPrintString.replaceAll(~/https:\/\/svn.shiyou.kingsoft.com\/repos/, "svn://172.18.73.6")

    writeFile(file: 'BuildFingerPrint.txt', text: fingerPrintString)
    GBranchWorkingFolderName = "JX" + sha1(file: 'BuildFingerPrint.txt').substring(0,8)

    echo "GBranchWorkingFolderName=" + GBranchWorkingFolderName

    GWorkSpaceAbsPath = env.WORKSPACE
    GWorkSpaceAbsPath = GWorkSpaceAbsPath.replaceAll(~/[\\]/, "/")

    // 避免以数字开头 导致define无效
    BuildWorkingAbsPath = GWorkSpaceAbsPath + "/" +  GBranchWorkingFolderName

    echo BuildWorkingAbsPath

    // Perversion file Path 
    PreVersionAbsPath = GWorkSpaceAbsPath + "/" +  GBranchWorkingFolderName + "_PreVersion.txt"

    GJenkinsBuildAbsPath = GWorkSpaceAbsPath + "/" + "JenkinsBuildScripts"

    // SwordGame paths
    SwordGameAbsPath = BuildWorkingAbsPath
    SwordGameContentAbsPath = BuildWorkingAbsPath + "/Content"
    SwordGameBinaryAbsPath = BuildWorkingAbsPath + "/Binaries/Win64"
    SwordGameUProjectAbsPath = SwordGameAbsPath + "/SwordGame.uproject"
    SwordGameSolutionAbsPath = SwordGameAbsPath + "/SwordGame.sln"
    SwordGameCMakeModuleAbsPath = SwordGameAbsPath + "/Source/CMakeModules"
    
    if ( Platform == PLATFORM_ANDROID ){
        SwordGameInstallerAbsPath = BuildWorkingAbsPath + "/Binaries/Android"
    }else if (Platform == PLATFORM_WINDOWS) {
        SwordGameInstallerAbsPath = SwordGameCMakeModuleAbsPath + "/WindowsInstaller"
    }
    else {
        SwordGameInstallerAbsPath = BuildWorkingAbsPath +"/Binaries/IOS"
    }


    // 导表 Log 临时备份位置
    GSwordGameExportTableLogAbsPath = "${SwordGameAbsPath}/SwordGame_ExportTable.log"
    // 导图 Log 临时备份位置
    GSwordGameExportMapLogAbsPath = "${SwordGameAbsPath}/SwordGame_ExportMap.log"
    // Cook Log 临时备份位置
    GSwordGameCookLogAbsPath = "${SwordGameAbsPath}/SwordGame_Cooklog.log"

    echo SwordGameInstallerAbsPath
    if ( Platform == PLATFORM_ANDROID || Platform == PLATFORM_WINDOWS){
        EngineEditorAbsPath = EngineAbsPath + "/Engine/Binaries/Win64/UE4Editor.exe"
        GSwordGameLogAbsPath = "${SwordGameAbsPath}/Saved/Logs/SwordGame.log"
    } else {
        EngineEditorAbsPath = EngineAbsPath + "/Engine/Binaries/Mac/UE4Editor.app/Contents/MacOS/UE4Editor"
        // Mac 系统下的 Log 位置在 ~/Library/Logs， Config 在 ~/Library/Preferences
        // Ref: https://answers.unrealengine.com/questions/37328/mac-os-x-where-are-the-log-files-in-41.html
        // 中间需要有空格转义
        GSwordGameLogAbsPath = "~/Library/Logs/Unreal\\ Engine/SwordGameEditor/SwordGame.log"
    }
    echo SwordGameUProjectAbsPath

    // 先clone jenkins脚本，不然第一次打包在package前失败或中止会找不到图片
    dir("${GWorkSpaceAbsPath}"){
        DeleteFileIfExist(GJenkinsBuildAbsPath)
        sh "git clone -b master --single-branch --depth 1 http://git.jx2.bjxsj.site/pirate/JenkinsBuildScripts.git"
    }

    // Patch 相关路径地址
    if ( Platform == PLATFORM_ANDROID){
        GPatchFolderAbsPath = "${SwordGameAbsPath}/ChunkInstall/Android_ASTC/CloudDir"
    }else if( Platform == PLATFORM_IOS){
        GPatchFolderAbsPath = "${SwordGameAbsPath}/ChunkInstall/IOS/CloudDir"
    }else if( Platform == PLATFORM_WINDOWS){
        GPatchFolderAbsPath = "${SwordGameAbsPath}/ChunkInstall/WindowsNoEditor/CloudDir"
    }

    GPatchZipFileAbsPath = "${SwordGameInstallerAbsPath}/Cloud_${GPlatformName}.zip"
    GReleasesBackupZipAbsPath = "${SwordGameAbsPath}/Releases_${GPlatformName}.zip"

    // StartupMovieName
    TencentMovieName = "tencent_logo"
    EFunMovieName = "efun_logo"
}

def UpdateEngine() {

    echo "Params: ${params.SKIP_UPDATE_ENGINE}"

    if(params.SKIP_UPDATE_ENGINE == true) {
        echo "Skip Updating Engine Code due to input parameters."
    } else {
        echo EngineAbsPath

        ws(EngineAbsPath) {
            sh 'pwd'

            // 处理 fatal: unable to access 'https://github.com/NoahXia/UnrealEngine.git/': OpenSSL SSL_read: SSL_ERROR_SYSCALL, errno 10054 问题
            // 参考：https://www.cnblogs.com/justdoyou/p/9832552.html
            sh """
                git config http.postBuffer 524288000
                git config http.sslVerify "false"
                git remote set-url origin git@github.com:NoahXia/UnrealEngine.git &&
                git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" &&
                git fetch &&
                git clean -f &&
                git reset --hard HEAD &&
                git checkout origin/${params.GIT_ENGINE_BRANCH}
            """
        }
    }
}

def UpdateSwordGame() {
    if(params.SKIP_UPDATE_SWORDGAME){       
        echo "Params: ${params.SKIP_UPDATE_SWORDGAME}"
        return
    }          
    if(!fileExists("${BuildWorkingAbsPath}")){
        echo "mkdir new folder"
        sh"mkdir ${BuildWorkingAbsPath}"       
        CreateGitlibrary()
        

    } 
    else {
        echo "folder existing"
        boolean isGitDir = true
        try{              
            dir("${BuildWorkingAbsPath}"){
                sh"git rev-parse --git-dir > /dev/null 2>&1"                           
            }
        } 
        catch(any){
            isGitDir = false
        }
        ws("${BuildWorkingAbsPath}"){
            if(isGitDir){            
                echo "isGitDir true update folder"                 
                UpdateGitlibrary()                                                                       
            } else {     
                echo "isGitDir false delete folder"              
                CreateGitlibrary()                   
            }
        }
    }              
}

def UpdateGitlibrary(){
    def GitUrlWithoutHttp = "http://git.jx2.bjxsj.site/pirate/SwordGame.git".substring(7)
    sh """   
        pwd                                              
        git remote set-url origin http://${GIT_CREDENTIALS}@${GitUrlWithoutHttp} &&
        git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" &&
        git fetch &&
        git clean -f -d &&
        git reset --hard HEAD &&
        git checkout "${params.BRANCH_NAME}" &&
        git pull &&
        git lfs install &&
        git lfs pull &&
        git submodule sync --recursive &&
        git submodule update
    """
}

def CreateGitlibrary(){    
    def GitUrlWithoutHttp = "http://git.jx2.bjxsj.site/pirate/SwordGame.git".substring(7)
    ws("${BuildWorkingAbsPath}"){
        if(params.FAST_GIT_CLONE == true){
            sh"""
                pwd
                rm -rf ${BuildWorkingAbsPath}/*
                cp -r \"${ORIGIN_SWORDGAME_GIT_PATH}\"/.git ${BuildWorkingAbsPath}          
            """
            UpdateGitlibrary()
        } else {
            sh"""
                pwd
                rm -rf ${BuildWorkingAbsPath}/*
                git -c filter.lfs.smudge= -c filter.lfs.required=false -c diff.mnemonicprefix=false -c core.quotepath=false clone --branch "${params.BRANCH_NAME}" --depth 1 --recursive "http://${GIT_CREDENTIALS}@${GitUrlWithoutHttp}" . &&
                git lfs install &&
                git lfs pull &&
                git submodule init &&
                git submodule sync --recursive &&
                git submodule update
            """
        } 
    }      
}

def UpdateContent() {
    echo "Params: ${params.SKIP_UPDATE_CONTENT}"

    if(params.SKIP_UPDATE_CONTENT == true) {
        echo "Skip Updating Content Code due to input parameters."
    } else {
        echo SwordGameContentAbsPath

        // 检查是否 SVN 目录存在
        if(!fileExists("${SwordGameContentAbsPath}"))
        {
            // SVN 目录不存在，需要创建
            if( params.FAST_SVN_CHECKOUT == true ) {
                sh"""
                    pwd
                    cp -r \"${ORIGIN_SWORDGAME_SVN_PATH}\" "${SwordGameContentAbsPath}"
                """
                UpdateContentSVN(SwordGameContentAbsPath, params.SVN_PATH)
            } else {
                dir("${BuildWorkingAbsPath}"){
                    // Do a fresh checkout
                    sh "svn checkout --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} ${params.SVN_PATH} Content"
                }
            }
        }
        else
        {
            // SVN 目录存在，直接更新
            UpdateContentSVN(SwordGameContentAbsPath, params.SVN_PATH)
        }
    }
}


def UpdateContentSVN(String absSvnDir, String inputSvnUrl) {

    dir("${absSvnDir}"){
                // Update Content
        echo "SVN cleanup "
        sh """
            echo cleanup...
            svn cleanup
            svn cleanup --remove-unversioned
            echo revert...
            svn revert -R .
        """

        // 分析当前的 SVN URL
        def currSvnUrl = sh script:"svn info --show-item url", returnStdout: true, encoding: 'UTF-8'
        boolean isSvnUrlValid
        String currSvnUrlHost
        String currSvnUrlBranch

        (isSvnUrlValid, currSvnUrlHost, currSvnUrlBranch) = GetSvnInfoFromSvnUrl(currSvnUrl)
        if(!isSvnUrlValid) {
            error("Current Svn Url is invalid! currSvnUrl: " + currSvnUrl)
        }

        // 计算输入 SVN 地址参数
        String inputSvnUrlHost
        String inputSvnUrlBranch

        (isSvnUrlValid, inputSvnUrlHost, inputSvnUrlBranch) = GetSvnInfoFromSvnUrl(inputSvnUrl)
        if(!isSvnUrlValid) {
            // SVN 分析失败
            error("Input SVN path is invalid! inputSvnUrl: " + inputSvnUrl)
        }

        if( currSvnUrlHost != inputSvnUrlHost) {
            // 需要 relocate
            sh """
                echo relocate...
                svn relocate ${currSvnUrlHost} ${inputSvnUrlHost} --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW}
            """
        }

        if( currSvnUrlBranch != inputSvnUrlBranch) {
            // 需要 switch
            // 注意下面的 ^/ 是必要的，否则会被 Git Bash 转义
            sh """
                echo switch...
                svn switch ^/${inputSvnUrlBranch} --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW}
            """
        }
               
        sh """
            echo update...
            svn update --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} .
        """
    }
}


def BuildNoahSDK() {
    if(params.SKIP_BUILD_CODE == true) {
        echo "Skip Build Code due to input parameters."
        return
    } 

    // some block
    dir(EngineAbsPath){
        sh 'pwd'

        echo "Building Engine..."

        sh 'devenv.com UE4.sln /build "Development Editor|Win64" /Project UnrealHeaderTool'

        sh 'Engine/Build/BatchFiles/build.bat UE4Editor Win64 Development'

    }

    echo "Building NoahSDK for Windows..."

    def NoahSDKLibNameList = ['NoahFoundation', 'NoahNetwork', 'NoahCore']
    for (int i = 0; i < NoahSDKLibNameList.size(); ++i) {

        def NoahSDKLibName = NoahSDKLibNameList[i]
        echo "Building ${NoahSDKLibName}"

        ws("${SwordGameAbsPath}/Source/NoahSDK/${NoahSDKLibName}/VSBuild"){
            sh 'cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../'
            sh "devenv.com ${NoahSDKLibName}.sln //build \"Release|x64\" //project ALL_BUILD"
            sh "devenv.com ${NoahSDKLibName}.sln //build \"Release|x64\" //project INSTALL"
        }
    }
    if (Platform == PLATFORM_ANDROID) {
        dir(SwordGameCMakeModuleAbsPath){

            echo "building NoahSDK for Android"

            bat "WindowsAndroidBuild.bat"
        }
    }
}

def BuildNoahSDK_IOS() {
    if(params.SKIP_BUILD_CODE == true){
        echo "Skip Build NOAHSDK due to input parameters." 
        return
    }
    dir(SwordGameCMakeModuleAbsPath){

        sh "python MacBuild.py osx" 
        sh "python MacBuild.py ios"
    }
}

def BuildSwordGame() {
    if(params.SKIP_BUILD_CODE == true) {
        echo "Skip Build SwordGame due to input parameters."
        return
    }
    echo "Generate projectfiles for SwordGame"

    sh "${EngineAbsPath}/Engine/Binaries/DotNET/UnrealBuildTool.exe -projectfiles -project=\"${SwordGameUProjectAbsPath}\" -game -engine -progressx -ForceHeaderGeneration ${GPublisher} ${params.ADDITIONAL_UBT_PARAM}"

    sh "${EngineAbsPath}/Engine/Build/BatchFiles/Build.bat SwordGameEditor Win64 Development ${SwordGameUProjectAbsPath} -waitmutex -ForceHeaderGeneration ${GPublisher} ${params.ADDITIONAL_UBT_PARAM}"


    def SwordGameProjectList = ['UnrealLightmass', 'ShaderCompileWorker', 'UnrealPak']
    for (int i = 0; i < SwordGameProjectList.size(); ++i) {

        def SwordGameProject = SwordGameProjectList[i]
        echo "Building ${SwordGameProject}"

    sh "devenv.com ${SwordGameSolutionAbsPath} //build \"Development Editor|Win64\" //Project ${SwordGameProject}"
    }

    echo "Building BuildPatchTool"
    sh "devenv.com ${SwordGameSolutionAbsPath} //build \"Shipping|Win64\" //Project BuildPatchTool"

    if ( Platform == PLATFORM_WINDOWS) {
        echo "Building SwordGame"
        sh "${EngineAbsPath}/Engine/Build/BatchFiles/Build.bat SwordGame Win64 ${GCompileFlag} ${SwordGameUProjectAbsPath} -waitmutex -ForceHeaderGeneration ${GPublisher}"
    }
}

def BuildEngine(){

    if (Platform == PLATFORM_IOS){
        PrepareIOSProvision(params.SIGNING)
        GenerateXCodeProjectFiles()
    }

    if(params.SKIP_BUILD_CODE == true) {
        echo "Skip Build Engine due to input parameters."
        return
    }

    dir(EngineAbsPath){

        sh "./GenerateProjectFiles.sh"
        sh "xcodebuild -workspace UE4.xcworkspace -scheme UE4"
        sh "xcodebuild -workspace UE4.xcworkspace -scheme ShaderCompileWorker"
        sh "xcodebuild -workspace UE4.xcworkspace -scheme UnrealPak"
        sh "xcodebuild -workspace UE4.xcworkspace -scheme BuildPatchTool -configuration Shipping"
        sh "${EngineAbsPath}/Engine/Build/BatchFiles/Mac/Build.sh SwordGameEditor Mac Development \"${SwordGameUProjectAbsPath}\" -ForceHeaderGeneration ${GPublisher} ${params.ADDITIONAL_UBT_PARAM}"
    }
    dir(SwordGameAbsPath){

        sh "file Config/DefaultGame.ini | grep -i utf-8"
        sh "file Config/DefaultEngine.ini | grep -i utf-8"

    }
}

def ExportDTandMaps() {
    if(params.SKIP_EXPORT_TABLES == true) {
        echo "Skip Exporting Tables due to input parameters."
        return
    }

    echo "export tab files"
    sh "${EngineEditorAbsPath} ${SwordGameUProjectAbsPath} -run=ExportTabFiles -DisableCache -CombineFile -stdout -culture=en -buildmachine ${GPublisher}"
    if (params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild"){
        sh "mv ${GSwordGameLogAbsPath} ${GSwordGameExportTableLogAbsPath}"
    }
    

    echo "export map information"
    sh "${EngineEditorAbsPath} ${SwordGameUProjectAbsPath} -run=SwordToolbarButton -stdout -culture=en -buildmachine ${GPublisher}"
    if (params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild"){
        sh "mv ${GSwordGameLogAbsPath} ${GSwordGameExportMapLogAbsPath}"
    }

}

def PreparePackage(){

    //package信息文件


    ws("${SwordGameContentAbsPath}/GameData/Common"){
        sh "touch PackageInfo.lua"
        SIGNING = ""
        if (Platform == PLATFORM_IOS){
            SIGNING = "SIGNING = \"${params.SIGNING}\""
        }else{
            SIGNING = ""
        }
        packageinfo ="""\
local packageinfo=
{
"""

        for (param in params) {
            packageinfo += "    " + param.key + " = " + "\"${param.value}\"" + ",\n"
        }

        packageinfo += """\
    BUILD_TAG = "${BUILD_TAG}",
    ${SIGNING}
}
return packageinfo\
        """

        writeFile(file: "${SwordGameContentAbsPath}/GameData/Common/PackageInfo.lua", text: packageinfo)
        sh "cat ${SwordGameContentAbsPath}/GameData/Common/PackageInfo.lua"
    }

    if(params.VERSION_OVERRIDE != null && params.VERSION_OVERRIDE != '') {

        writeFile(file: "${SwordGameContentAbsPath}/GameData/Common/ShortVersion.txt", text: GShortVersionOverride)
        writeFile(file: "${SwordGameContentAbsPath}/GameData/Common/LongVersion.txt", text: GLongVersionOverride)

        echo "Verison Is Override ${params.VERSION_OVERRIDE}"
    } 

    GShortVersion = readFile("${SwordGameContentAbsPath}/GameData/Common/ShortVersion.txt")
    GLongVersion = readFile("${SwordGameContentAbsPath}/GameData/Common/LongVersion.txt")

    echo "GShortVersion: " + GShortVersion
    echo "GLongVersion: " + GLongVersion

    if(params.VERSION_OVERRIDE != null && params.VERSION_OVERRIDE != '') {
        echo "Change CommonConfig.lua Version Value: GMajorVersionOverride = ${GMajorVersionOverride}, GMinorVersionOverride = ${GMinorVersionOverride}, GLogicVersionOverride = ${GLogicVersionOverride}, GBuildVersionOverride = ${GBuildVersionOverride}"
        ws("${SwordGameContentAbsPath}/GameData/Common") {
            CommonConfigFileName = "CommonConfig.lua"
            def ClientCommonConfig = readFile(CommonConfigFileName) 
            def FileContentList = ClientCommonConfig.tokenize('\n')
            def Textmessage = ""

            def MajorVersionReg = /MajorVersion = \S*,/
            def MajorVersionFindText = FileContentList.findAll{it =~ "${MajorVersionReg}"}[0]
            ClientCommonConfig = ClientCommonConfig.replaceAll("${MajorVersionFindText}", "\t\tMajorVersion = ${GMajorVersionOverride},")

            def MinorVersionReg = /MinorVersion = \S*,/
            def MinorVersionFindText = FileContentList.findAll{it =~ "${MinorVersionReg}"}[0]
            ClientCommonConfig = ClientCommonConfig.replaceAll("${MinorVersionFindText}", "\t\tMinorVersion = ${GMinorVersionOverride},")

            def LogicVersionReg = /LogicVersion = \S*,/
            def LogicVersionFindText = FileContentList.findAll{it =~ "${LogicVersionReg}"}[0]
            ClientCommonConfig = ClientCommonConfig.replaceAll("${LogicVersionFindText}", "\t\tLogicVersion = ${GLogicVersionOverride},")

            def BuildVersionReg = /BuildVersion = \S*,/
            def BuildVersionFindText = FileContentList.findAll{it =~ "${BuildVersionReg}"}[0]
            ClientCommonConfig = ClientCommonConfig.replaceAll("${BuildVersionFindText}", "\t\tBuildVersion = ${GBuildVersionOverride},")
            
            writeFile(file: "${CommonConfigFileName}", text: ClientCommonConfig)
        }
    }

    // 计算 Patch 和 Chunk 相关路径

    // 分支对应的云Chunk根目录
    // chunk/BuddyBuild/JXda9339
    GCloudBranchPath = "chunk/${params.BuildType}/${GBranchWorkingFolderName}"
    if(params.USE_EXTRA_CLOUD_PATH) {
        // 增加额外路径，避免同分支内容互相影响
        // chunk/BuddyBuild/JXda9339/GRobotFix
        GCloudBranchPath = GCloudBranchPath + "/" + params.WORK_NAME
    }
    echo "GCloudBranchPath: " + GCloudBranchPath

    // 分支对应的区分版本的Chunk目录
    // Patch/V3.21
    GPatchPatchVersionPrefix = "Patch/V${GShortVersion}"
    echo "GPatchPatchVersionPrefix: " + GPatchPatchVersionPrefix

    // 带版本号和平台名的 Chunk 目录相对路径
    // Patch/V3.21/Patch/Android
    GPatchPathWithVersion = "${GPatchPatchVersionPrefix}/Patch/${GChunkPlatformName}"
    echo "GPatchPathWithVersion: " + GPatchPathWithVersion

    // 本地 Patch 压缩用临时路径
    // CloudDir/BuddyBuild/JXda9339/Patch/V3.21/Patch/Android
    GLocalPatchZipDir = "CloudDir/${GPatchPathWithVersion}"
    echo "GLocalPatchZipDir: " + GLocalPatchZipDir

    // 腾讯云 CDN 域名
    GCloudUrl = "https://jgx-download.kingsoft.com"

    // 尽早计算包名
    PreparePackageNames(GLongVersion)

    if(Platform == PLATFORM_ANDROID) {
        //修改 StoreVersion
        def StoreVersion = ParseStoreVersion()
        println StoreVersion
        StoreVersionParameter = " -ini:Engine:[/Script/AndroidRuntimeSettings.AndroidRuntimeSettings]:StoreVersion=${StoreVersion}"
        // 修正 Android App 显示版本号
        GAddPackageParameter = GAddPackageParameter + StoreVersionParameter + " -ini:Engine:[/Script/AndroidRuntimeSettings.AndroidRuntimeSettings]:VersionDisplayName=${GLongVersion}"

        echo "GAddPackageParameter: " + GAddPackageParameter
    }

    echo "Generating AdditionalAlwaysCookFiles"

    if (Platform == PLATFORM_ANDROID || Platform == PLATFORM_WINDOWS) {
        sh "${SwordGameCMakeModuleAbsPath}/bin/lua53/lua53.exe ${SwordGameContentAbsPath}/GameData/DynamicAddCook.lua"
        sh "cd ${EngineAbsPath}/Engine/Programs/AutomationTool/Saved/Logs/ && rm -rf Cook-*"
    }else {
        sh "${SwordGameCMakeModuleAbsPath}/bin/lua53/lua53 ${SwordGameContentAbsPath}/GameData/DynamicAddCook.lua"
        sh "cd ${EngineAbsPath}/Engine/Programs/AutomationTool/Saved && rm -rf Cook-*"
    }

    if (Platform == PLATFORM_ANDROID || Platform == PLATFORM_IOS){
        if (params.BuildType != "PreReleaseBuild") {
            def ShaderPipelineVersion =  ""

            if ( fileExists("${SwordGameContentAbsPath}/GameData/Common/ShaderPipelineVersion.txt") == false){
                echo "cannot find file ShaderPipelineVersion.txt"
                ShaderPipelineVersion= "UnVersioned"
            }else{
                ShaderPipelineVersion = readFile("${SwordGameContentAbsPath}/GameData/Common/ShaderPipelineVersion.txt")
            }


            def ShaderPipelineCacheKey = "${GBranchWorkingFolderName}_${ShaderPipelineVersion}"
            

            echo "ShaderPipelineVersion"  +  ShaderPipelineVersion
            echo "Current ShaderPipelineCache Key is "+ShaderPipelineCacheKey
            echo "Current ShaderPipelineCache RemoteHttpServer=" + "${ShaderPipelineCacheServer}"

            sh "echo [AutoShaderPipelineCacheCollect] >> ${SwordGameAbsPath}/Config/DefaultEngine.ini"
            sh "echo -e\n >> ${SwordGameAbsPath}/Config/DefaultEngine.ini"
            sh "echo RemoteHttpServer=\\\"${ShaderPipelineCacheServer}\\\" >> ${SwordGameAbsPath}/Config/DefaultEngine.ini"
            sh "echo -e\n >> ${SwordGameAbsPath}/Config/DefaultEngine.ini"
            sh "echo ShaderPipelineCacheVersionString=${ShaderPipelineCacheKey} >> ${SwordGameAbsPath}/Config/DefaultEngine.ini"
        }
    
    }
    // def EfunPlistData=readFile("${SwordGameAbsPath}/Build/IOS/EfunPlistData.txt")
    if (params.PUBLISHER != 'None'){
        if (Platform == PLATFORM_IOS) {
            DefaultEngineRevise(params.PUBLISHER)
        }
        DefaultGameRevise(params.PUBLISHER)
    }

    RemoveUnusedStartupMovies(params.PUBLISHER)

    applicationDisplayName = ''
    packageName = ''
    pwd = ''
    alias = ''
    store = ''
    if (Platform == PLATFORM_ANDROID){
        if (params.PUBLISHER == 'eFun'){
            applicationDisplayName = "劍俠情緣2:劍歌行"
            packageName = "com.mover.twjxqy2"
            pwd = "efungoogle"
            alias = "efun"
            store = "efungame.keystore"
        }
        else{
            applicationDisplayName = "剑侠情缘2:剑歌行"
            packageName = "com.tencent.tmgp.jxqy2"
            pwd = "20121221"
            alias = "app"
            store = "SwordGame.jks"
        }
        dir(SwordGameAbsPath){
            sh "sed -i \'s/PackageName=.*\$/PackageName=${packageName}/g\' Config/DefaultEngine.ini"
            sh "sed -i \'s/ApplicationDisplayName=.*\$/ApplicationDisplayName=${applicationDisplayName}/g\' Config/DefaultEngine.ini"
            sh "sed -i \'s/KeyStore=.*\$/KeyStore=${store}/g\' Config/DefaultEngine.ini"
            sh "sed -i \'s/KeyAlias=.*\$/KeyAlias=${alias}/g\' Config/DefaultEngine.ini"
            sh "sed -i \'s/KeyPassword=.*\$/KeyPassword=${pwd}/g\' Config/DefaultEngine.ini"
            sh "sed -i \'s/KeyStorePassword=.*\$/KeyStorePassword=${pwd}/g\' Config/DefaultEngine.ini"
        }
    }
    // Compile LaunchScreen.storyboard to LaunchScreen.storyboardc
    // Hacking way for Storyboard support in iOS Packaging 
    if (Platform == PLATFORM_IOS){
        dir("${EngineAbsPath}/Engine/Build/IOS/Resources/Interface"){
            sh "ibtool --compile LaunchScreen.storyboardc LaunchScreen.storyboard"
        }
    }

}

def DefaultEngineRevise(String Publisher){
    def FileContent = readFile("${SwordGameAbsPath}/Config/DefaultEngine.ini")
    def EfunPlistData = readFile("${SwordGameAbsPath}/Build/IOS/EfunPlistData.txt")
    def TencentPlistData = readFile("${SwordGameAbsPath}/Build/IOS/TencentPlistData.txt")
    def FileContentList = FileContent.tokenize('\n')
    def Textmessage = ""

    x = /^AdditionalPlistData/
    def FindText = FileContentList.findAll{it =~ "${x}"}.join()
    if ( FindText == TencentPlistData || FindText == EfunPlistData){
        if (Publisher == "eFun"){
            Textmessage = FileContent.replaceAll("${FindText}", "${EfunPlistData}")
        }else if (Publisher == "Tencent"){
            Textmessage = FileContent.replaceAll("${FindText}", "${TencentPlistData}")
        }
    }else{
        println "DefaultEngine.ini was Manually modified"
        sh "exit 1"
    }

    writeFile(file: "${SwordGameAbsPath}/Config/DefaultEngine.ini", text: "${Textmessage}", encoding: "UTF-8")
    sh "cat \"${SwordGameAbsPath}/Config/DefaultEngine.ini\""
}

def DefaultGameRevise(String Publisher){
    echo "DefaultGameRevise : " + Publisher
    def FileContent = readFile("${SwordGameAbsPath}/Config/DefaultGame.ini")

    if (Publisher == "eFun"){
        FileContent = FileContent.replaceAll("${TencentMovieName}", "${EFunMovieName}")
    }else if (Publisher == "Tencent"){
        FileContent = FileContent.replaceAll("${EFunMovieName}", "${TencentMovieName}")
    }

    writeFile(file: "${SwordGameAbsPath}/Config/DefaultGame.ini", text: "${FileContent}", encoding: "UTF-8")
    sh "cat \"${SwordGameAbsPath}/Config/DefaultGame.ini\""
}

def RemoveUnusedStartupMovies(String Publisher){
    dir("${SwordGameAbsPath}/Content/Movies") {
        if (Publisher == "eFun"){
            sh "rm -f ${TencentMovieName}*"
        }else if (Publisher == "Tencent"){
            sh "rm -f ${EFunMovieName}*"
        }
    }
}

// 拼接 Cook 参数串
// baseVersion: 不为空时，表示打 Patch 并且使用作为 BaseVersion
String BuildCookParameters(String baseVersion = "") {

    echo "baseVersion: " + baseVersion

    // 平台无关的固定 Cook 参数
    String platformIndependentCookParameters = "-ScriptsForProject=${SwordGameUProjectAbsPath} BuildCookRun -culture=en -nocompileeditor -nop4 -project=${SwordGameUProjectAbsPath} -clientconfig=${GCompileFlag} -stage -prereqs -compressed -nodebuginfo -CrashReporter -ForceHeaderGeneration -utf8output -compile -skipeditorcontent -pak ${GExtraPackageArg} ${GDistributionFlag} ${GPublisher} ${params.ADDITIONAL_UBT_PARAM} -manifests -createchunkinstall -chunkinstalldirectory=${SwordGameAbsPath}/ChunkInstall/ -chunkinstallversion=${GLongVersion} ${GAddPackageParameter} -build" 

    echo "platformIndependentCookParameters: " + platformIndependentCookParameters

    String patchUrlString = ""

    if(params.UPLOAD_TENCENT_CLOUD) {
        // 计算 腾讯云 URL 地址
        // chunk/BuddyBuild/JXda9339/Patch/V3.21
        patchUrlString = "${GCloudUrl}/${GCloudBranchPath}/${GPatchPatchVersionPrefix}"
                                                                                            
        echo "Tencent Cloud Patch Url: " + patchUrlString
    }

    if(params.PATCH_URL != ""){
        // 已经设置了 Patch Url， 则强制使用 Patch Url 
        echo "PATCH_URL is not empty: " + params.PATCH_URL
        patchUrlString = params.PATCH_URL
    }

    String patchUrlFullCmdString = ""
    if ( patchUrlString != "") {
        patchUrlFullCmdString = "-PatchURL_Dis=${patchUrlString} -PatchURL_Dev=${patchUrlString}"
    }

    String publisherCmdString = ""
    if (params.PUBLISHER == "eFun"){
        publisherCmdString = "-publisher=efun"
    }

    // DailyBuild 使用的更新 URL
    String dailybuildCmdLineParameters = "-addcmdline=\"-Patch ${patchUrlFullCmdString} ${params.AdditionCommandLine} ${publisherCmdString} \""
    // PreReleaseBuild 使用的 更新 URL 及特殊设置
    String preReleaseCmdLineParameters = "-addcmdline=\"DisableAllScreenMessages -NoConsole -Patch ${patchUrlFullCmdString} ${params.AdditionCommandLine} ${publisherCmdString} \""
    // BuddyBuild 使用Login URL
    String buddybuildCmdLineParameters = "-addcmdline=\"${patchUrlFullCmdString} ${params.AdditionCommandLine} ${publisherCmdString} \""

    // 制作基础版本的参数 
    // BuddyBuild 无效
    String baseVersionParameters = "-unversionedcookedcontent"
    // 制作Patch版本的参数
    // BuddyBuild 无效
    String patchVersionParameters = ""
    
    //patch 去掉build和package参数
    String packageBuildParameters = ""

    if(baseVersion != "") {
        // 如果传入 baseVersion 则表示打 Patch
        patchVersionParameters = "-generatepatch -newpatchlevel -basedonreleaseversion=${baseVersion}"
        packageBuildParameters = ""
    } else {
        // 没有传入 baseVersion 则表示打整包
        patchVersionParameters = "-createreleaseversion=${GLongVersion}"
        packageBuildParameters = "-package"
    }

    String cookParameter

    // 平台相关参数
    if (Platform == PLATFORM_ANDROID){
        cookParameter = platformIndependentCookParameters + " -ue4exe=UE4Editor-Cmd.exe -targetplatform=Android -cookflavor=ASTC"

        // 32Bit 的 Android 打包需要调整打包参数
        if(params.USE_32BIT){
            cookParameter = cookParameter + " -ini:Engine:[/Script/AndroidRuntimeSettings.AndroidRuntimeSettings]:bBuildForArmV7=True -ini:Engine:[/Script/AndroidRuntimeSettings.AndroidRuntimeSettings]:bBuildForArm64=False"
        }

    } else if (Platform == PLATFORM_IOS){
        cookParameter = platformIndependentCookParameters + " -ue4exe=UE4Editor -targetplatform=IOS"

        // 禁用文件共享功能
        if(params.DISABLE_FILESHARING == true) {
            cookParameter = cookParameter + "  -ini:Engine:[/Script/IOSRuntimeSettings.IOSRuntimeSettings]:bDisableFileSharing=True"
        }

        if(!GiOSWithAppleSignInEnabled){
            cookParameter = cookParameter + "  -ini:Engine:[/Script/IOSRuntimeSettings.IOSRuntimeSettings]:bEnableSignInWithAppleSupport=False"
        }

    } else {
        // Windows
        cookParameter = platformIndependentCookParameters + " -ue4exe=UE4Editor-Cmd.exe -targetplatform=Win64 -bootstraprhi=d3d11 -bootstrapfeaturelevel=featureleveles31"
    }

    echo "After add platform parameters, cookParameter: " + cookParameter

    // Dailybuild 和 PreReleaseBuild 打开版本生成
    if (params.BuildType == "DailyBuild") {
        cookParameter = cookParameter + " " + baseVersionParameters + " " + dailybuildCmdLineParameters + " " + patchVersionParameters
    } else if (params.BuildType == "PreReleaseBuild") {
        cookParameter = cookParameter + " " + baseVersionParameters + " " + preReleaseCmdLineParameters + " " + patchVersionParameters
    } else {
        cookParameter = cookParameter + " " + baseVersionParameters + " " + buddybuildCmdLineParameters + " " + patchVersionParameters
    }

    echo "After add package type parameters, cookParameter: " + cookParameter

    if(GAlreadyCooked) {
        cookParameter = cookParameter + " -skipcook"
    } else {
        cookParameter = cookParameter + " " + GCookOrSkipFlag
    }

    cookParameter = cookParameter + " " + packageBuildParameters

    echo "Final cookParameter: " + cookParameter

    return cookParameter
}

def CompileLua(){

    // Lua Compiler 参数
    def LuaCompileArgs = ""

    if(params.LUA_COMPILER == false) {
        echo "Skip Compiling Lua due to input paramaters"
        return
    }

    if(params.LUA_CONFUSION == true) {
        // 混淆参数
        LuaCompileArgs = " -s "
    }

    echo "Dump Scripts directory" 

    dir("${SwordGameAbsPath}") {
        // Remove old folder
        sh returnStatus: true, script: 'rm -rf Scripts.back'
        sh "cp -R -f Scripts/ Scripts.back"
    }
    
    if(Platform == PLATFORM_IOS){
        dir("${SwordGameAbsPath}/Source/ThirdParty/LuaCompiler/LuaMacOS_64"){
            sh "chmod +x luac.sh"
            sh "chmod +x luac"
            sh "./luac.sh ${LuaCompileArgs}"
        }
    } else if(Platform == PLATFORM_ANDROID && params.USE_32BIT){
        // 如果打包 32Bit 的 Android 则，使用 32 位的 Luac
        dir("${SwordGameAbsPath}/Source/ThirdParty/LuaCompiler/LuaWin_32"){                            
            sh "./luac.bat ${LuaCompileArgs}"    
        }
    } else {
        // PLATFORM_ANDROID 和 PLATFORM_WINDOWS 都默认使用 64 位的 Luac
        dir("${SwordGameAbsPath}/Source/ThirdParty/LuaCompiler/LuaWin_64"){                            
            sh "./luac.bat ${LuaCompileArgs}"    
        }        
    }
}

def PackagePatch(){
    if ( params.PATCH == false ){
        echo "Skip patch due to input params"
        return
    }

    if (params.BuildType == "PreReleaseBuild"){
        // PreReleaseBuild 需要手动填写版本号，与 Dailybuild 不同
        if ( params.BASEVERSION == ""){
            println "填写BASEVERSION"
            exit -1
           
        } else {
            def strList = params.BASEVERSION.split("\\.")
            String shortVersionFromBaseVersion = strList[0] + '.' + strList[1]

            if ( shortVersionFromBaseVersion == GShortVersion && params.BASEVERSION != GLongVersion) {

                
                CompileLua()
                // TODO: 与下方基本相同，需要合并
                String cookParameters = BuildCookParameters(params.BASEVERSION)
                RunUAT(cookParameters)

                //Prerelease Patch zip
                ZipCloudDir(GPatchZipFileAbsPath)

                if(params.UPLOAD_TENCENT_CLOUD) {
                    // 需要上传腾讯云，没有需要跳过的文件
                    UploadChunkToTencentCloud('', true)
                    // 标记 Patch 上传过腾讯云
                    GAlreadyUploadPatchToCloud = true
                }

            } else {
                echo "Patch version error! GShortVersion=${GShortVersion} GLongVersion=${GLongVersion} shortVersionFromBaseVersion=${shortVersionFromBaseVersion}"
                sh "exit 1"
            }
        }
        return
    }

    // DailyBuild 逻辑，自动判断
    // TODO: 需要考虑 DailyBuild 和 BuddyBuild
    if ( fileExists("${PreVersionAbsPath}") == true ){
        String baseVersion = readFile("${PreVersionAbsPath}")
        def strList = baseVersion.split('\\.')
        def shortVersionFromBaseVersion = strList[0]+'.'+strList[1]
        echo "PreVersion: " + baseVersion


        if ( GShortVersion != shortVersionFromBaseVersion ){
            // 大版本号变化
            println "Skip Patch because significant version changes."
            return
        }

        if ( baseVersion == GLongVersion ){
            // 与上个版本号相同
            println "Skip Patch because current version is the same as last build."
            return
        }

        println "Making patch..."

        CompileLua()

        String cookParameters = BuildCookParameters(baseVersion)
        RunUAT(cookParameters)

        // DailyBuild Patch zip
        ZipCloudDir(GPatchZipFileAbsPath)

        if(params.UPLOAD_TENCENT_CLOUD) {
            // 需要上传腾讯云，没有需要跳过的文件
            UploadChunkToTencentCloud('', true)
            // 标记 Patch 上传过腾讯云
            GAlreadyUploadPatchToCloud = true
        }

    }
}


def RunUAT(String cookParameters) {

    echo "RunUAT cookParameters: " + cookParameters

    if ( Platform == PLATFORM_ANDROID || Platform == PLATFORM_WINDOWS){
        dir("${EngineAbsPath}/Engine/Build/BatchFiles"){
            sh "./RunUAT.bat ${cookParameters} ${params.ADDITIONAL_UAT_PARAM}"
        }
    }else if( Platform == PLATFORM_IOS){
        dir("${EngineAbsPath}/Engine/Build/BatchFiles"){
            sh "./RunUAT.sh ${cookParameters} ${params.ADDITIONAL_UAT_PARAM}"
        }
    }

    try{
        if (params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild"){
            if(! (GAlreadyCooked || params.SKIP_COOK) ) {
                // 既不是 cook 过，也没有 skip cook ，则保留 Cook log
                if ( Platform == PLATFORM_IOS ){
                // Mac 上引擎 Cook 文件路径中没有 /Logs
                    sh "cd ${EngineAbsPath}/Engine/Programs/AutomationTool/Saved && mv \$(ls -t Cook-* | head -n 1)  ${GSwordGameCookLogAbsPath}"
                    
                } else {
                    sh "cd ${EngineAbsPath}/Engine/Programs/AutomationTool/Saved/Logs/ && mv \$(ls -t Cook-* | head -n 1)  ${GSwordGameCookLogAbsPath}"
                }
            }
        }
    } catch (any){
        currentBuild.result = 'UNSTABLE'
        println "Upload Cooklog failed"
    }

    // 设置已经 Cook 过标记位
    GAlreadyCooked = true

}



// 如果文件存在，则删除
def DeleteFileIfExist(String fileAbsPath, boolean ignoreFail = true) {
    if (fileExists(fileAbsPath)) {
        sh label: 'Remove file', returnStatus: ignoreFail, script: "rm -rf \"${fileAbsPath}\" "
    }
}

def CleanupPackage(){

    if( params.LUA_COMPILER == false ) {
        echo "Skip Compiling Lua due to input paramaters"
        return
    }

    echo "Recover Script directory"
    
    dir("${SwordGameAbsPath}"){
    
        sh """
            rm -d -f -r "./Scripts/"
            mv Scripts.back Scripts
        """
    }
}


def BuildPatchTool(){
    if (params.ADDRESOURCE_PACKAGE == false){
        println "ADDRESOURCE_PACKAGE == ${params.ADDRESOURCE_PACKAGE}" 
        return
    }
    def ChunkPatchZipFileAbsPath = "${SwordGameInstallerAbsPath}/ChunkDir.zip" 

    if (Platform == PLATFORM_ANDROID){
        GPakChunkAbsPath = "${SwordGameAbsPath}/ChunkInstall/Android_ASTC/${GLongVersion}/pakchunk3"
    }else if( Platform == PLATFORM_WINDOWS){
        GPakChunkAbsPath = "${SwordGameAbsPath}/ChunkInstall/WindowsNoEditor/${GLongVersion}/pakchunk3"
    }else{
        GPakChunkAbsPath = "${SwordGameAbsPath}/ChunkInstall/IOS/${GLongVersion}/pakchunk3"
    }

    println GPakChunkAbsPath
    if (Platform == PLATFORM_ANDROID || Platform == PLATFORM_WINDOWS){
        sh """
            ${EngineAbsPath}/Engine/Binaries/Win64/BuildPatchTool.exe -BuildRoot="${GPakChunkAbsPath}" -CloudDir="${GPatchFolderAbsPath}" -AppID=1 -AppName=\"SwordGame_DownloadContent" -BuildVersion="${GLongVersion}" -BaseVersion=\"${GLongVersion}\" -AppLaunch="" -AppArgs="" -custom="bIsPatch=false" -customint="ChunkID=3" -customint="PakReadOrdering=0" -stdout
        """
    }else if(Platform == PLATFORM_IOS){
        sh """
            ${EngineAbsPath}/Engine/Binaries/Mac/BuildPatchTool -BuildRoot="${GPakChunkAbsPath}" -CloudDir="${GPatchFolderAbsPath}" -AppID=1 -AppName=\"SwordGame_DownloadContent" -BuildVersion="${GLongVersion}" -BaseVersion=\"${GLongVersion}\" -AppLaunch="" -AppArgs="" -custom="bIsPatch=false" -customint="ChunkID=3" -customint="PakReadOrdering=0" -stdout
        """
    }

    //Chunk 分包 zip
    ZipCloudDir(ChunkPatchZipFileAbsPath)
}


def Package() {
    if (params.SKIP_Package == true) {
        echo "Skip Package "
        return
    }
    
    CompileLua()
    
    String cookParameters = BuildCookParameters()
    RunUAT(cookParameters)

    // TODO: Android 版本 在 Dailybuild 上导出 Cook 分析，不支持其他平台
    if (Platform == PLATFORM_ANDROID && params.BuildType == "DailyBuild"){
        DeleteFileIfExist("${SwordGameAbsPath}/Saved/CookOutputCSVs")
        sh "${EngineEditorAbsPath} ${SwordGameUProjectAbsPath} OutputCookedCSVCommandlet -targetplatform=Android_ASTC -stdout -culture=en -buildmachine ${GPublisher}"
    }
    if (params.PATCH == false && params.BuildType == "PreReleaseBuild"){
        //PreRelease 第一个整包 SwordGameVesion zip
        ZipCloudDir(GPatchZipFileAbsPath)
        // zip dir: GPatchFolderAbsPath, glob: '', zipFile: GPatchZipFileAbsPath
    }
    BuildPatchTool()
}

def MakeWinInstaller() {

    // echo LoginUrLString
    bat "makensis.exe /V4 /DPRODUCT_VERSION=${GLongVersion} /DCMDLINE=\"${params.AdditionCommandLine}\" ${SwordGameInstallerAbsPath}/Sword_Installer.nsi"

}

def PrepareIOSProvision(String Signing) {
    def provisionFile  = ""
    def bundleID
    def signingCertificate =""
    def bundleName = "剑侠情缘2:剑歌行"
    GiOSWithAppleSignInEnabled = true

    if (Signing == "AdHoc") {
        // 腾讯 AdHoc 签名，需要特定手机安装
        bundleID = "com.tencent.jxqy2jgx"
        provisionFile = "jxqy2_adhoc.mobileprovision"
        signingCertificate = "iPhone Distribution: Shenzhen Tencent Computer Systems Company Limited (9TV4ZYSS4J)"
    } else if (Signing == "AppStore") {
        // 腾讯 AppStore 签名，无法安装，只能用于上架 AppStore
        bundleID = "com.tencent.jxqy2jgx"
        provisionFile = "jxqy2.mobileprovision"
        signingCertificate = "iPhone Distribution: Shenzhen Tencent Computer Systems Company Limited (9TV4ZYSS4J)"
    } else if (Signing == "TencentDev") {
        // 腾讯 Dev 签名，可以调试，需要特定手机安装
        bundleID = "com.tencent.jxqy2jgx"
        provisionFile = "jxqy2_dev.mobileprovision"
        signingCertificate = "iPhone Developer: Created via API (JDPXHYVWYZ)"
    } else if (Signing == "ROGInHouse") {
        // RoG InHouse 签名
        bundleID = "cn.com.xishanju.relicsofgods"
        provisionFile = "relicsofgods_for_testing.mobileprovision"
        signingCertificate = "iPhone Distribution: Westhouse Corporation Limited"
        // 企业签名不支持 Apple SignIn
        GiOSWithAppleSignInEnabled = false
    } else if (Signing == "Tako") {
        // Tako InHouse 签名
        bundleID = "com.seasun.jxqy2.tako"
        provisionFile = "Tako.mobileprovision"
        signingCertificate = "iPhone Distribution: Kingsoft (M) Sdn Bhd"
        // 企业签名不支持 Apple SignIn
        GiOSWithAppleSignInEnabled = false
    } else if (Signing == "ROGDevelopment") {
        bundleID = "cn.com.xishanju.jxqy2"
        provisionFile = "WentaoXia.mobileprovision"
        signingCertificate = "Apple Development: 赵 德禄 (P65Q9QKYL6)"
        // 暂时不支持 Apple SignIn
        GiOSWithAppleSignInEnabled = false
    } else if (Signing == "eFunAdhoc") {
        bundleID = "com.movergames.twjxqy2"
        provisionFile = "eFunAdHoc.mobileprovision"
        signingCertificate = "iPhone Distribution: Mover Games Limited (V8855KPN73)"
        bundleName = "劍俠情緣2:劍歌行"
    } else if (Signing == "eFunDistribution") {
        bundleID = "com.movergames.twjxqy2"
        provisionFile = "eFunDistribution.mobileprovision"
        signingCertificate = "iPhone Distribution: Mover Games Limited (V8855KPN73)"
        bundleName = "劍俠情緣2:劍歌行"
    } else if (Signing == "eFunDevelopment") {
        bundleID = "com.movergames.twjxqy2"
        provisionFile = "eFunDevelopment.mobileprovision"
        signingCertificate = "iPhone Developer: Xiaoran Wang (95738NRN4X)"
        bundleName = "劍俠情緣2:劍歌行"
    }

    def forDistribution = "True"
    // GDistributionFlag 在打包开始时进行计算
    if(GDistributionFlag == "") {
        forDistribution = "False"
    }

    sh """
    cp "${SwordGameAbsPath}/Certificate/${provisionFile}" "/Users/\$USER/Library/MobileDevice/Provisioning Profiles/${provisionFile}"
    """

    dir(SwordGameAbsPath){
        sh "gsed -i \'s/ForDistribution=.*\$/ForDistribution=${forDistribution}/\' Config/DefaultGame.ini"
        sh "gsed -i \'s/BundleIdentifier=.*\$/BundleIdentifier=${bundleID}/\' Config/DefaultEngine.ini"
        sh "gsed -i \'s/BundleName=.*\$/BundleName=${bundleName}/\' Config/DefaultEngine.ini"
        sh "gsed -i \'s/BundleDisplayName=.*\$/BundleDisplayName=${bundleName}/\' Config/DefaultEngine.ini"
        sh "gsed -i \'s/MobileProvision=.*\$/MobileProvision=${provisionFile}/\' Config/DefaultEngine.ini"


        sh "gsed -i \'s/SigningCertificate=.*\$/SigningCertificate=${signingCertificate}/\' Config/DefaultEngine.ini"
        sh "gsed -i \'/SigningCertificate=.*\$/a\\bGeneratedSYMFile=True\' Config/DefaultEngine.ini"
        sh "gsed -i \'/SigningCertificate=.*\$/a\\bUseRSync=False\' Config/DefaultEngine.ini"

        sh "cat Config/DefaultEngine.ini"
    }
}

def GenerateXCodeProjectFiles() {
    if(params.GENERATE_PROJECTFILES != true) {
        echo "No Need to Generate XCode project files"
        return
    }

    echo "Generate XCode project files ..."
    
    // Cleaning up old files
    DeleteFileIfExist("${SwordGameAbsPath}/SwordGame.xcworkspace}")
    DeleteFileIfExist("${SwordGameAbsPath}/Intermediate/ProjectFiles")
   

    String engineMacGenProjectSHPath = EngineAbsPath + "/Engine/Build/BatchFiles/Mac"

    dir(engineMacGenProjectSHPath) {
        sh "./GenerateProjectFiles.sh -project=\"${SwordGameUProjectAbsPath}\" -game"

        def BuildScriptHeader = """#!/bin/sh
        export CUSTOM_CMAKE_MODULE=${SwordGameCMakeModuleAbsPath}
        export THIRDPARTY_PATH=${SwordGameAbsPath}/Source/ThirdParty
        export SWORDGAME_HOME=${SwordGameAbsPath}

        """
        
        String buildScriptContent = BuildScriptHeader + readFile("${engineMacGenProjectSHPath}/Build.sh")

        echo buildScriptContent

        writeFile(file: "${SwordGameAbsPath}/Build.sh", text: buildScriptContent)


        sh """
        sed -ig "s+buildToolPath = \\\"${EngineAbsPath}\\/Engine\\/Build\\/BatchFiles\\/Mac\\/Build.sh\\\";+buildToolPath = \\\"${SwordGameAbsPath}\\/Build.sh\\\";+g" /${SwordGameAbsPath}/Intermediate/ProjectFiles/SwordGame.xcodeproj/project.pbxproj
        chmod +x ${SwordGameAbsPath}/Build.sh
        """
    }	
}   

// 计算包名
def PreparePackageNames(String packageVersion) {

    //命名
    echo "Preparing Package Names"

    def packageLastName = ""
    def symbolLastName = ""
    def signingName = ""
    def androidBitName = ""
    // 计算平台名

    // 计算包名
    def packageTopName = "SwordGame_${GPlatformName}"

    if ( Platform == PLATFORM_ANDROID){
        packageLastName = "apk"
        symbolLastName = "so"
        GChunk3PakOriginalName = "pakchunk3-Android_ASTC.pak"
        if ( params.USE_32BIT == true ){
            androidBitName = "_32Bit"
        }
    } else if ( Platform == PLATFORM_IOS){
        packageLastName = "ipa"
        symbolLastName = "dSYM"
        GChunk3PakOriginalName = "pakchunk3-ios.pak"
        signingName = "${params.SIGNING}"
    } else {
        packageLastName = "exe"
        symbolLastName = "pdb"
        GChunk3PakOriginalName = "pakchunk3-WindowsNoEditor.pak"
    }



    Date currentBuildTime = new Date(currentBuild.startTimeInMillis)
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd_HHmm")
    def currentTime = dateFormatter.format(currentBuildTime)

    def packageBaseName

    GPackagePatchName = "Cloud_${GPlatformName}_${GCompileFlag}${androidBitName}_${params.BuildType}_${params.PUBLISHER}_V${packageVersion}_${currentTime}.zip"
    GReleasesBackupName = "Releases_${GPlatformName}_${GCompileFlag}${androidBitName}_${params.BuildType}_${params.PUBLISHER}_V${packageVersion}_${currentTime}.zip"

    echo "Patch package name is: " + GPackagePatchName

    if ( params.BuildType == "BuddyBuild" && Platform == PLATFORM_IOS){
        // iOS 需要 SigningName
        packageBaseName = "${packageTopName}_${signingName}_${GCompileFlag}_${params.WORK_NAME}_${params.PUBLISHER}_V${packageVersion}_${currentTime}"
    } else if(params.BuildType == "BuddyBuild" && (Platform == PLATFORM_ANDROID || Platform == PLATFORM_WINDOWS) ){
        packageBaseName = "${packageTopName}_${params.WORK_NAME}_${GCompileFlag}${androidBitName}_${params.PUBLISHER}_V${packageVersion}_${currentTime}"
    } else {
        // DailyBuild 不需要 WORK_NAME
        packageBaseName = "${packageTopName}_${GCompileFlag}${androidBitName}_${params.BuildType}_${params.PUBLISHER}_V${packageVersion}_${currentTime}"

        // DailyBuild 需要记录 Cook Output @linhuiying
        GCookOutputName = "${packageTopName}_${params.BuildType}_${params.PUBLISHER}_V${packageVersion}_${currentTime}"
    }

    GPackageName = "${packageBaseName}.${packageLastName}"
    GSymbolFileName = "${packageBaseName}.${symbolLastName}"
    GChunk3PakPackageName = "${packageBaseName}.pak"
    GChunkPatchName = "${packageBaseName}.zip"
    GNsisbinFileName = "${packageBaseName}.nsisbin"
    GManifestFileName = "${packageBaseName}.manifest"

    // 重新定义 Log 的名字，用于上传到 FTP
    GCookLogUploadName = "CookLog_${GPlatformName}_${params.BuildType}_V${packageVersion}_${currentTime}.txt"
    GTablesLogUploadName = "TablesLog_${GPlatformName}_${params.BuildType}_V${packageVersion}_${currentTime}.txt"
    GMapLogUploadName = "MapLog_${GPlatformName}_${params.BuildType}_V${packageVersion}_${currentTime}.txt"

}


def UploadToFTP() {
    if (params.SKIP_UPLOADFTP == true){
        echo "SKIP_UPLOADFTP"
        return
    }

    echo "Uploading to FTP ..."

    // 上传 Shader Cache 文件
    if ( Platform == PLATFORM_ANDROID && params.BuildType == "DailyBuild"){
        dir ("${SwordGameAbsPath}/Saved/Shaders/GLSL_ES2"){
            sh "cp ShaderStableInfo-SwordGame-GLSL_ES2.scl.csv ${GBranchWorkingFolderName}_ShaderStableInfo-SwordGame-GLSL_ES2.scl.csv"

            sh "cp ShaderStableInfo-Global-GLSL_ES2.scl.csv ${GBranchWorkingFolderName}_ShaderStableInfo-Global-GLSL_ES2.scl.csv "

            PutToFtp('ShenZhenFTP', "ShaderCacheServer/ShaderMaterialMapping", "${GBranchWorkingFolderName}_ShaderStableInfo-SwordGame-GLSL_ES2.scl.csv", false, false)
            PutToFtp('ShenZhenFTP', "ShaderCacheServer/ShaderMaterialMapping", "${GBranchWorkingFolderName}_ShaderStableInfo-Global-GLSL_ES2.scl.csv", false, false)
        }
    }else if (Platform == PLATFORM_IOS && params.BuildType == "DailyBuild"){
        dir ("${SwordGameAbsPath}/Saved/Shaders/SF_METAL"){
            sh "cp ShaderStableInfo-SwordGame-SF_METAL.scl.csv ${GBranchWorkingFolderName}_ShaderStableInfo-SwordGame-SF_METAL.scl.csv"
            sh "cp ShaderStableInfo-Global-SF_METAL.scl.csv ${GBranchWorkingFolderName}_ShaderStableInfo-Global-SF_METAL.scl.csv"

            PutToFtp('ShenZhenFTP', "ShaderCacheServer/ShaderMaterialMapping", "${GBranchWorkingFolderName}_ShaderStableInfo-SwordGame-SF_METAL.scl.csv", false, false)
            PutToFtp('ShenZhenFTP', "ShaderCacheServer/ShaderMaterialMapping", "${GBranchWorkingFolderName}_ShaderStableInfo-Global-SF_METAL.scl.csv", false, false)

        }
    }

    // 上传 Cook Log
    if (params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild"){

        try{

            if (params.SKIP_EXPORT_TABLES == false) {
                sh "mv ${GSwordGameExportTableLogAbsPath} ${SwordGameAbsPath}/${GTablesLogUploadName}"
                sh "mv ${GSwordGameExportMapLogAbsPath} ${SwordGameAbsPath}/${GMapLogUploadName}"
                sh "mv ${GSwordGameCookLogAbsPath} ${SwordGameAbsPath}/${GCookLogUploadName}"
                dir("${SwordGameAbsPath}") {
                    PutToFtp('ShenZhenFTP', 'LogCollect/PackageLog', GTablesLogUploadName, false, true)
                    PutToFtp('ShenZhenFTP', 'LogCollect/PackageLog', GMapLogUploadName, false, true)
                    PutToFtp('ShenZhenFTP', 'LogCollect/PackageLog', GCookLogUploadName, false, true)
                }
            }

            // TODO： 移动到 Package 和 Cook 中，并同时保留 Patch Cook Log
            // TODO: 可以使用函数的形式来统一保留 Log
           
        } catch (any) {
            currentBuild.result = 'UNSTABLE'
            println "Upload UploadCooklog failed"
        }
    }

    //改名并上传CookOutput
    // 只有在 Android平台，且打整包的 Dailybuild 才有效
    if ( Platform == PLATFORM_ANDROID && params.BuildType == "DailyBuild" && params.SKIP_Package != true){
        try {
            dir ("${SwordGameAbsPath}/Saved/"){
                sh "mv CookedOutputCSVs ${GCookOutputName}"
            
                // /${GCookOutputName}/** 不能在此函数内删除
                PutToFtp('ShenZhenFTP', 'CookedOutputCSV', "/${GCookOutputName}/**", false, false)
                DeleteFileIfExist("${SwordGameAbsPath}/Saved/${GCookOutputName}")

            }
        }catch (any) {
            currentBuild.result = 'UNSTABLE'
            println "Upload CookedOutputCSVs failed"
        }
    }


    //上传安装包
    GSwordGamePackageAbsPath = "${SwordGameInstallerAbsPath}/${GPackageName}"
    SymbolFilePath = "${SwordGameInstallerAbsPath}/${GSymbolFileName}"

    if (params.SKIP_Package == true) {
        println "Skip UploadPackage"
    } else {

        if (Platform == PLATFORM_ANDROID){
            def ApkName = ""

            // Shipping 优先级最高
            if (params.SHIPPING == true) {
                ApkName="SwordGame-Android-Shipping-${GAndroidArchName}-es2"
            }else{
                if (params.FORCEDEBUG == true ){
                    ApkName="SwordGame-Android-Debug-${GAndroidArchName}-es2"
                } else {
                    ApkName="SwordGame-${GAndroidArchName}-es2"
                }
            }

            sh "mv ${SwordGameInstallerAbsPath}/${ApkName}.apk ${SwordGameInstallerAbsPath}/${GPackageName}"
            sh "mv ${SwordGameInstallerAbsPath}/${ApkName}.so ${SwordGameInstallerAbsPath}/${GSymbolFileName}"
        }else if(Platform == PLATFORM_WINDOWS){
            sh "mv ${SwordGameInstallerAbsPath}/setup_jx2.exe ${SwordGameInstallerAbsPath}/${GPackageName}"
            //如果存在nsisbin文件，证明分包了
            if (fileExists("${SwordGameInstallerAbsPath}/setup_jx2.nsisbin")){
                sh "mv ${SwordGameInstallerAbsPath}/setup_jx2.nsisbin ${SwordGameInstallerAbsPath}/${GNsisbinFileName}"
            }
            if (params.SHIPPING == false){
                sh "mv ${SwordGameBinaryAbsPath}/SwordGame.pdb ${SwordGameInstallerAbsPath}/${GSymbolFileName}"
            }else{
                sh "mv ${SwordGameBinaryAbsPath}/SwordGame-Win64-Shipping.pdb ${SwordGameInstallerAbsPath}/${GSymbolFileName}"
            }
        }else{

            def ipaPrefix= ""
            def ipaSurfix= ""

            // GDistributionFlag 在打包开始时进行计算
            if(GDistributionFlag != "") {
                ipaPrefix= "Distro_"
            }
            
            // Shipping 优先级最高
            if (params.SHIPPING == true){
                ipaSurfix= "-IOS-Shipping"
            } else {
                if (params.FORCEDEBUG == true){
                    ipaSurfix= "-IOS-Debug"
                }
            }

            def ipaFullName="${ipaPrefix}SwordGame${ipaSurfix}.ipa"
            def dsymFullName = "SwordGame${ipaSurfix}.dSYM"

            sh "mv ${SwordGameInstallerAbsPath}/${ipaFullName} ${SwordGameInstallerAbsPath}/${GPackageName}"
            sh "mv ${SwordGameInstallerAbsPath}/${dsymFullName} ${SwordGameInstallerAbsPath}/${GSymbolFileName}"
        }
        //上传ftp
        dir("${SwordGameInstallerAbsPath}") {
            PutToFtp(FTPSelect, "/${params.FTP_PATH}", GPackageName, true, false)
            //windows分包，则上传ftp
            if ( Platform == PLATFORM_WINDOWS && fileExists("${GNsisbinFileName}")){
                PutToFtp(FTPSelect, "/${params.FTP_PATH}", GNsisbinFileName, true, false)
            }
            if (params.BuildType == "PreReleaseBuild" && Platform == PLATFORM_WINDOWS){
                println "Skip Upload SymbolFile"
            }else{
                PutToFtp(FTPSelect, "/${params.FTP_SYM}", GSymbolFileName, true, false)
            }
        }
    }
    //给patch留版本号
    sh "cp ${SwordGameContentAbsPath}/GameData/Common/LongVersion.txt \"${PreVersionAbsPath}\""

    // 分包上传到服务器
    if ( params.SKIP_Package == false){

        if(params.ADDRESOURCE_PACKAGE == true){

            // 单独上传分包 manifest 文件 到 FTP
            sh "cp ${GPatchFolderAbsPath}/SwordGame_DownloadContent${GLongVersion}.manifest ${SwordGameInstallerAbsPath}"
            dir("${SwordGameInstallerAbsPath}"){
                sh "mv SwordGame_DownloadContent${GLongVersion}.manifest ${GManifestFileName}"
                PutToFtp(FTPSelect, "/${params.FTP_PATH}", GManifestFileName, true, false)    
            }

            // 上传分包 Zip 文件 到 FTP
            dir("${SwordGameInstallerAbsPath}") {
                if ( fileExists("ChunkDir.zip") ) {

                    // 上传 Zip 到 FTP
                    sh "mv ChunkDir.zip ${GChunkPatchName}"
                    PutToFtp(FTPSelect, "/${params.FTP_PATH}", GChunkPatchName, true, false)
                }
            }
        }

        // 如果使用腾讯云，无论如何都要上传 CloudDir
        if(params.UPLOAD_TENCENT_CLOUD) {
            // 需要上传腾讯云
            if(!GAlreadyUploadPatchToCloud){
                // 上传所有文件
                UploadChunkToTencentCloud('', true)
            } else {
                // 上传除了整包manifest以外的所有文件
                UploadChunkToTencentCloud('SwordGame_[0-9]*.manifest', true)
            }
        }
    }
    
    

    //PakChunk3 上传ftp
    println params.ADDRESOURCE_PACKAGE
    if ( params.SKIP_Package == false && params.ADDRESOURCE_PACKAGE == true){
        dir("${GPakChunkAbsPath}"){
            if ( fileExists("${GChunk3PakOriginalName}") == true){
                sh "cp ${GChunk3PakOriginalName} ${SwordGameInstallerAbsPath}"
                dir("${SwordGameInstallerAbsPath}"){
                    sh "mv ${GChunk3PakOriginalName} ${GChunk3PakPackageName} "
                    PutToFtp(FTPSelect, "/${params.FTP_PATH}", "${GChunk3PakPackageName}", true, false)
                }
            }
        }
    }

    // Patch 上传ftp
    dir("${SwordGameInstallerAbsPath}") {
        if( fileExists("${GPatchName}.zip") == true ){
            sh "mv ${GPatchName}.zip ${GPackagePatchName}"

            PutToFtp(FTPSelect, "/${params.FTP_PATH}", GPackagePatchName, true, false)
        }
    }
    

    //上传珠海服务器
    if ( params.UPLOADZHUHAIFTP == false || params.BuildType == "BuddyBuild"){
        echo "Skip UpLoad ZhuHaiFtp"
        return
    }

    def ZhuHaiFtpPath = ""

    if ( params.BuildType == "DailyBuild"){
        ZhuHaiFtpPath = "JX2/DailyBuild"
    }else if( params.BuildType == "WeeklyBuild"){
        ZhuHaiFtpPath = "JX2/WeeklyBuild"
    }else if( params.BuildType == "PreReleaseBuild"){
        ZhuHaiFtpPath = "JX2/PreRelease"
    }
    dir("${SwordGameInstallerAbsPath}") {
        PutToFtp('ZhuHaiFTP', "/${ZhuHaiFtpPath}", GPackageName, true, false)
    }
}


//Prerelease上传备份
def UpLoadReleases() {
    if( params.BuildType != "PreReleaseBuild"){
        println "Skip UpLoadReleases due to current build type."
        return
    }

    if( params.ReleasesSVN_Path == "" ) {
        println "Skip UpLoadReleases due to input params"
        return 
    }
    
    def ReleasesAbsPath = ""
    def BaseReleaseAbsPath = ""
    def PrereleaseBackupAbsPath = "${SwordGameAbsPath}/PrereleaseBackup"
    def ReleasesBackupAbsPath = ""
    def ReleasesZipName = ""
    def PrereleaseShadersBackupAbsPath = ""

    def ReleaseShaderPath = "${SwordGameAbsPath}/Releases/${GLongVersion}/Shaders"

    if ( Platform == PLATFORM_ANDROID ){
        ReleasesBackupAbsPath = "${SwordGameAbsPath}/${GLongVersion}/Android_ASTC"
        ReleasesAbsPath = "${SwordGameAbsPath}/Releases/${GLongVersion}/Android_ASTC"
        BaseReleaseAbsPath = "${SwordGameAbsPath}/Releases/${params.BASEVERSION}/Android_ASTC"
        ReleasesZipName = "Releases_Android.zip"
        PrereleaseShadersBackupAbsPath = "${PrereleaseBackupAbsPath}/Shaders_Android"
    }else if (Platform == PLATFORM_WINDOWS){
        ReleasesBackupAbsPath = "${SwordGameAbsPath}/${GLongVersion}/WindowsNoEditor"
        ReleasesAbsPath = "${SwordGameAbsPath}/Releases/${GLongVersion}/WindowsNoEditor"
        BaseReleaseAbsPath = "${SwordGameAbsPath}/Releases/${params.BASEVERSION}/WindowsNoEditor"
        ReleasesZipName = "Releases_WIN64.zip"
        PrereleaseShadersBackupAbsPath = "${PrereleaseBackupAbsPath}/Shaders_Windows"
    }else if (Platform == PLATFORM_IOS){
        ReleasesBackupAbsPath = "${SwordGameAbsPath}/${GLongVersion}/IOS"
        ReleasesAbsPath = "${SwordGameAbsPath}/Releases/${GLongVersion}/IOS"
        BaseReleaseAbsPath = "${SwordGameAbsPath}/Releases/${params.BASEVERSION}/IOS"
        ReleasesZipName = "Releases_IOS.zip"
        PrereleaseShadersBackupAbsPath = "${PrereleaseBackupAbsPath}/Shaders_IOS"
    }

    //上传svn
    try{
        if (!fileExists("${PrereleaseBackupAbsPath}")){
            sh "mkdir -p ${PrereleaseBackupAbsPath}"

            dir("${PrereleaseBackupAbsPath}"){
                sh "svn checkout --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} ${params.ReleasesSVN_Path} ."
            }
        }else{
            dir("${PrereleaseBackupAbsPath}"){
                sh """
                    svn cleanup
                    svn cleanup --remove-unversioned 
                    svn revert -R .
                    svn update --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} .
                """
            }
        }
        
        // 例： Android 平台下，将 Android_ASTC 拷贝到 PrereleaseBackupAbsPath 目录下
        sh "cp -rf ${ReleasesAbsPath} ${PrereleaseBackupAbsPath}"

        if (!fileExists("${PrereleaseShadersBackupAbsPath}")){
            sh "mkdir -p ${PrereleaseShadersBackupAbsPath}"
        }
        // 拷贝Shaders目录以备份Shader增量更新依赖的ShaderCodeLibrary
        sh "cp -rf ${ReleaseShaderPath} ${PrereleaseShadersBackupAbsPath}"

        dir("${PrereleaseBackupAbsPath}"){
            sh """
                svn add * --force
                svn commit -m \"${Platform} ${GLongVersion} PrereleaseBackup\" * --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW}
            """
        }
    }catch(any){
        currentBuild.result = 'UNSTABLE'
        println "Upload Releases SVN Failed"
    }
    
    //上传ftp
    try{
        sh "mkdir -p ${ReleasesBackupAbsPath}"
        if ( params.PATCH == false ){
            sh "cp -rf ${ReleasesAbsPath}/* ${ReleasesBackupAbsPath}" 
            dir("${SwordGameAbsPath}"){
                zip dir: "${GLongVersion}", glob: '', zipFile: GReleasesBackupZipAbsPath
                sh "mv ${ReleasesZipName} ${GReleasesBackupName}"
                PutToFtp(FTPSelect, "/${params.FTP_PATH}", GReleasesBackupName, true, false)
            }
        }else{
            def Releasesfiles =sh(returnStdout: true, script: "ls ${ReleasesAbsPath}")
            def OldReleasesfiles = sh(returnStdout: true, script: "ls ${BaseReleaseAbsPath}")
            def FileList = Releasesfiles.split('\n')
            def OldFilelist = OldReleasesfiles.split('\n')
            def NewFileList = FileList - OldFilelist
            for (int i = 0; i < NewFileList.size(); ++i){
                sh "cp -rf ${ReleasesAbsPath}/${NewFileList[i]} ${ReleasesBackupAbsPath}"
            }
            dir("${SwordGameAbsPath}"){
                zip dir: "${GLongVersion}", glob: '', zipFile: GReleasesBackupZipAbsPath
                sh "mv ${ReleasesZipName} ${GReleasesBackupName}"
                PutToFtp(FTPSelect, "/${params.FTP_PATH}", GReleasesBackupName, true, false)
            }
        }
    }catch(any){
        currentBuild.result = 'UNSTABLE'
        println "Upload Releases ftp Failed"
    }

    //删除文件
    dir("${SwordGameAbsPath}"){
        DeleteFileIfExist(GReleasesBackupName)
        DeleteFileIfExist(GLongVersion)
    }
}

def UploadMetabaseInfo(String tablename, String name, String type, def size, String version) {
    try{
    def param = "data={\"name\":\"${name}\",\"type\":\"${type}\",\"size\":${size},\"version\":${version}}&tablename=${tablename}"
    println param
    httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_FORM', httpMode: 'POST', requestBody: "${param}", responseHandle: 'NONE', url: 'http://120.92.158.166:9002/stat.php'

    }catch (any) {
        currentBuild.result = 'UNSTABLE'
        println "UploadInfo Failed"
    }
}

def ParseStoreVersion(){
    String storeVersion = ""
    def strList = GLongVersion.split('\\.')

    for (int i = 0; i < strList.size(); ++i) {
        def num = strList[i].toInteger()
        if (i == 0){
            storeVersion = storeVersion + num
        }else{
            if (num < 10){
                storeVersion = storeVersion + "0" + num
            }else{
                storeVersion = storeVersion + num
            }
        }
    }

    return storeVersion
}

def ParseLongVersion() {
    String numLongVersion = ""
    def strList = GLongVersion.split('\\.')

    for (int i = 0; i < strList.size(); ++i) {
        def num = strList[i].toInteger()
        if (i == 0) {
            // 首位不补位
            numLongVersion = numLongVersion + num
        }
        else {
            if (num < 10) {
                numLongVersion = numLongVersion + "00" + num
            }
            else if (num < 100) {
                numLongVersion = numLongVersion + "0" + num
            }
            else {
                numLongVersion = numLongVersion + num
            }
        }
    }

    return numLongVersion
}

// 临时 PackageVersion 兼容方案
def ParseLongVersion_PackageVersion() {
    String numLongVersion = ""
    def strList = GLongVersion.split('\\.')

    for (int i = 0; i < strList.size(); ++i) {
        def num = strList[i].toInteger()
        if (i == 0) {
            // 首位不补位
            numLongVersion = numLongVersion + num
        }
        else if (i == 1) {
            // 第二位补一个0
            if (num < 10) {
                numLongVersion = numLongVersion + "0" + num
            }
            else {
                numLongVersion = numLongVersion + num
            }
        }
        else {
            if (num < 10) {
                numLongVersion = numLongVersion + "00" + num
            }
            else if (num < 100) {
                numLongVersion = numLongVersion + "0" + num
            }
            else {
                numLongVersion = numLongVersion + num
            }
        }
    }

    return numLongVersion
}

def ParseCookSize(String path, String postName, String[] childList = null) {

    if( Platform == PLATFORM_ANDROID ){
        postName = "Android" + postName
    }else if( Platform == PLATFORM_WINDOWS ) {
        postName = "WIN64" + postName
    }else{
        postName = "IOS" + postName
    }
    def files = sh(returnStdout: true, script: "du -d1 ${path}")
    println "${files}"
    def str = files.split('\n')

    for (String strLine in str) {
        def argList = strLine.split('\t')
        if (argList.size() == 2) {
            def fileName = argList[1].toString()
            def size = argList[0].toInteger()
            def version = ParseLongVersion()

            if (childList) {
                for(int i = 0; i < childList.size(); ++i) {
                    def childPath = path + '/' + childList[i]
                    if(fileName == childPath) {
                        ParseCookSize(childPath, childList[i])
                    }
                }
            }

            UploadMetabaseInfo("StagedContentSize", postName, fileName, size, version)
        }
        else{
            println 'Cook Size Data Split Error!'
        }
    }
}

def UploadInfo() {
    if (params.SKIP_UPLOADINFO == true){
        echo "SKIP_UpLoadInfo"
    }else{
        if ( params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild")
        {
            def path = ""
            if ( Platform == PLATFORM_ANDROID ){
                path = "${SwordGameAbsPath}/Saved/Cooked/Android_ASTC/SwordGame"
            }else if (Platform == PLATFORM_WINDOWS){
                path = "${SwordGameAbsPath}/Saved/Cooked/WindowsNoEditor/SwordGame"
            }else {
                path = "${SwordGameAbsPath}/Saved/Cooked/IOS/SwordGame"
            }

            dir(path) 
            {
                String[] childList = ["GameContent", "Resource"]
                ParseCookSize("Content", "Content", childList)
            }
        }
    }
}


def UploadPackageInfo(){
    if (params.SKIP_UPLOADINFO == true){
        echo "SKIP_UpLoadPackageInfo"
        return
    }

    if (params.BuildType == "DailyBuild" || params.BuildType == "PreReleaseBuild"){
        def packageAbsPath = ""
        def devPlatform = ""

        if ( Platform == PLATFORM_ANDROID ){
            packageAbsPath = "${SwordGameInstallerAbsPath}/SwordGame-${GAndroidArchName}-es2.apk"
            devPlatform = "DevAndroid"
        }else if ( Platform == PLATFORM_IOS ){
            packageAbsPath = "${SwordGameInstallerAbsPath}/Distro_SwordGame.ipa"
            devPlatform = "DevIOS"
        }else{
            packageAbsPath = "${SwordGameInstallerAbsPath}/setup_jx2.exe"
            devPlatform = "DevWindows"
        }
        
        def packageSize = GetFileSize(packageAbsPath)

        echo "Package Size: ${packageSize}"

        def packageVersion = ParseLongVersion_PackageVersion()

        UploadMetabaseInfo("PackageSize", "PackageSize", devPlatform, packageSize, packageVersion) 

        def patchFileAbsPath = "${SwordGameInstallerAbsPath}/${GPatchName}.zip"

        if (fileExists("${patchFileAbsPath}") == true) {
            def patchSize = GetFileSize(patchFileAbsPath)
            echo "filesize: ${patchSize}"

            UploadMetabaseInfo("PackageSize", "PatchSize", devPlatform, patchSize, packageVersion)
        }
    }
}


// 输入文件绝对路径
// 返回文件大小
def GetFileSize(def fileAbsPath) {

    echo "GetFileSize: ${fileAbsPath}"

    if (fileExists("${fileAbsPath}") == true) {
        
        // sh 结果读出来的值是 string 类型
        def fileSizeInString

        if(Platform == PLATFORM_WINDOWS || Platform == PLATFORM_ANDROID) {
            fileSizeInString = sh(returnStdout: true, script: "stat -c%s ${fileAbsPath}")
        } else { 
            // PLATFORM_IOS
            fileSizeInString = sh(returnStdout: true, script: "stat -f%z ${fileAbsPath}")
        }

        return fileSizeInString.toBigInteger()

    } else {

        echo "File doesn't exits! ${fileAbsPath}"
        return 0
    }

}


def BuglyLongVersion() {

    String numLongVersion = ""
    def strList = GLongVersion.split('\\.')

    for (int i = 0; i < strList.size(); ++i) {
        def num = strList[i].toInteger()
        if (i == 0) {
            // 首位不补位
            numLongVersion = numLongVersion + num
        }
        else {
            if (num < 10) {
                numLongVersion = numLongVersion + "0" + num
            }
            else {
                numLongVersion = numLongVersion + num
            }
        }
    }
    echo numLongVersion
    return numLongVersion
}

def UploadSymbolsToBugly(){
    if (params.BUGLY_UPLOAD_SYMBOL == false || params.SKIP_Package == true){
        echo "Skip Upload Symbol Bugly"
        return
    }
    def Longnum = BuglyLongVersion()
    echo Longnum
    def AndroidVersion = "${GShortVersion}" + "." + "${Longnum}"
    echo AndroidVersion

    def IosLongVersion = ""
    def strList = GLongVersion.split('\\.')
    IosLongVersion = strList[0] + "." + strList[1] + "." + strList[2] + "(${GLongVersion})"
    
    try{
        if ( Platform == PLATFORM_ANDROID ){
            bat "java -Xms2048m -Xmx2048m -Dfile.encoding=UTF8 -jar ${GJenkinsBuildAbsPath}/AndroidBugly/buglySymbolAndroid.jar -i \"${SymbolFilePath}\" -u -version ${AndroidVersion}"
        }else if ( Platform == PLATFORM_IOS ){
            sh "java -Xms4096m -Xmx4096m -Dfile.encoding=UTF8 -jar ${GJenkinsBuildAbsPath}/Bugly/buglySymboliOS.jar -i \"${SymbolFilePath}\" -u -version \"${IosLongVersion}\""
        }
    }catch (any) {
        currentBuild.result = 'UNSTABLE'
        println "Upload Symbols To Bugly Failure"
    }
}

def UploadtoTako(){
    if (params.SKIP_USE_TAKO == true ){
        echo "skip uploadtoTako"
        return
    }
    def UpLoadKey = ""
    if (params.BuildType == "PreReleaseBuild"){
        if ( Platform == PLATFORM_ANDROID){
            UpLoadKey = "5cde2fbea9ecf75cb1b7ca8a"
        }else if (Platform == PLATFORM_IOS){
            UpLoadKey = "5c49777bfd893073ada21e99"
        }
    }else if(params.BuildType == "DailyBuild"){
        if ( Platform == PLATFORM_ANDROID){
            UpLoadKey = "5c4977a2fd893073ada21e9e"
        }else if (Platform == PLATFORM_IOS){
            UpLoadKey = "5c49776bfd893073ada21e97"
        }
    }else{
        echo "skip uploadtoTako"
        return
    }
    try{
        if( Platform == PLATFORM_IOS) {
            dir ("${SwordGameInstallerAbsPath}"){
                sh " curl -F \"uploadfile=@${GSwordGamePackageAbsPath}\" -F \"uploadkey=${UpLoadKey}\" -F \"cloud=false\" -F \"lan=true\" http://tako.jx2.bjxsj.site:33888/upload -v"
            }

        }else{
    sh "${SwordGameCMakeModuleAbsPath}/bin/curl.exe -F \"uploadfile=@${GSwordGamePackageAbsPath}\" -F \"uploadkey=${UpLoadKey}\" -F \"cloud=false\" -F \"lan=true\" http://tako.jx2.bjxsj.site:33888/upload -v"
        }
    }catch (any) {
        currentBuild.result = 'UNSTABLE'
        println "UploadtoTako Failure"
    }
}


//Patch和分包打带目录结构的zip，ZipAbsPath=zip包地址
def ZipCloudDir(String ZipAbsPath){
    dir ("${SwordGameInstallerAbsPath}"){
        DeleteFileIfExist("CloudDir")
        sh "mkdir -p ${GLocalPatchZipDir}"
        sh "cp -rf ${GPatchFolderAbsPath}/* ${GLocalPatchZipDir}"

        DeleteFileIfExist(ZipAbsPath)
        zip dir: "CloudDir", glob: '', zipFile: ZipAbsPath
        DeleteFileIfExist("CloudDir")
    }
}
    

def Cleanup() {
    if (params.SKIP_UPLOADFTP == true || currentBuild.result == 'UNSTABLE'){
        println "Skip Cleanup"
        return
    }
    dir (SwordGameInstallerAbsPath){
        DeleteFileIfExist(GSwordGamePackageAbsPath)
        //patch zip
        DeleteFileIfExist(GPackagePatchName)
        //pak
        DeleteFileIfExist(GChunk3PakPackageName)
        //manifest
        DeleteFileIfExist(GManifestFileName)
        //so/dSYM/pdb
        DeleteFileIfExist(GSymbolFileName)
        //chunk zip
        DeleteFileIfExist("${SwordGameInstallerAbsPath}/${GChunkPatchName}")
        if (Platform == PLATFORM_WINDOWS){
            DeleteFileIfExist(GNsisbinFileName)
        }
    }
}

// 上传文件到 Ftp
// ftpConfigName: FTP 配置名，例：ShenZhenFTP
// ftpDir: FTP 远程目录
// fileName: 本地文件名
// deleteAfterSuccess: 上传成功后是否删除
def PutToFtp(String ftpConfigName, String ftpDir, String fileName, boolean markUnstableIfFail, boolean deleteAfterSuccess) {

    echo "Uploading Ftp: " + " " + ftpConfigName + " " + ftpDir + " " + fileName

    try {
        ftpPublisher alwaysPublishFromMaster: false, continueOnError: true, failOnError: false, 
            publishers: [
                [
                    configName: "${ftpConfigName}", transfers: [
                        [
                            asciiMode: false, cleanRemote: false, excludes: '', flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: "${ftpDir}", remoteDirectorySDF: false, removePrefix: '', sourceFiles: "${fileName}"
                        ]
                    ], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false
                ]
            ]


        if(deleteAfterSuccess) {
            DeleteFileIfExist(fileName)
        }

    } catch (any) {
        echo "Upload Ftp failed: " + " " + ftpConfigName + " " + ftpDir + " " + fileName

        if(markUnstableIfFail) {
            echo "Mark build unstable"
            currentBuild.result = 'UNSTABLE'
        }
    }

}

// 上传文件到 腾讯云
def UploadChunkToTencentCloud(String ignorePattern, boolean markUnstableIfFail) {

    echo "Uploading Tencent Cloud: "

    // chunk/BuddyBuild/JXda9339/Patch/V3.21/Patch/Android
    String cloudPath = "${GCloudBranchPath}/${GPatchPathWithVersion}"
    String cloudSwordGameVersionUrl = "${GCloudUrl}/${cloudPath}/SwordGame.version"
    String cloudSwordGameMinafestUrl = "${GCloudUrl}/${cloudPath}/SwordGame_${GLongVersion}.manifest"

    echo "cloudPath=" + cloudPath

    try {

        String ignoreArgs = ""

        if(ignorePattern != null && ignorePattern != '') {
            // 带有忽略列表
            ignoreArgs = "--ignore ${ignorePattern}"
        } 

        sh """
            coscmd upload -rs ${GPatchFolderAbsPath} ${cloudPath} ${ignoreArgs}
            coscmd upload ${GWorkSpaceAbsPath}/BuildFingerPrint.txt ${GCloudBranchPath}/BuildFingerPrint.txt
        """

        if( Platform == PLATFORM_IOS) {
            // Mac 系统下要调用 python3
            sh """
                python3 ${GJenkinsBuildAbsPath}/Helpers/PurgeTencentCloudUrl.py -u \'${cloudSwordGameVersionUrl}\'
                python3 ${GJenkinsBuildAbsPath}/Helpers/PurgeTencentCloudUrl.py -u \'${cloudSwordGameMinafestUrl}\'
            """
        } else {
            // Windows 系统下直接使用 python
            sh """
                python ${GJenkinsBuildAbsPath}/Helpers/PurgeTencentCloudUrl.py -u \'${cloudSwordGameVersionUrl}\'
                python ${GJenkinsBuildAbsPath}/Helpers/PurgeTencentCloudUrl.py -u \'${cloudSwordGameMinafestUrl}\'
            """
        }

    } catch (any) {
        echo "Upload Tencent Cloud failed!"

        if(markUnstableIfFail) {
            echo "Mark build unstable"
            currentBuild.result = 'UNSTABLE'
        }
    }

}


def SendBuildNotify(String buildResult, boolean sendMail, boolean sendLark) {

    String statusColorString = ""
    String statusString = ""
    String title = ""
    // String content = ""
    String ftppath = ""
    String jenkinsLogDesc = "Jenkins打包日志"
    String ftpDownloadDesc = ""
    String imageName = ""
    String ftpDownloadUrl = ""
    String portalUrl = ""
    String jenkinsLogUrl = "${env.BUILD_URL}/console"
    String portalDesc = "点击进入扫码下载页面"
    
    if (params.FTP == "BeiJingFTP"){
        ftppath = "http://bjftp.jx2.bjxsj.site/ftp"
    }else if (params.FTP == "ShenZhenFTP"){
        ftppath = "http://ftp.jx2.bjxsj.site/ftp"
    }

    title = "${GPlatformName} ${params.BuildType} ${env.BUILD_NUMBER} - ${buildResult}"

    if(buildResult == 'UNSTABLE') {
        // 有问题，但是没有失败
        statusColorString = "#ff9900"
        statusString = "Unstable"
        ftpDownloadUrl = "${ftppath}/${FTP_PATH}/${GPackageName}"
        ftpDownloadDesc = "点击从FTP下载安装包"
        portalUrl = "http://portal.jx2.bjxsj.site"
        imageName = "unstable.png"
    } else if (buildResult == 'FAILURE') {
        // 失败
        statusColorString = "#ff0000"
        statusString = "Failure"
        imageName = "failure.png"
    } else if (buildResult == "ABORTED"){
        statusColorString = "#708090"
        statusString = "Aborted"
        imageName = "aborted.png"
    } else {
        // 成功
        statusColorString = "#008000"
        statusString = "Success"
        ftpDownloadUrl = "${ftppath}/${FTP_PATH}/${GPackageName}"
        ftpDownloadDesc = "点击从FTP下载安装包"
        portalUrl = "http://portal.jx2.bjxsj.site"
        imageName = "success.png"
    }

    imgPath = "${GJenkinsBuildAbsPath}/image/${imageName}"
    imageCode = sh(returnStdout: true, script: "base64 ${imgPath}")

    if( sendMail ) {
        emailext(
            subject: "${GPlatformName} ${params.BuildType} ${params.WORK_NAME} #${env.BUILD_NUMBER} ${statusString}",
            attachLog: true,
            compressLog: true, 
            recipientProviders: [requestor()],
            to: "${params.MAIL_NAME}",
            body: """
            <p>Job Name: ${env.JOB_NAME}</p> 
            <p>Build ID: ${env.BUILD_NUMBER}</p>
            <p>Build Name: ${GPlatformName}_${params.BuildType}_${params.WORK_NAME}</p>
            <p><a href="${env.BUILD_URL}console">Job log</a></p>
            <p><font color="${statusColorString}">Build status: ${statusString}</font></p>
            """,
        )
    }

    //BuddyBuild 不发送
    if ( sendLark && params.BuildType != "BuddyBuild" ){
        SendLarkMessageToGroup("版本通知", title, statusString, jenkinsLogUrl, jenkinsLogDesc, imageCode, imageName, ftpDownloadUrl, ftpDownloadDesc, 600, 600)
    }


    // 发送 BuddyBuild Lark 消息
    if ( sendLark && params.BuildType == "BuddyBuild" ){
        
        // 获取所有需要通知的用户名
        def mailUsernameList = GetUsernameListFromMailList()

        wrap([$class: 'BuildUser']) {
            // 添加打包者自己
            mailUsernameList.add("\"${env.BUILD_USER_ID}\"")
        }

        String mailListString = mailUsernameList.join(",")

        echo "mailListString: " + mailListString

        String notifyMessageBody = """
            {
                "to_user_list":[${mailListString}],
                "msg_content":
                {
                    "title": "${env.JOB_NAME} ${params.WORK_NAME}#${BUILD_NUMBER}",
                    "desc": "结果：${buildResult}",
                    "url_desc": "${JOB_BASE_NAME} 启动日志链接",
                    "url": "${env.BUILD_URL}"
                }
            }
        """

        echo "notifyMessageBody: " + notifyMessageBody

        try{
            httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${notifyMessageBody}", responseHandle: 'NONE', url: "http://nemesis.jx2.bjxsj.site/api/larkrelay/sendmsg"
        }catch (any) {
            println "Failed sending to Lark! Error: " + any
        }

    }
}

void SendMessageWithLark(String content, String title, larkBotUrl){
    String body =
            """
            {
                "title": "${title}",
                "text":"${content}"
            }
            """
        
    echo "SendMessageWithLark: " + body
            httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON_UTF8', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: "${larkBotUrl}"
}

def SendLarkMessageToGroup(String group, String title, String desc, String url, String url_desc, String image_code, String image_name, String url1 = '', String url_desc1 = '', int image_width = 600, int image_height = 400) {
    data = [
            "to_chat_name": group,
            "msg_content": [
                    "title"   : title,
                    "desc"    : desc,
                    "url_desc": url_desc,
                    "url_desc1": url_desc1,
                    "url"     : url,
                    "url1"     : url1,
                    "image"   : [
                            "name"  : image_name,
                            "data"  : image_code,
                            "width" : image_width,
                            "height": image_height
                    ]
            ]
    ]
    String json = JsonOutput.toJson(data)
    try{
        String body = json
        httpRequest consoleLogResponseBody: true, contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: "${body}", responseHandle: 'NONE', url: 'http://nemesis.jx2.bjxsj.site/api/larkrelay/sendmsg'
    }catch (any) {
        println "Send Failed"
        println any
    }
}

void GetBuildResultValue(any){
    if (any =~ "FlowInterruptedException"){
        currentBuild.result = 'ABORTED'
            println currentBuild.result
    }else{
            currentBuild.result = 'FAILURE'
            println currentBuild.result
    }
    return currentBuild.result
}


void GetUsernameListFromMailList(){
    def nameList = []
    String mailList = params.MAIL_NAME

    if (mailList?.trim()) {

        echo "mailList: " + mailList

        String mailListNoSpace = mailList.replaceAll(" ", ";")
        String mailListNoComma = mailListNoSpace.replaceAll(",", ";")
        String[] mailArray = mailListNoComma.split(";")

        for( mail in mailArray){
            def matchList = (mail =~ /([a-zA-Z0-9_-]+)@kingsoft\.com/)
            String matchUsername
            String allMatch
            if(matchList.matches()) {
                (allMatch, matchUsername) = matchList[0]
                nameList.add("\"${matchUsername}\"")
            }
        }

    }

    return nameList
}
