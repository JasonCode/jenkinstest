@Library("PLPipelineLibrary") _

pipeline {
	parameters{
	    choice(choices: ['WinBuild01', 'master'], description: '选择打包机', name: 'NODE')
		string(name:'info', defaultValue:'wxLog', description:'微信log', trim:true)
		string(name:'changelist', defaultValue:'1', description:'changelist', trim:true)
        string(name:'user', defaultValue:'Root', description:'user', trim:true)
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"


	}

	stages{
		stage("Sync Workspace"){
			steps{
				script{
                    EngineFolder = 'C:/ProjectX_UE5'
                    ProjectFolder = 'C:/XBuild01_Builder_GreatTour'
                    EngineWorkSpace = 'XBuild01_ProjectX_UE5CB2'
                    ProjectWorkSpace = 'XBuild01_Builder_GreatTour'
					commonUtil.SyncWorkspace(ProjectWorkSpace, ProjectFolder, params.changelist)
					echo "SyncWorkspace"
				}
			}
		}


		stage("Execute Script"){
			steps{
				script{
                    EngineFolder = 'C:/ProjectX_UE5'
                    ProjectFolder = 'C:/XBuild01_Builder_GreatTour'
                    EngineWorkSpace = 'XBuild01_ProjectX_UE5CB2'
                    ProjectWorkSpace = 'XBuild01_Builder_GreatTour'
                    UeExe = '"C:/XBuild01_ProjectX_UE5CB2/Engine/Binaries/Win64/UnrealEditor-Cmd.exe"'
                    Uproject = 'C:/XBuild01_Builder_GreatTour/GreatTour.uproject'


                    String cmd = "python ./third_tool/PyExecute.py -c=${params.changelist} -e=${UeExe} -p=${Uproject} -u=${params.user}"
                    echo cmd
					sh "${cmd}"
					echo "Check Project"
				}
			}
		}

	}


}
