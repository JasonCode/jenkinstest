//在jenkins相同主机上操作的一些方法和函数

String _serverRootOnWin //服务器运行的父目录
String _deployRootOnWin //部署svn仓库的根目录


String GetServerRootOnWin(){ return _serverRootOnWin }

void Reset(){
	_serverRootOnWin = "c:/Server"
	_deployRootOnWin = "c:/PLDeploy"
}

//关闭本机启动的游戏服务器
void ShutdownLocalServer(String osPlatform){
	echo 'ShutdownLocalServer --------'
	switch(osPlatform) {
		case CommonConfig.OS_Windows:
			sh "${_GetLauncerTool()} stop=XGameServer"
		break
		case CommonConfig.OS_Linux:
		break
	}
}

//备份log文件
void BackupServerLog(String osPlatform){
	echo 'BackupServerLog --------'
	switch(osPlatform) {
		case CommonConfig.OS_Windows:
			//备份log
			try{
				def time = new Date().format('yyyyMMdd_HHmmss')
				echo "cp -r ${_serverRootOnWin}/XGame/Saved ${_GetLogSavedFolderOnWin()}/Saved_${time}"
				sh "cp -r ${_serverRootOnWin}/XGame/Saved ${_GetLogSavedFolderOnWin()}/Saved_${time}"


				dir(_deployRootOnWin){sh 'svn cleanup'}
				dir(_GetLogSavedFolderOnWin()){
					sh 'svn add ./ --force --no-ignore'
					echo ' finish add files. start to commit '
					sh 'svn commit ./ -m "服务器提交备份log"'
				}
			}catch(Exception ex) {
				echo ex.toString()
				echo "----------------------------------------------------------------------- igore the exception -------------------------------"
			}
		break
		case CommonConfig.OS_Linux:
		break
	}
}

//关闭本机启动的游戏服务器, 自己保证服务器是关的
void StartLocalerver(String osPlatform){
	echo 'StartLocalerver --------'
	switch(osPlatform) {
		case CommonConfig.OS_Windows:
			ShutdownLocalServer()
			sh "${_GetLauncerTool()} start=${_serverRootOnWin}/XGameServer.exe"

			//windos下踩过的坑
			//dir(_serverRootOnWin){
				// 会执行一闪而过的成功. 进程显示并没有启动
				//bat 'nohup.exe WindowsServer/XGameServer.exe -log &'
				// 会执行一闪而过的成功. 进程显示并没有启动
				//bat 'start WindowsServer/XGameServer.exe -log'
				// 会执行一闪而过的成功. 进程显示并没有启动
				//sh 'WindowsServer/XGameServer.exe -silent -log'
				
				//会一直运行, 从而挂起了jenkins job
				//sh 'WindowsServer/XGameServer.exe -log'

				// 会执行一闪而过的成功. 进程显示并没有启动
				//powershell (script: '''
				//	$process = (start WindowsServer/XGameServer.exe "-log")
				//	if ($process.ExitCode -ne 0){ exit 1 }
				//	else { exit 0 }
				//''', returnStatus: true, encoding: 'UTF-8')

				//bat './run.bat'
				//windosw垃圾
			//}
		break

		case CommonConfig.OS_Linux:
		break
	}
}

String _GetLauncerTool(){
	return "${_deployRootOnWin}/tool/shiryu/ShirYu.exe"
}

String _GetDeamonTool(){
	return "${_deployRootOnWin}/tool/shiryu/ShirYuServer.exe"
}

String _GetLogSavedFolderOnWin(){
	return "${_deployRootOnWin}/game_log/log_back"
}