import os
import os.path
import sys


# svn update.
def DeleteFunc(targetPath):

    os.chdir(targetPath)
    cmd = r"svn status|grep ! |awk '{print $2}'> missing.list"
    print(cmd)
    os.system(cmd)


    lists = ""
    filePath = os.path.join(targetPath,"missing.list")

    if not os.path.exists(filePath):
        return


    with open(filePath, "r") as f:  # 打开文件
        lists = f.readlines()  # 读取文件

    lens = len(lists)-1
    for i in range(lens, -1, -1):
        lists[i] = lists[i].strip('\n')
        cmd = r'svn delete %s '%(lists[i])
        print(cmd)
        os.system(cmd)

    cmd = r'del missing.list 2>NUL'
    print(cmd)  
    os.system(cmd)

    cmd = r'svn commit -m "编译机提交打包内容" ./'
    print(cmd)  
    os.system(cmd)


if __name__ == "__main__":
    DeleteFunc(sys.argv[1])