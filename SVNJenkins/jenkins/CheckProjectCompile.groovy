@Library("PLPipelineLibrary") _

pipeline {
	parameters{
		string(name:'EngineFolder', defaultValue:'C:/XBuild01_ProjectX_Dev', description:'引擎根目录')
		string(name:'GameFolder', defaultValue:'C:/XBuild01_ProjectX_Dev/ProjectX', description:'游戏根目录')
		string(name:'EngineWorkSpace', defaultValue:'XBuild01_ProjectX_Dev', description:'引擎的工作区')
		booleanParam(defaultValue: true, description: '更新游戏?', name: 'bUpdateGame')
		string(name:'GameWorkSpace', defaultValue:'XBuild01_ProjectX_Dev', description:'游戏的工作区')
	}
	
	agent{ node{ label 'WinBuild-13.37' } }

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					packageUtil.Reset()
					packageUtil.SetGameFolder(params.GameFolder)
					packageUtil.SetEngineFolder(params.EngineFolder)
					packageUtil.SetServerTargetPlatform("${CommonConfig.Plat_Win64}")
					packageUtil.SetPak(false)
					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
					if(params.bUpdateGame)
						commonUtil.SyncWorkspace(params.EngineWorkSpace, params.EngineFolder)
				}
			}
		}

		// stage("Compile Engine"){
		// 	steps{
		// 		script{
		// 			commonUtil.CompileEngineOn(CommonConfig.OS_Windows, params.EngineFolder)
		// 		}
		// 	}
		// }

		// stage("Checkout Game"){
		// 	steps{
		// 		script{
		// 			commonUtil.SyncWorkspace(params.GameWorkSpace, params.GameFolder)
		// 		}
		// 	}
		// }

		stage("Compile Game"){
			steps{
				script{
					commonUtil.CompileGameOn(CommonConfig.OS_Windows, params.EngineFolder, params.GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build"){
			steps{
				script{
					packageUtil.StartBuildOnly()
				}
			}
		}
	}

	post{
		failure {script {messageUtil.SendWXMsg("编译检查", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("编译检查", "main", "Unstable")}}
		//success {script {messageUtil.SendWXMsg("编译检查", "main", "Success")}}
	}

//--------- end of pipeline	
}
