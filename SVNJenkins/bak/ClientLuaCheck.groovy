@Library("SwordGamePipelineLibrary") _
import java.text.*
import java.util.Date

// FTP 路径地址
GFtpPath = ""
GFtpPathForLogFile = ""
GFtpPathForChartImage = ""
GFtpPathForLogFile_LuaInspect = ""
GFtpPathForLogFile_LuaCheck = ""

GArtistSwordGamePath = ""

properties([
  parameters([
      string(defaultValue: 'SwordGame', description: 'SwordGame文件夹名', name: 'SWORDGAME_FOLDER_NAME', trim: true),
  ])
])

pipeline {
    agent {
        node {
            label 'PC_Build7'
        }
    }

    stages {

        // stage('Copy CMakeModules Bin') {
        //   steps {
        //     powershell (script: '''
        
        //     cd $env:SWORDGAME_HOME/Source
        //     $env:ARTISTSWORD_PATH="$env:ARTIST_ROOT_PATH/SwordGame"


        //     rd -r -force CMakeModules
        //     cp -r "$env:ARTISTSWORD_PATH/Source/CMakeModules"  "$env:SWORDGAME_HOME/Source"

        //     ''', returnStatus: true, encoding: 'UTF-8')
        //   }
        // }

        stage('Prepare Environment') {      
            steps {
                script{
                    ws(env.SWORDGAME_HOME) {
                        BranchName = sh returnStdout: true, script: 'git name-rev --name-only HEAD'
                        echo "BranchName is ${BranchName}"
                    }

                    // 计算时间路径
                    Date currentBuildTime = new Date(currentBuild.startTimeInMillis)
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
                    GTimestamp = dateFormatter.format(currentBuildTime)

                    GFtpPath = "LogCollect/LuaChecker/${params.SWORDGAME_FOLDER_NAME}-${GTimestamp}"

                    GArtistSwordGamePath = "${env.ARTIST_ROOT_PATH}/SwordGame"

                }
            }
        }
        


        stage('Update Source Code Git') {      
            steps {
                sh(script: """
                    cd $SWORDGAME_HOME

                    git remote set-url origin http://git.jx2.bjxsj.site/pirate/SwordGame.git
                    git checkout ${BranchName}
                    git clean -f -d
                    git reset --hard HEAD
                    git pull
                    git lfs install
                    git lfs pull
                    git submodule foreach --recursive git reset --hard
                    git submodule update

                """)
            }
        }

        stage('Init Environment') {
            steps {
                sh (script: """
                cd $SWORDGAME_HOME

                rm -rf Binaries
                cp -rf ${GArtistSwordGamePath}/Binaries $SWORDGAME_HOME

                rm -rf Config
                cp -rf ${GArtistSwordGamePath}/Config  $SWORDGAME_HOME

                rm -rf Plugins
                cp -rf ${GArtistSwordGamePath}/Plugins $SWORDGAME_HOME

                cp -f ${GArtistSwordGamePath}/SwordGame.uproject $SWORDGAME_HOME
                
            """)
            }
        }

        stage('Update Content SVN') {
            steps {
                sh(script: """
                cd $CONTENT_PATH
                svn cleanup
                svn revert -R .
                svn update --username $SVN_CREDENTIALS_USR --password $SVN_CREDENTIALS_PSW
                """)
            }
        }

        stage('Export UI UP Scripts') {
            steps {
                powershell (script: '''
                    $process = (start -FilePath $env:ENGINE_PATH/Binaries/Win64/UE4Editor.exe "$env:SWORDGAME_HOME/SwordGame.uproject -run=ExportTabFiles -DisableCache -ExportUI -WARNINGSASERRORS -stdout -culture=en" -Wait -NoNewWindow -PassThru)

                    if($process.ExitCode -ne 0)
                    {
                        exit 1
                    }
                    else
                    {
                        exit 0
                    }        
                    ''', returnStatus: true, encoding: 'UTF-8')
            }
        }

        stage('Build Server') {
            steps {
                script {
                    echo "Building Server"
                    returnStatus = bat returnStatus: true, script: """

                        cd /D %SWORDGAME_HOME%/Source/Server/Server/Generated/
                        del Server.reflection.cache

                        set THIRDPARTY_NAME=NoahFoundation
                        cd /D %SWORDGAME_HOME%/Source/NoahSDK/%THIRDPARTY_NAME%
                        rd /s /q VSBuild
                        mkdir VSBuild
                        cd VSBuild
                        cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project ALL_BUILD
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project INSTALL

                        set THIRDPARTY_NAME=NoahNetwork
                        cd %SWORDGAME_HOME%/Source/NoahSDK/%THIRDPARTY_NAME%
                        rd /s /q VSBuild
                        mkdir VSBuild
                        cd VSBuild
                        cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project ALL_BUILD
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project INSTALL


                        set THIRDPARTY_NAME=NoahCore
                        cd %SWORDGAME_HOME%/Source/NoahSDK/%THIRDPARTY_NAME%
                        rd /s /q VSBuild
                        mkdir VSBuild
                        cd VSBuild
                        cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project ALL_BUILD
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project INSTALL


                        set THIRDPARTY_NAME=ServerCore
                        cd %SWORDGAME_HOME%/Source/NoahSDK/%THIRDPARTY_NAME%
                        rd /s /q VSBuild
                        mkdir VSBuild
                        cd VSBuild
                        cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project ALL_BUILD
                        devenv %THIRDPARTY_NAME%.sln /build \"Release|x64\" /project INSTALL

                        cd /D %SWORDGAME_HOME%/Source/Server
                        rd /s /q VSBuild
                        mkdir VSBuild
                        cd VSBuild
                        cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 14 Win64" ../
                        devenv.com Server.sln /build \"RelWithDebInfo|x64\" /project ALL_BUILD
                        devenv.com Server.sln /build \"RelWithDebInfo|x64\" /project INSTALL
                    """

                    echo "returnStatus : ${returnStatus}"
                }
            }
        }

        //////////////////////////////////////////////////////////
        //  Server Lua Check
        //////////////////////////////////////////////////////////

        stage('Server LuaCheck') {
            steps {
                powershell(script: '''

                $env:ExcutePath= "$env:SWORDGAME_HOME/Source/CMakeModules/bin/LuaTools/bin"  
                $luacheckrc = "LuaTools/bin/luacheck/server.luacheckrc"
                cd "$env:SWORDGAME_HOME/Source/CMakeModules/bin"

                $process = (start -FilePath $env:ExcutePath/luacheck/luacheck.exe "$env:SERVER_SCRIPT_PATH --config $luacheckrc --allow-defined --quiet --formatter customformat --no-color" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "$env:WORKSPACE/ServerLog.txt")

                $strWarningCount =(Get-Content $env:WORKSPACE/ServerLog.txt | Select-String -Pattern '^Warning: ' | Measure-Object).Count
                "ServerLuaCheck Warning Count: $strWarningCount" | Out-File $env:WORKSPACE/luachecklog.txt

                #显示LOG
                Get-Content -encoding UTF8 $env:WORKSPACE/ServerLog.txt

                "$strWarningCount" | Out-File $env:WORKSPACE/varServerCount.txt

                if($process.ExitCode -ne 0)
                {
                    exit 1
                }
                else
                {
                    exit 0
                }''',returnStatus: true, encoding: 'UTF-8')

                script {
                    // 上传FTP
                    packageLib.PutToFtp('ShenZhenFTP', GFtpPath, "ServerLog.txt", false, false)

                    GFtpPathForLogFile = GFtpPath + "/ServerLog.txt"

                    def varWarningCount = readFile(file:"${env.WORKSPACE}/varServerCount.txt", encoding :"unicode").toInteger() - 2
                    def strVersion = currentBuild.startTimeInMillis.toString()
                    packageLib.UploadMetabaseInfo("LuaClientCheck", "${BranchName}", "ServerCount", varWarningCount, strVersion)
                }
            }
        }
        stage("GeneratePie For Server LuaCheck"){
            steps {
                powershell(script: '''
                $env:checkerResultPath= "$env:SWORDGAME_HOME/Source/CMakeModules/bin/luachecker_statistic.txt"  
                python ./Helpers/DrawChart.py ServerLuaCheck.png $env:checkerResultPath
                
                ''',returnStatus: true, encoding: 'UTF-8')

                script {
                    // 上传FTP
                    packageLib.PutToFtp('ShenZhenFTP', GFtpPath, "ServerLuaCheck.png", false, false)

                    GFtpPathForChartImage = GFtpPath + "/ServerLuaCheck.png"

                }
            }
        }

        stage("SendMsg For Server LuaCheck"){
            steps {
                script {
                String imgPath = "${env.WORKSPACE}/ServerLuaCheck.png"
                String imgCode = sh(returnStdout: true, script: "base64 ${imgPath}")
                String logUrl = "http://ftp.jx2.bjxsj.site/ftp/" + GFtpPathForLogFile
                packageLib.SendLarkMessageToGroup("剑二程序群", "${BranchName} Server LuaCheck 结果", "请各位尽快修复 LuaCheck 检查结果", logUrl, "点击查看检查日志", imgCode, "checker.png")
                }
            }
        }

        //////////////////////////////////////////////////////////
        //  Client Lua Check
        //////////////////////////////////////////////////////////

        stage('Check by LuaCheck') {
            steps {
                powershell(script: '''

                $env:ExcutePath= "$env:SWORDGAME_HOME/Source/CMakeModules/bin/LuaTools/bin"  
                $luacheckrc = "LuaTools/bin/luacheck/client.luacheckrc"
                cd "$env:SWORDGAME_HOME/Source/CMakeModules/bin"

                $process = (start -FilePath $env:ExcutePath/luacheck/luacheck.exe "$env:SCRIPT_PATH --config $luacheckrc --quiet --formatter customformat --no-color" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "$env:WORKSPACE/WarningLog.txt")

                $strWarningCount =(Get-Content $env:WORKSPACE/WarningLog.txt | Select-String -Pattern '^Warning: ' | Measure-Object).Count
                "ClientLuaCheck Warning Count: $strWarningCount" | Out-File -Append $env:WORKSPACE/luachecklog.txt

                #显示LOG
                Get-Content -encoding UTF8 $env:WORKSPACE/WarningLog.txt

                "$strWarningCount" | Out-File $env:WORKSPACE/varWarningCount.txt

                if($process.ExitCode -ne 0)
                {
                    exit 1
                }
                else
                {
                    exit 0
                }''',returnStatus: true, encoding: 'UTF-8')

                script {
                    // 上传FTP
                    packageLib.PutToFtp('ShenZhenFTP', GFtpPath, "WarningLog.txt", false, false)

                    GFtpPathForLogFile_LuaCheck = GFtpPath + "/WarningLog.txt"

                    def varWarningCount = readFile(file:"${env.WORKSPACE}/varWarningCount.txt", encoding :"unicode").toInteger() - 2
                    def strVersion = currentBuild.startTimeInMillis.toString()
                    packageLib.UploadMetabaseInfo("LuaClientCheck", "${BranchName}", "ClientLuaCheck", varWarningCount, strVersion)
                }
            }
        }

        //////////////////////////////////////////////////////////
        //  Client Lua Inspect
        //////////////////////////////////////////////////////////


        stage('Check by Inspect') {
            steps {
                powershell(script: '''
                
                    $env:ExcutePath= "$env:SWORDGAME_HOME/Source/CMakeModules/bin/LuaTools/bin"  
                    cd "$env:SWORDGAME_HOME/Source/CMakeModules/bin"

                    $process = (start -FilePath $env:ExcutePath/lua.exe "$env:ExcutePath/lua-inspect/luainspect -fAllScripts1" -Wait -NoNewWindow -PassThru -RedirectStandardOutput "$env:WORKSPACE/ErrorLog.txt")

                    $strErrorCount =(Get-Content $env:WORKSPACE/ErrorLog.txt | Select-String -Pattern '^Error: ' | Measure-Object).Count
                    "ClientLuaCheck Error Count: $strErrorCount" | Out-File -Append $env:WORKSPACE/luachecklog.txt

                    "$strErrorCount" | Out-File $env:WORKSPACE/varErrorCount.txt

                    if($process.ExitCode -ne 0)
                    {
                        exit 1
                    }
                    else
                    {
                        exit 0
                    }
                ''',returnStatus: true, encoding: 'UTF-8')

                script {
                    // 上传FTP
                    packageLib.PutToFtp('ShenZhenFTP', GFtpPath, "ErrorLog.txt", false, false)

                    GFtpPathForLogFile_LuaInspect = GFtpPath + "/ErrorLog.txt"

                    def varErrorCount = readFile(file:"${env.WORKSPACE}/varErrorCount.txt", encoding :"unicode").toInteger()
                    def strVersion = currentBuild.startTimeInMillis.toString()
                    packageLib.UploadMetabaseInfo("LuaClientCheck", "${BranchName}", "ClientLuaInspect", varErrorCount, strVersion)
                }
            }
        }

        stage("GeneratePie For LuaChecker&LuaInspect"){
            steps {
                powershell(script: '''
                python ./Helpers/DrawChart.py ClientLuaInspect.png $env:SWORDGAME_HOME/Source/CMakeModules/bin/luachecker_statistic.txt $env:SWORDGAME_HOME/Source/CMakeModules/bin/luachecker_statistic2.txt
                
                ''',returnStatus: true, encoding: 'UTF-8')

                script {
                    // 上传FTP
                    packageLib.PutToFtp('ShenZhenFTP', GFtpPath, "ClientLuaInspect.png", false, false)

                    GFtpPathForChartImage2 = GFtpPath + "/ClientLuaInspect.png"

                }
            }
        }

        stage("SendMsg For LuaChecker&LuaInspect"){
            steps {
                script {
                String imgPath = "${env.WORKSPACE}/ClientLuaInspect.png"
                String imgCode = sh(returnStdout: true, script: "base64 ${imgPath}")
                String logUrlCheck = "http://ftp.jx2.bjxsj.site/ftp/" + GFtpPathForLogFile_LuaCheck
                String logUrlInspect = "http://ftp.jx2.bjxsj.site/ftp/" + GFtpPathForLogFile_LuaInspect
                packageLib.SendLarkMessageToGroup("剑二程序群", "${BranchName} LuaChecker&LuaInspect 结果", "请各位尽快修复 LuaChecker&LuaInspect 检查结果", logUrlCheck, "点击查看LuaCheck检查日志", imgCode, "checker.png", logUrlInspect, "点击查看LuaInspect日志")
                }
            }
        }
    }
    post {
        success {
            script {
                def errorcount = readFile(file: "${env.WORKSPACE}/luachecklog.txt", encoding :"unicode")
                
                emailext(
                subject: "Pipeline_ClientLuaCheck",
                to: "${'lidi@kingsoft.com'}",
                body: """
                    <p>Job Name: ${env.JOB_NAME}</p>
                    <p>Build ID: ${env.BUILD_NUMBER}</p>
                    <p>Build Status: Success</p>
                    <p>Build Summary:[[${errorcount}]]</p>
                    <p>LOG URL: &QUOT;<a href='${env.BUILD_URL}/console'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>
                    """,
                attachLog: true,
                ) 
            }
        }
        failure {
            emailext(
                subject: "Pipeline_ResourcesCheck_Failure",
                to: "${'lidi@kingsoft.com'}",
                body: """
                <p>Job Name: ${env.JOB_NAME}</p> 
                <p>Build ID: ${env.BUILD_NUMBER}</p>
                <p>Build STATUS: Failure</p>
                <p>LOG URL: &QUOT;<a href='${env.BUILD_URL}/console'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>
                """,
            ) 
        }
    }

    environment {
        ENGINE_PATH = "C:/Workspace/Artist/Engine"
        SWORDGAME_HOME = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}"
        SCRIPT_PATH = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}/Scripts"
        SERVER_SCRIPT_PATH = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}/Source/Config/Server/script"
        CONTENT_PATH = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}/Content"
        SVN_CREDENTIALS = credentials('092907a7-dcbf-425b-bc2d-c61715b73536')
        FTP_CREDENTIALS = credentials('3a51894f-d6f5-43a6-85be-e23daacc197b')
        FTP_SERVER = "ftp.jx2.bjxsj.site"
        FTP_PATH = "PipelineLog"
        THIRDPARTY_PATH = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}/Source/ThirdParty"
        CUSTOM_CMAKE_MODULE = "C:/Workspace/${params.SWORDGAME_FOLDER_NAME}/Source/CMakeModules"
    }
}