@Library(['SwordGamePipelineLibrary', 'checkLib']) _

properties([
  parameters([
        choice(choices: ['Artist', 'Master', 'Release'], description: '检查版本', name: 'CHECKER_BRANCH'),
        string(defaultValue: 'fukai@kingsoft.com lidi@kingsoft.com wumeiru@kingsoft.com yekaiming@kingsoft.com',description:'邮件通知的收件人',name: 'MAIL_RECLPITNTS',trim: true),
        booleanParam(defaultValue: true, description: '是否POST Error 数据到服务器', name: 'POST_DATA'),
        string(defaultValue: 'Checkers',description:'post data 到服务器时的一级分类',name:'POST_DATA_TABLE_NAME',trim:true),
        string(defaultValue: 'UICheckers',description:'post data 到服务器时的二级分类',name:'POST_DATA_NAME',trim:true),
    ])
])

pipeline {
  options{
    timeout(time: 4, unit: 'HOURS')
  }

  agent {
    node {
      label 'PC_Build7'
    }
  }
  stages {
    stage('Update AutoTest Tools'){
      steps{
        script{
          checkerLib.UpdateAutoTestTools()
        }
      }
    }
    stage('Svn Update') {
      steps {
        parallel(
          UpdateArtist:{
            script{
              checkerLib.SvnUpdate("${env.ARTIST_ROOT_PATH}")
            }
          },
          UpdateBranchContent:{
            script{
              if(params.CHECKER_BRANCH == "Master")
              {
                checkerLib.SvnUpdate("${env.MASTERBUILD_ROOT_PATH}/Content")
              }
              else if(params.CHECKER_BRANCH == "Release")
              {
                checkerLib.SvnUpdate("${env.RELEASEBUILD_ROOT_PATH}/Content")
              }
            }
          }
        )
      }
    }
    stage('Make Checker Env') {
      steps {
        script{
          def szBranchPath = env.MASTERBUILD_ROOT_PATH
          if(params.CHECKER_BRANCH == "Release")
          {
            szBranchPath = env.RELEASEBUILD_ROOT_PATH
          }
          checkerLib.MakeCheckerEnv("${szBranchPath}")
        }
      }
    }
    stage('Check Hidden Image') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("HiddenImage", "SwordToolbarButton CheckUIHiddenImage", nCheckerType)
        }
      }
    }
    stage('Check Prefab') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("Prefab", "ExportTabFiles CheckBindPrefab", nCheckerType)
        }
      }
    }
    stage('Check UI Animation') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("UIAnimation", "SwordToolbarButton CheckUIAnimation", nCheckerType)
        }
      }
    }
    stage('Check Flash Image') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("FlashImage", "SwordToolbarButton CheckFlashImage", nCheckerType)
        }
      }
    }
    stage('Check UI Texture') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("UITexture", "SwordToolbarButton CheckUITextureConfig", nCheckerType)
        }
      }
    }
    stage('Check Button IsFocusable') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("ButtonIsFocusable", "SwordToolbarButton CheckButtonIsFocusable", nCheckerType)
        }
      }
    }
    stage('Check Widget Naming') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("WidgetNaming", "SwordToolbarButton CheckWidgetNamingConventions", nCheckerType)
        }
      }
    }
    stage('Check Widget NameCase') {
      steps {
        script{
          def nCheckerType = checkerLib.@CHECKER_MASTER
          if(params.CHECKER_BRANCH == "Artist")
          {
            nCheckerType = checkerLib.@CHECKER_ARTIST
          }
          else if(params.CHECKER_BRANCH == "Release")
          {
            nCheckerType = checkerLib.@CHECKER_RELEASE
          }
          checkerLib.ExecuteCheckerCommand("WidgetNameCase", "SwordToolbarButton CheckWidgetNameCase", nCheckerType)
        }
      }
    }
    stage('Post Data') {
      steps
      {
        script
        {
          checkerLib.PostMetabaseinfoData()
        }
      }
    }
  }
  post('Send a message')
  {
    always {
      script {
        checkerLib.SendMessage(params.CHECKER_BRANCH)
      }
    }
  }
  environment {
    SVN_CREDENTIALS = credentials('092907a7-dcbf-425b-bc2d-c61715b73536')
    FTP_CREDENTIALS = credentials('3a51894f-d6f5-43a6-85be-e23daacc197b')
    FTP_PATH = "LogCollect/CheckUI"
  }
}