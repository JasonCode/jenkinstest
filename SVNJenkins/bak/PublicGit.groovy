@Library("SwordGamePipelineLibrary") _

pipeline{
    agent{
        node{
            //用PC_Build7启动job
            label "PC_Build7"
        }
    }

    parameters{
        string(name:'timeout', defaultValue:'1', description:'')
    }

    options{
        withCredentials([
            usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS'),
            usernamePassword(credentialsId: '092907a7-dcbf-425b-bc2d-c61715b73536', usernameVariable: 'SVN_CREDENTIALS_USR', passwordVariable: 'SVN_CREDENTIALS_PSW')
        ])
        timeout(time:"${params.timeout}", unit: 'HOURS')
    }



    stages{
        stage('prepare'){
            //所有机器并行
            parallel{
                stage('PC_Build1'){
                    agent{ 
                        node {label "PC_Build1"}
                    }
                    steps{
                        UpdateSwordGame()
                    }
                }
                stage('PC_Build2'){
                    agent{ 
                        node {label "PC_Build2"}
                    }
                    steps{
                        UpdateSwordGame()
                        UpdateSwordSVN()
                    }
                }
                stage('PC_Build8'){
                    agent{ 
                        node {label "PC_Build8"}
                    }
                    steps{
                        UpdateSwordGame()
                        UpdateSwordSVN()
                    }
                }
                stage('PC_Build9'){
                    agent{ 
                        node {label "PC_Build9"}
                    }
                    steps{
                        UpdateSwordGame()
                    }
                }
                stage('Mac_JX2Designer'){
                    agent{ 
                        node {label "Mac_JX2Designer"}
                    }
                    steps{
                        UpdateSwordGame()
                    }
                }
                stage('Mac_JX2'){
                    agent{ 
                        node {label "Mac_JX2"}
                    }
                    steps{
                        UpdateSwordGame()
                        UpdateSwordSVN()
                    }
                }
                stage('Mac_WentaoXia'){
                    agent{ 
                        node {label "Mac_WentaoXia"}
                    }
                    steps{
                        UpdateSwordGame()
                        UpdateSwordSVN()
                    }
                }
                stage('Mac_JGX'){
                    agent{ 
                        node {label "Mac_JGX"}
                    }
                    steps{
                        UpdateSwordGame()
                        UpdateSwordSVN()
                    }
                }
            }
        }
    }
}

void UpdateSwordGame(){
    if (env.ORIGIN_SWORDGAME_GIT_PATH == ""){
        println "environment variables ORIGIN_SWORDGAME_GIT_PATH does not exist"
        currentBuild.result = "UNSTABLE"
        return
    }
    // Remove http://
    def GitUrlWithoutHttp = "http://git.jx2.bjxsj.site/pirate/SwordGame.git".substring(7)

    //是否存在公用git文件夹
    if(!fileExists("${env.ORIGIN_SWORDGAME_GIT_PATH}"))
    {
        println "Folder ${env.ORIGIN_SWORDGAME_GIT_PATH} does not exist"
        currentBuild.result = "UNSTABLE"
    }

    dir("${env.ORIGIN_SWORDGAME_GIT_PATH}") {

        // 更新git库
        sh """
            git remote set-url origin http://${GIT_CREDENTIALS}@${GitUrlWithoutHttp} &&
            git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" &&
            git fetch &&
            git clean -f -d &&
            git reset --hard HEAD &&
            git submodule foreach git reset --hard &&
            git checkout master &&
            git pull &&
            git lfs install &&
            git lfs pull &&
            git submodule sync --recursive &&
            git submodule foreach git fetch &&
            git submodule update
        """
    }
}

void UpdateSwordSVN() {
    if (env.ORIGIN_SWORDGAME_SVN_PATH == ""){
        println "environment variables ORIGIN_SWORDGAME_SVN_PATH does not exist"
        currentBuild.result = "UNSTABLE"
        return
    }

    //是否存在公用 SVN 文件夹
    if(!fileExists("${env.ORIGIN_SWORDGAME_SVN_PATH}"))
    {
        println "Folder ${env.ORIGIN_SWORDGAME_SVN_PATH} does not exist"
        currentBuild.result = "UNSTABLE"
        return
    }

    packageLib.UpdateContentSVN(env.ORIGIN_SWORDGAME_SVN_PATH, "https://svn.shiyou.kingsoft.com/repos/SwordGame/trunk/Program/Content")
}