@Library("SwordGamePipelineLibrary") _

def GWorkSpaceAbsPath 
def GSwordGameAbsPath
def GSourcePath
pipeline{
    parameters{
        choice(choices: ['Linux_FT'], description: '', name: 'NODE')
        booleanParam(defaultValue: true, description: '', name: 'BUILD_ALL')
        string(defaultValue: 'https://svn.shiyou.kingsoft.com/repos/SwordGame/trunk/Program/Content/GameData', description: 'Content SVN地址', name: 'SVN_PATH', trim: true)
    }
    agent{
        node{
            label "${params.NODE}"
        }
        
    }
    options{
        withCredentials([
            usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS'),
            usernamePassword(credentialsId: '092907a7-dcbf-425b-bc2d-c61715b73536', usernameVariable: 'SVN_CREDENTIALS_USR', passwordVariable: 'SVN_CREDENTIALS_PSW')
        ])
    }
    stages{
        stage("Preparing"){
            steps{
                script{
                    GWorkSpaceAbsPath = env.WORKSPACE
                    println GWorkSpaceAbsPath
                    GSwordGameAbsPath = "${GWorkSpaceAbsPath}/SwordGame"
                    GSourcePath = "${GSwordGameAbsPath}/Source"
                }
            }
        }
        stage("Checkout Git"){
            steps{
                script{
                    if (!fileExists("${GSwordGameAbsPath}")){
                        sh """mkdir SwordGame
                        cd SwordGame
                        git init
                        git remote add -f origin http://${GIT_CREDENTIALS}@git.jx2.bjxsj.site/pirate/SwordGame.git
                        git config core.sparsecheckout true
                        echo /Source > .git/info/sparse-checkout
                        echo /SvnTag.ini >> .git/info/sparse-checkout
                        echo /Scripts >> .git/info/sparse-checkout
                        git pull origin master
                        """
                    }else{
                        dir("${GSwordGameAbsPath}"){
                            if (params.BUILD_ALL == true ){
                                sh "git clean -d -f"
                            }
                            sh """
                                git remote set-url origin http://${GIT_CREDENTIALS}@git.jx2.bjxsj.site/pirate/SwordGame.git                   
                                git fetch --all
                                git reset --hard origin/master
                                git pull origin master
                            """
                        }
                    }
                }
            }
        }
        stage("Checkout Svn"){
            steps{
                script{
                    dir("${GSwordGameAbsPath}"){
                        if(!fileExists("${GSwordGameAbsPath}/Content")){
                            sh "svn checkout --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} ${params.SVN_PATH} Content/GameData"
                        }else{
                            dir("${GSwordGameAbsPath}/Content/GameData"){
                                sh """
                                    svn cleanup --remove-unversioned
                                    svn revert -R .
                                    svn update --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} .
                                """
                            }
                        }
                    }
                }
            }
        }
        stage("Build Sever"){ 
            steps{
                withEnv(['LD_LIBRARY_PATH=\\\$LD_LIBRARY_PATH:/usr/local/lib',
                        "CUSTOM_CMAKE_MODULE=${GSourcePath}/CMakeModules",
                        "THIRDPARTY_PATH=${GSwordGameAbsPath}/linux_libs",
                        "SWORDGAME_HOME=${GSwordGameAbsPath}"
                ]) {
                    script{
                        def BuildSrcPaths=['NoahSDK/Noah3rdparty/Lua','NoahSDK/Noah3rdparty/Zip','NoahSDK/NoahFoundation','NoahSDK/NoahNetwork','NoahSDK/ServerCore','NoahSDK/TencentLog','Tools/IDIP']
                        def BuildAllSrcPaths =['NoahSDK/Noah3rdparty/mongo-c-driver-1.6.1/src/libbson','NoahSDK/Noah3rdparty/mongo-c-driver-1.6.1','NoahSDK/Noah3rdparty/nedmalloc']
                        def SrcPaths = BuildSrcPaths

                        if (params.BUILD_ALL == true){
                            sh """
                                cd ${GSwordGameAbsPath}
                                rm -rf linux_libs
                                mkdir linux_libs linux_libs/acl linux_libs/acl/lib linux_libs/acl/build linux_libs/curl linux_libs/jemalloc_build
                                mkdir linux_libs/jemalloc linux_libs/jemalloc/lib linux_libs/jemalloc/include
                            """
                            sh """
                                cd ${GSourcePath}/NoahSDK/Noah3rdparty/openssl-1.0.2o/
                                dos2unix config
                                chmod a+x config
                                chmod a+x util/*
                                ./config no-shared -fPIC --prefix=${THIRDPARTY_PATH}/openssl && make clean && make -j 2 && make install
                            """
                            sh """
                                cd ${GSourcePath}
                                cp NoahSDK/Noah3rdparty/acl/acl*.zip ${THIRDPARTY_PATH}/acl/build
                                cd ${THIRDPARTY_PATH}/acl/build && unzip acl*.zip
                                make all -j 2 && make install

                                cp -r dist/include ../ && cp dist/lib/linux64/lib_acl.a  ../lib/libacl.a && cp dist/lib/linux64/lib_acl_cpp.a ../lib/libacl_cpp.a

                                cd ${GSourcePath}
                                cd NoahSDK/Noah3rdparty/curl-7.60.0/
                                chmod u+x configure
                                ./configure --disable-shared --enable-static --prefix=${THIRDPARTY_PATH}/curl
                                make clean
                                make -j 2 && make install 
                            """
                            sh """
                                cd ${GSourcePath}
                                if [ -f "NoahSDK/Noah3rdparty/jemalloc_linux/jemalloc-5.1.0.tar.bz2" ]; then
                                    cd NoahSDK/Noah3rdparty/jemalloc_linux/
                                    cp jemalloc-5.1.0.tar.bz2 ${THIRDPARTY_PATH}/jemalloc_build
                                    cd ${THIRDPARTY_PATH}/jemalloc_build
                                    tar xvf jemalloc-5.1.0.tar.bz2
                                    cd jemalloc-5.1.0
                                    ./configure --prefix=${THIRDPARTY_PATH}/jemalloc_build/jemalloc-5.1.0/lib --with-mangling --with-jemalloc-prefix=je CXXFLAGS=-fPIC CFLAGS=-fPIC

                                    make -j 2 && make install 

                                    cp lib/include/jemalloc/*.h ${THIRDPARTY_PATH}/jemalloc/include/ -f
                                    cp lib/libjemalloc.a ${THIRDPARTY_PATH}/jemalloc/lib/ -f
                                fi
                            """
                            SrcPaths = BuildAllSrcPaths + BuildSrcPaths
                        }
                        println SrcPaths
                        for (int i=0; i < SrcPaths.size(); ++i){
                            SrcPath = SrcPaths[i]
                            sh """
                                cd ${GSourcePath}/${SrcPath}
                                rm -rf Build_; mkdir Build_; cd Build_;
                                cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DJEMALLOC_SWITCH:BOOL=TRUE -G "Unix Makefiles" ../
                                make -j 2 && make install ;
                            """
                        }

                        dir ("${GSourcePath}/Server"){
                            sh """
                                rm -rf Build_; mkdir Build_; cd Build_;
                                cmake -DNOAH_UNITY_BUILD:BOOL=TRUE -DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_PROXY:BOOL=TRUE -DJEMALLOC_SWITCH:BOOL=TRUE -G "Unix Makefiles" ../
                                make -j 2 && make install
                            """
                        }
                    }
                }
            }
        }
        stage("Preparing Publish"){
            steps{
                script{
                    dir("${GSourcePath}"){
                        sh "rm -rf .publish"
                        sh "mkdir .publish"
                    }
                    dir("${GSourcePath}/.publish"){
                        sh """
                            mkdir idip
                            mkdir online
                            mkdir web
                            mkdir server
                        """

                        sh """cp -R ${GSourcePath}/Tools/IDIP/Build_/SDK/bin/* ${GSourcePath}/.publish/idip
                            cd ${GSourcePath}/.publish/idip/conf
                            rm -rf *

                            cp -R ${GSourcePath}/Server/PHP/online/* ${GSourcePath}/.publish/online
                            cp -R ${GSourcePath}/Server/PHP/web/* ${GSourcePath}/.publish/web

                            cd ${GSourcePath}/Server/Build_/SDK/bin/Config
                            rm -rf .svn */.svn */*/.svn */*/*/.svn *.ini *.xml */*/*/*/.svn */*/*/*/*/.svn ProtocolExport.json

                            cd ${GSourcePath}/Server/Build_/SDK/bin/bin
                            cp Server Pvp

                            cp -R ${GSourcePath}/Server/Build_/SDK/bin/* ${GSourcePath}/.publish/server


                            cd ${GSourcePath}/.publish
                            tar -zcvf server.tar.gz *
                            echo "this is a notify file" > notify.txt
                        """
                    }
                }
            }
        }
        stage("PublishSever"){
            steps{
                script{
                    dir("${GSwordGameAbsPath}"){
                        sshPublisher(publishers: [sshPublisherDesc(configName: '上海游戏服务器', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: 'tmp/', remoteDirectorySDF: false, removePrefix: 'Source/.publish/', sourceFiles: 'Source/.publish/server.tar.gz'), sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: 'tmp/', remoteDirectorySDF: false, removePrefix: 'Source/.publish/', sourceFiles: 'Source/.publish/notify.txt')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
                    }
                }
            }
        }
    }
    post{
        success {
            script {
                echo "---------------------- finish by success ---------------"
                String title = "DailyBuild服务器更新发布"
                String content = "结果：成功\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>"
                packageLib.SendMessageWithLark(content,title,env.LARK_CUSTOMBOT_URL_JIANGEXING)
            }
        }
        failure {
            script {
                echo "---------------------- finish by failure ---------------"
                String title = "DailyBuild服务器更新发布"
                String content = "结果：失败\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>"
                packageLib.SendMessageWithLark(content,title,env.LARK_CUSTOMBOT_URL_JIANGEXING)
            }
        }
    }
}
