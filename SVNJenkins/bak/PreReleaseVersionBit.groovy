pipeline{
    parameters{
        choice(choices: ['JXPortal'], description: '打包机', name: 'NODE')
        string(name:'VersionBit', defaultValue:'4', description:'填写升第几位版本号')
        string(name:'SVN_PATH', defaultValue:'', description:'SVN地址')
    }

    agent{
        node{
            label "${NODE}"
        }
        
    }

    options{
        withCredentials([
            usernamePassword(credentialsId: '092907a7-dcbf-425b-bc2d-c61715b73536', usernameVariable: 'SVN_CREDENTIALS_USR', passwordVariable: 'SVN_CREDENTIALS_PSW')
        ])
    }

    stages{
        stage('CheckoutSVN'){
            steps{
                script{
                    //checkout Common文件夹
                    dir("${env.WORKSPACE}"){
                        sh "svn checkout --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW} ${params.SVN_PATH}/GameData/Common"
                    }
                }
            }
        }

        stage('UpdateVersion'){
            steps{
               script{
                    //先临时使用VersionChanger
                    def COMMON_DIR = "${env.WORKSPACE}/Common" 
                    bat "D:/Tools/VersionChanger.exe ${COMMON_DIR}/CommonConfig.lua ${VersionBit} ${COMMON_DIR}/ShortVersion.txt ${COMMON_DIR}/LongVersion.txt"
                    LongVersion = readFile("${COMMON_DIR}/LongVersion.txt")

                    dir ("${COMMON_DIR}"){
                        sh "svn commit -m \"Update Project Version to ${LongVersion}\" "
                    }
                    //删除common 每次都重新checkout 避免出问题
                    dir("${env.WORKSPACE}"){
                        sh "rm -rf Common"
                    }
               }
            }
        }
    }
} 