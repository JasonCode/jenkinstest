#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
import os
import sys
# import ConfigParser


# def ChangeConfig(configPath,section,changeKey,changeValue):
# 	config = ConfigParser.ConfigParser() # 类实例化
# 	config.read(configPath)
# 	config.set(section,changeKey,changeValue) 
# 	config.write(open(configPath,'w+')) 

def ChangeConfig(configPath,changeKey,changeValue):
	with  open(configPath) as f:
		lines = f.readlines()
		f.close()


	cf = open(configPath, 'w+')
	cf.truncate()

	for line in lines:
		templine = line
		strlist = templine.split('=')
		if len(strlist) == 2:
			# print(strlist[0])
			if strlist[0] == changeKey :
				templine = "%s=%s\n" %(changeKey,changeValue)
		cf.write(templine)
	
	cf.close()

if __name__ == "__main__":
	# 上传单个文件
	# my_ftp.UploadFile("G:/ftp_test/Release/XTCLauncher.apk", "/App/AutoUpload/ouyangpeng/I12/Release/XTCLauncher.apk")

	if len(sys.argv) >= 3:
		print(sys.argv)
		valueParam = sys.argv[3]
		if("C:/Users/a/AppData/Local/Atlassian/SourceTree/git_local" in valueParam):
			valueParam = valueParam.replace("C:/Users/a/AppData/Local/Atlassian/SourceTree/git_local","")

		ChangeConfig(sys.argv[1],sys.argv[2],valueParam)
		
	else:
		raise Exception("调用python参数错误")
	