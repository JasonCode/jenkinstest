@Library("SwordGamePipelineLibrary") _
pipeline {
    parameters{
        string(name:'GitBranch', defaultValue:'release', description:'部署 Nemesis 分支名')
        
        booleanParam(defaultValue: true, description: 'Shoud client be deployed', name: 'DeployClient')
        booleanParam(defaultValue: true, description: 'Shoud server be deployed', name: 'DeployServer')
        booleanParam(defaultValue: true, description: '下载代码', name: 'DownloadGit')
        booleanParam(defaultValue: true, description: '代码检查', name: 'CodeCheck')
        booleanParam(defaultValue: true, description: '检查不失败',name: 'CheckUnstable')

        choice(choices: ['Linux_FT', 'Linux_DDC'], description: 'Select which machine to be deployed', name: 'DeployMachine')
    }

    agent {
        node {
            label "${DeployMachine}"
        }
    }

    stages{

        stage("Prepare"){
            steps{
                script {
                    buildName params.GitBranch + "@" + params.DeployMachine + "#${BUILD_NUMBER}"

                    if(DeployMachine == "Linux_DDC") {
                        input message: 'Do you want to deploy to the production Server?', ok: 'Yes! Sure about it'
                    }
                }
            }
        }

        stage("Download Git"){
            steps{
                script{
                  if(!params.DownloadGit) {
                        echo "Skip Download Git."
                        return
                    }
                    ws("${WORKSPACE}/OlympusCheckout")
                    {

                    checkout([$class: 'GitSCM', branches: [[name: "*/${GitBranch}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'GitLFSPull'], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', url: 'http://git.jx2.bjxsj.site/pirate/Olympus.git']]])
                    }
                }
            }
        }
        stage("Client Code Check"){
            steps{
                script {
                    if(!params.CodeCheck) {
                        echo "Skip client code check."
                        return
                    }
                    try{
                        sh """
                            cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxClient
                            npm install
                            ./node_modules/.bin/eslint -c .eslintrc.json . --ext .ts
                        """
                        
                    }catch(err){    
                        if(params.CheckUnstable){
                            currentBuild.result = 'UNSTABLE'
                        }else{
                            currentBuild.result = 'FAILURE'
                        }
                    }
                        
                    echo "Client code check finished."
                }
            }
        }
        stage("Client Deploy"){
            steps{

                script {
                    if(!params.DeployClient) {
                        echo "Skip Client deploy."
                        return
                    }

                    def BuildFlag = "--configuration=devtest"

                    if(DeployMachine == "Linux_DDC") {
                        BuildFlag = "--prod"
                    }

                    echo BuildFlag
                    if(!params.CodeCheck){
                        sh """
                        cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxClient
                        npm install
                    """
                    }
                    sh """
                        cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxClient
                        ng build ${BuildFlag} --output-path /var/www/nemesis/
                    """
                    
                    echo "Client build finished"
                }                
            }
        }
        stage("Sever Mod Download"){
            steps {
                script {
                    sh '''
                        cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer 
                        go env -w GO111MODULE=on
                        go env -w GOPROXY=https://goproxy.cn,direct
                        go mod download
                        go mod vendor
                    '''
                    
                    echo "Server server mod download finished"
                }
            }
        }
        stage("Sever Code Check"){
            steps {
                script {
                    if(!params.CodeCheck) {
                        echo "Skip server code check."
                        return
                    }
                    try{
                        sh '''
                            cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer/src 
                            ~/go/bin/revive ./... > CheckLog.txt
                            cat CheckLog.txt
                            go vet
                        '''
                        dir("${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer/src"){
                            if (readFile("CheckLog.txt")){
                                sh "exit 1"
                            }
                        }
                    }catch(err){
                        if(params.CheckUnstable){
                            currentBuild.result = 'UNSTABLE'
                        }else{
                            currentBuild.result = 'FAILURE'
                        }
                    }
                    echo "Server code check finished"
                }
            }
        }
        stage("Sever Build"){
            steps {

                script {
                    if(!params.DeployServer) {
                        echo "Skip Server deployment."
                        return
                    }

                    sh '''
                        cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer/src 
                        go build -o nemesis 
                    '''
                    echo "Server build finished"
                }

            }
        }
        
        stage("kill Process"){
            steps{

                script {
                    if(!params.DeployServer) {
                        echo "Skip Server deployment."
                        return
                    }

                    // 判断 nemesis 的进程是否存在，如果不存在则重新启动，如果存在则kill掉该进程
                    sh label: 'Kill Nemesis', returnStatus: true, script: 'killall nemesis'
                    sh "sleep 30"
                }


            }
        }
            
        stage("Sever Deploy"){
            steps{

                script {
                    if(!params.DeployServer) {
                        echo "Skip Server deployment."
                        return
                    }

                    withEnv(['JENKINS_NODE_COOKIE=dontkillme', 'GIN_MODE=release']) {
                        sh'''
                            cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer/src
                            cp -rf nemesis /var/nemesis
                            cd ${WORKSPACE}/OlympusCheckout/Nemesis/UboxServer
                            cp -rf ./config /var/nemesis
                            cd /var/nemesis
                            mv -f nemesis.log nemesis_$(date +%Y%m%0d_%H%M%S).log || echo "move nemesis.log failed"
                            mv -f nohup.log nohup_$(date +%Y%m%0d_%H%M%S).log || echo "move nohup.log failed"
                            nohup ./nemesis > nohup.log 2>&1 &
                        '''
                    
                    }
                }


            }
        }
    }

    post{
        success {
            script {
                echo "---------------------- finish by success ---------------"
                String title = "Nemesis 程序发布"
                String content = "结果：成功\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>"
                packageLib.SendMessageWithLark(content,title,env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
        failure {
            script {
                echo "---------------------- finish by failure ---------------"
                String title = "Nemesis 程序发布"
                String content = "结果：失败\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>"
                packageLib.SendMessageWithLark(content,title,env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
        unstable {
            script {
                echo "---------------------- finish by unstable ---------------"
                String title = "Nemesis 程序发布"
                String content = "结果：unstable\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>"
                packageLib.SendMessageWithLark(content,title,env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
    }
    
}