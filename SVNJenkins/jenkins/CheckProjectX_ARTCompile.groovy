@Library("PLPipelineLibrary") _

pipeline {
	parameters{
	    choice(choices: ['WinBuild02','WinBuild01','master'], description: '选择打包机', name: 'NODE')
		string(name:'GameFolder', defaultValue:'C:/ProjectX_UE5', description:'游戏根目录')
		string(name:'GameWorkSpace', defaultValue:'XBuild01_ProjectX_UE5', description:'游戏的工作区')
		booleanParam(defaultValue: true, description: 'CheckoutGame？', name: 'bCheckoutGame')
		booleanParam(defaultValue: true, description: 'Compile？', name: 'bCompile')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package Android阶段?', name: 'bPackageAndroid')
		string(name:'info', defaultValue:'wxLog', description:'微信log', trim:true)
		string(name:'changelist', defaultValue:'1', description:'changelist', trim:true)
		booleanParam(defaultValue: true, description: '打包集合导出？', name: 'bArchive')
		string(defaultValue: "C:/projX_UE5_PackageArchive", description: '打包集合导出路径', name: 'bArchiveFolder')
		booleanParam(defaultValue: false, description: 'CopyStream', name: 'bCopyStream')
	}
	
	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
				    echo "changelist"
				    echo params.changelist
				    echo "changelist"
				    gameFolder=params.GameFolder+"/ProjectX"
                    engineFolder=params.GameFolder
                    projName="XGame"

					packageUtil.Reset()
					packageUtil.SetUeExeName("UnrealEditor-Cmd.exe")
					packageUtil.SetGameFolder(gameFolder)
					packageUtil.SetEngineFolder(engineFolder)
					packageUtil.SetProjectName(projName)

					packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Win64)
					packageUtil.SetPak(true)

					packageUtil.SetTargetPlatform(CommonConfig.Plat_Android)
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetAchive(params.bArchive)
					packageUtil.SetAchiveFolder(params.bArchiveFolder)
					packageUtil.OnInitialize()
				}
			}
		}


		stage("Checkout Game"){
			steps{
				script{
					if(params.bCheckoutGame)commonUtil.SyncWorkspace(params.GameWorkSpace, engineFolder, params.changelist)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompile)commonUtil.CompileEngineOn(CommonConfig.OS_Windows, engineFolder, "UE5", "UnrealEditor")
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					if(params.bCompile)commonUtil.CompileGameOn(CommonConfig.OS_Windows, engineFolder, gameFolder, projName)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackageAndroid)packageUtil.StartPackage()
				}
			}
		}

		stage("上传二进制"){
			steps{
				script{
					echo "update binary"
					String cmd = "python ./third_tool/PyUpProjext_UE5_package.py ${params.bArchiveFolder}"
	                sh "${cmd}"
				}
			}
		}

		stage("拷贝Stream"){
			steps{
				script{
					echo "Copy Stream"
					stream_workspace = "XBuild01_Project_Station"
					stream_srcStream = "//Project_Station/main"
					stream_dstStream = "//Project_Station/ProjectX_Env"
					stream_fold = "c:/mergeStream/XBuild01_Project_Station"
					if(params.bCopyStream)commonUtil.MergeStream(stream_workspace, stream_srcStream, stream_dstStream, stream_fold)
					if(params.bCopyStream)commonUtil.CopyStream(stream_workspace, stream_dstStream, stream_srcStream, stream_fold)
				}
			}
		}
	}

	post{
		failure {script {messageUtil.SendWXMsg("编译ProjectX_UE5检查", "main", "Failure", params.info)}}
		unstable {script {messageUtil.SendWXMsg("编译ProjectX_UE5检查", "main", "Unstable", params.info)}}
		success {script {messageUtil.SendWXMsg("编译ProjectX_UE5检查", "main", "Success", params.info)}}
	}

//--------- end of pipeline	
}
