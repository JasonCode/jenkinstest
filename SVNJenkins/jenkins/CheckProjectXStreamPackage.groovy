@Library("PLPipelineLibrary") _

pipeline {
	parameters{

	    choice(choices: ['master', 'WinBuild01'], description: '选择打包机', name: 'NODE')
		string(name:'EngineFolder', defaultValue:'C:/UnrealEngine', description:'引擎根目录')
		string(name:'GameFolder', defaultValue:'C:/ProjectX_Stream', description:'游戏根目录')
        string(name:'EngineWorkSpace', defaultValue:'XBuild01_UE', description:'引擎的工作区')
		string(name:'GameWorkSpace', defaultValue:'XBuild01_ProjectX_Stream', description:'游戏的工作区')
		string(name:'GameWorkSpaceStream', defaultValue:'//ProjectX/dev', description:'游戏工作区的stream')

		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package Android阶段?', name: 'bPackageAndroid')
		string(name:'info', defaultValue:'wxLog', description:'微信log', trim:true)
		booleanParam(defaultValue: true, description: '打包集合导出？', name: 'bArchive')
		string(defaultValue: "C:/projextX_Stream_PackageArchive", description: '打包集合导出路径', name: 'bArchiveFolder')
	}
	
	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
				    gameFolder=params.GameFolder
                    engineFolder=params.EngineFolder
                    String[] str
                    str = params.GameWorkSpaceStream.split('/');
                    archiveFolder=params.bArchiveFolder + "/" + str[-1]


					packageUtil.Reset()
					packageUtil.SetGameFolder(gameFolder)
					packageUtil.SetEngineFolder(engineFolder)

					packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Win64)
					packageUtil.SetPak(true)

					packageUtil.SetTargetPlatform(CommonConfig.Plat_Android)
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetAchive(params.bArchive)
					packageUtil.SetAchiveFolder(archiveFolder)
					packageUtil.OnInitialize()
				}
			}
		}
		stage("Switch Stream"){
			steps{
				script{
					commonUtil.SwitchStream(params.GameWorkSpace, gameFolder, params.GameWorkSpaceStream)
				}
			}
		}


		stage("Checkout Game"){
			steps{
				script{
					commonUtil.SyncWorkspace(params.GameWorkSpace, gameFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					commonUtil.CompileEngineOn(CommonConfig.OS_Windows, engineFolder)
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					commonUtil.CompileGameOn(CommonConfig.OS_Windows, engineFolder, gameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackageAndroid)packageUtil.StartPackage()
				}
			}
		}
	}



//--------- end of pipeline	
}
