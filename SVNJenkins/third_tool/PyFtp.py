from ftplib import FTP
import os
import sys
import time
import socket

class JenkinsFTP:
	#host:ip地址
	def __init__(self, host, port=21):
		self.host = host
		self.port = port
		self.ftp = FTP()

	def login(self, username, password):
		try:
			timeout = 60
			socket.setdefaulttimeout(timeout)
			# 0主动模式 1 #被动模式
			self.ftp.set_pasv(False)
			# 打开调试级别2，显示详细信息
			# self.ftp.set_debuglevel(2)

			print('开始尝试连接到 %s' % self.host)
			self.ftp.connect(self.host, self.port)
			print('成功连接到 %s' % self.host)

			print('开始尝试登录到 %s' % self.host)
			self.ftp.login(username, password)
			print('成功登录到 %s' % self.host)
		except Exception as err:
			self._RaiseException("FTP 连接或登录失败 ，错误描述为：%s" % err)

	def UploadFile(self, local_path, remote_path):
		if not os.path.isfile(local_path):
			self._RaiseException('%s 不存在' % local_path)

		buf_size = 1024
		file_handler = open(local_path, 'rb')
		self.ftp.storbinary('STOR %s' % remote_path, file_handler, buf_size)
		file_handler.close()

		print('上传: %s' % local_path + "成功!")

	def UploadFolder(self, local_folder, remote_folder):
		print("UploadFolder()---> local_folder  %s" % local_folder)
		print("UploadFolder()---> remote_folder %s" % remote_folder)

		if not os.path.isdir(local_folder):
			self._RaiseException('本地目录 %s 不存在' % local_folder)

		try:
			print("remote_folder is " + remote_folder)
			self.ftp.cwd(remote_folder)  # 切换工作路径
		except Exception as err:
			self.ftp.mkd(remote_folder)
			self.ftp.cwd(remote_folder)

		local_name_list = os.listdir(local_folder)
		for local_name in local_name_list:
			src = os.path.join(local_folder, local_name)
			print("src路径=========="+src)
			if os.path.isdir(src):
				ftp_folder = local_name
				self.UploadFolder(src, ftp_folder)
			else:
				self.UploadFile(src, local_name)
		self.ftp.cwd("..")


	def Close(self):
		print("close()---> FTP退出")
		self.ftp.quit()

	def _RaiseException(self, content):
		raise Exception(content)
		sys.exit(-1)

	def _GetFileList(self, line):
		file_arr = self._GetFileName(line)
		# 去除  . 和  ..
		if file_arr[1] not in ['.', '..']:
			self.file_list.append(file_arr)

	def _GetFileName(self, line):
		pos = line.rfind(':')
		while (line[pos] != ' '):
			pos += 1
		while (line[pos] == ' '):
			pos += 1
		file_arr = [line[0], line[pos:]]
		return file_arr


if __name__ == "__main__":
	# 上传单个文件
	# my_ftp.UploadFile("G:/ftp_test/Release/XTCLauncher.apk", "/App/AutoUpload/ouyangpeng/I12/Release/XTCLauncher.apk")

	# if len(sys.argv) >= 3:
		print(sys.argv)

		my_ftp = JenkinsFTP("10.0.13.37")
		# my_ftp.login("ftp", "123")
		my_ftp.login("1", "share123")
		my_ftp.UploadFolder(sys.argv[1], sys.argv[2])
		my_ftp.Close()
	# else:
	# 	raise Exception("调用python参数错误")
	