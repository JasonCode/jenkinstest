@Library("PLPipelineLibrary") _

pipeline {
	parameters{
	    choice(choices: ['WinBuild01', 'master'], description: '选择打包机', name: 'NODE')
		string(name:'EngineFolder', defaultValue:'C:/EpicGames/UE_4.26', description:'引擎根目录')
		string(name:'GameFolder', defaultValue:'C:/ProjectX_Binaries', description:'游戏根目录')
		string(name:'GameWorkSpace', defaultValue:'XBuild01_ProjectX_Binaries', description:'游戏的工作区')
		string(name:'info', defaultValue:'lack', description:'微信log')
	}
	
	agent{
		node{
			label "${params.NODE}"
		}

	}


	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					packageUtil.Reset()
					packageUtil.SetGameFolder(params.GameFolder)
					packageUtil.SetEngineFolder(params.EngineFolder)
					packageUtil.SetServerTargetPlatform("${CommonConfig.Plat_Linux}+${CommonConfig.Plat_Win64}")
					packageUtil.SetPak(false)
					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Game"){
			steps{
				script{
					commonUtil.SyncWorkspace(params.GameWorkSpace, params.GameFolder)
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					commonUtil.CompileLauncherGameOn(CommonConfig.OS_Windows, params.EngineFolder, params.GameFolder)
				}
			}
		}

		stage("上传二进制"){
			steps{
				script{
					echo "update binary"
					String cmd = "python ./third_tool/PyUpBinaries.py ${GameFolder}"
	                sh "${cmd}"
				}
			}
		}

	}

	post{
		failure {script {messageUtil.SendWXMsg("Laucher二进制编译检查", "main", "Failure", params.info)}}
		unstable {script {messageUtil.SendWXMsg("Laucher二进制编译检查", "main", "Unstable", params.info)}}
		//success {script {messageUtil.SendWXMsg("Laucher二进制编译检查", "main", "Success", params.info)}}
	}

//--------- end of pipeline	
}
