@Library("PLPipelineLibrary") _

pipeline {
	parameters{
		string(name:'EngineFolder', defaultValue:'C:/XBuild01_ProjectX_Dev', description:'引擎根目录')
		string(name:'GameFolder', defaultValue:'C:/XBuild01_ProjectX_Dev/ProjectX', description:'游戏根目录')
		// string(name:'EngineWorkSpace', defaultValue:'XBuild01_ProjectX_Dev', description:'引擎的工作区')
		booleanParam(defaultValue: true, description: 'CopyStream', name: 'bCopyStream')
	}


	agent{ node{ label 'WinBuild-13.37' } }

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					packageUtil.Reset()
					packageUtil.SetGameFolder(params.GameFolder)
					packageUtil.SetEngineFolder(params.EngineFolder)
					packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Win64)
					packageUtil.SetPak(false)
					packageUtil.OnInitialize()
				}
			}
		}

		// stage("Checkout Engine"){
		// 	steps{
		// 		script{
		// 			commonUtil.SyncWorkspace(params.EngineWorkSpace, params.EngineFolder)
		// 		}
		// 	}
		// }


		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Cook"){
			steps{
				script{
					packageUtil.StartBuildCook()
				}
			}
		}
		stage("拷贝Stream"){
			steps{
				script{
					echo "Copy sourcecode Stream"
					stream_workspace = "XBuild01_ProjectX_SourceCode"
					stream_srcStream = "//ProjectX_SourceCode/main"
					stream_dstStream = "//ProjectX_SourceCode/dev"
					stream_fold = "c:/mergeStream/XBuild01_ProjectX_SourceCode"
					if(params.bCopyStream)commonUtil.MergeStream(stream_workspace, stream_srcStream, stream_dstStream, stream_fold)
					if(params.bCopyStream)commonUtil.CopyStream(stream_workspace, stream_dstStream, stream_srcStream, stream_fold)

					echo "merge Stream"
					stream_workspace = "XBuild01_Project_Station"
					stream_srcStream = "//Project_Station/main"
					stream_dstStream = "//Project_Station/dev"
					stream_fold = "c:/mergeStream/XBuild01_Project_Station"
					if(params.bCopyStream)commonUtil.MergeStream(stream_workspace, stream_srcStream, stream_dstStream, stream_fold)
				}
			}
		}
	}

	post{
		failure {script {messageUtil.SendWXMsg("Cook检查", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("Cook检查", "main", "Unstable")}}
		//success {script {messageUtil.SendWXMsg("Cook检查", "main", "Success")}}
	}

//--------- end of pipeline	
}
