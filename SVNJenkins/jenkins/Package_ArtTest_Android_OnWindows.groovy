@Library("PLPipelineLibrary") _

pipeline {
	//options {
		//timeout(time: 30, unit: 'SECONDS')
	//}

	parameters{
		choice(choices: ['master', 'WinBuild01'], description: '选择打包机', name: 'NODE')

		string(name:'PackageLabel', defaultValue:'', description:'--------------------------------------------------------------------------------------------------------------')
		booleanParam(defaultValue: false, description: '更新引擎?', name: 'bUpdateEngine')
		booleanParam(defaultValue: false, description: '编译引擎?', name: 'bCompileEngine')
		booleanParam(defaultValue: true, description: '更新游戏?', name: 'bUpdateGame')
		booleanParam(defaultValue: true, description: '编译游戏?', name: 'bCompileGame')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package Android阶段?', name: 'bPackageAndroid')
		booleanParam(defaultValue: true, description: '上传FTP?', name: 'bUploadFtp')
		booleanParam(defaultValue: true, description: '打包集合导出？', name: 'bArchive')
		string(defaultValue: "C:/packageArchive_ArtTest", description: '打包集合导出路径', name: 'bArchiveFolder')
		string(defaultValue: "/Game/Debug/AssetPreview/AssetPreview.AssetPreview", description: '启动map', name: 'TargetMap')
		string(defaultValue: "-map=AssetPreview", description: 'cook  map  参数', name: 'CookMapParam')
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		EngineFolder = 'C:/UnrealEngine'
		GameFolder = 'C:/ProjectX_ArtTest_Android'
		EngineWorkSpace = 'XBuild01_UE'
		GameWorkSpace = 'XBuild01_ProjectX_ArtTest_Android'
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
					packageUtil.Reset()
					packageUtil.SetGameFolder(GameFolder)
					packageUtil.SetEngineFolder(EngineFolder)

					packageUtil.SetTargetPlatform(CommonConfig.Plat_Android)
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetPak(true)
					packageUtil.SetPackageServer(false)

					packageUtil.SetAchive(params.bArchive)
					packageUtil.SetAchiveFolder(params.bArchiveFolder)

					packageUtil.SetCookMapParam(params.CookMapParam)

					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
					if(params.bUpdateEngine)
						commonUtil.SyncWorkspace(EngineWorkSpace, EngineFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompileEngine)
						commonUtil.CompileEngineOn(CommonConfig.OS_Windows, EngineFolder)
				}
			}
		}

		stage("Checkout Game"){
			steps{
				script{
					if(params.bUpdateGame)
						commonUtil.SyncWorkspace(GameWorkSpace, GameFolder)
						commonUtil.ChangeConfig(GameFolder + "/Config/DefaultEngine.ini","GameDefaultMap",params.TargetMap)
						commonUtil.ChangeConfig(GameFolder + "/Config/DefaultEngine.ini","ServerDefaultMap",params.TargetMap)
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					if(params.bCompileGame)
						commonUtil.CompileGameOn(CommonConfig.OS_Windows, EngineFolder, GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)
						packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackageAndroid)packageUtil.StartPackage()
				}
			}
		}

		stage("UploadFTP"){
			steps{
				script{
					if(params.bUploadFtp)
						commonUtil.UploadArtTestFolderToFtp(packageUtil.GetPackagedPath(), packageUtil.GetTargetPlatform())
				}
			}
		}

	}

	post{
		//success {script {messageUtil.SendWXMsg("Android打包", "main", "Success")}}
		failure {script {messageUtil.SendWXMsg("Android打包", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("Android打包", "main", "Unstable")}
		}
	}

//--------- end of pipeline	
}
