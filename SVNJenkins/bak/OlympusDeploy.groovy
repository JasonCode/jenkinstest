@Library("SwordGamePipelineLibrary") _

def GNewVersionNumber

pipeline {
    parameters{
        choice(choices: ['Athena','Hestia','Lachesis','Phoebe','Themis', "MSBuildSimple"], description: '选择Olympus中需要发布的工程', name: 'ProjectName')
        string(name:'GitBranch', defaultValue:'release', description:'')
        choice(choices: ['JXPortal'], description: '选择需要部署的服务器', name: 'DeployMachine')
        string(name:'IncrementVersionNumberIndex', defaultValue:'3', description:'版本号样式：1.3.6.7（Major.Minor.Revision.Build），升级采取指定位加1，后置位清零的规则。例如【IncrementVersionNumberIndex】为1，则版本升级为2.0.0.0；再如【IncrementVersionNumberIndex】为4，则版本升级为1.3.6.8。')
    }

    agent {
        node {
            label "${DeployMachine}"
        }
    }

    options{
        withCredentials([
            usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS')
        ])
    }

    stages{

        stage("Download Git"){
            steps{
                dir("InternalProject")
                {
                    script{
                        buildName params.ProjectName + "@" + params.DeployMachine + "#${BUILD_NUMBER}"

                        checkout([$class: 'GitSCM', branches: [[name: "*/${GitBranch}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'GitLFSPull'], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', url: "http://${GIT_CREDENTIALS}@git.jx2.bjxsj.site/pirate/Olympus.git"]]])
                    }
                }
            }
        }

        stage("Nuget Restore"){
            steps{
                dir("${WORKSPACE}/InternalProject")
                {
                    powershell '''
                    chcp 850
                    ../3rdpartytools/nuget.exe restore Olympus.sln
                    '''
                }
            }
        }

        stage("Git Checkout")
        {
            steps{
                dir("${WORKSPACE}/InternalProject")
                {
                    script {
                        bat "git branch"  
                        bat "git fetch origin ${GitBranch}"                          
                        bat "git checkout -B ${GitBranch}"                          
                        bat "git branch"  
                        bat "git pull origin ${GitBranch}"                          
                    }
                }
            }
        }

        stage("Update Version Number"){
            steps{
                script{
                    echo "Update version number at ${IncrementVersionNumberIndex}"
                    
                    dir("${WORKSPACE}/InternalProject/${ProjectName}")
                    {
                        def UpdateVersionNumber = powershell returnStdout: true, script: """msbuild ${ProjectName}.csproj /t:UpdateVersionNumber /p:UpdateNumberIndex=${IncrementVersionNumberIndex}""", encoding: 'UTF-8'
                        println UpdateVersionNumber
                        x = /New Build Version:\s(\d+\.\d+\.\d+\.\d+)/
                        def matcher = UpdateVersionNumber =~x
                        if(matcher){
                            GNewVersionNumber = matcher[0][1]
                            println GNewVersionNumber
                        }
                    }
                }
            }
        }

        stage("PublishProject"){
            steps{
                dir("${WORKSPACE}/InternalProject/${ProjectName}")
                {
                    powershell(
                        script:"""
                            msbuild ${ProjectName}.csproj /t:publish /P:Configuration=Release /P:Platform=x64 /p:PublishDir=\"${PublishFtpRootPath}/${ProjectName}/\"
                        """,
                        encoding: 'UTF-8'
                    )
                }
            }
        }

        stage("CommitVersion"){
            steps{
                dir("${WORKSPACE}/InternalProject")
                {
                    script {
                        bat "git branch"
                        bat "git add ${ProjectName}/${ProjectName}.csproj"
                        bat "git commit -m \"Olympus Auto Publish :${ProjectName} ${GNewVersionNumber}\""
                        bat "git push origin ${GitBranch}"
                    }
                }
            }
        }
    }

    post{
        success {
            script {
                echo "---------------------- finish by success ---------------"
                String title = "Olympus 程序发布"
                String content = "${ProjectName} ${GNewVersionNumber}\\n发布结果：成功\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>\\n<a href=\\\"http://portal.jx2.bjxsj.site/Olympus/${ProjectName}/publish.htm\\\">安装连接</a>"
                packageLib.SendMessageWithLark(content,title, env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
        failure {
            script {
                echo "---------------------- finish by failure ---------------"
                String title = "Olympus 程序发布"
                String content = "${ProjectName} ${GNewVersionNumber}\\n发布结果：失败\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>\\n"
                packageLib.SendMessageWithLark(content,title, env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
    }
}