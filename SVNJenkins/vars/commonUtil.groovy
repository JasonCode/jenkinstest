import groovy.transform.Field
import java.text.*
import java.util.Date
import groovy.json.JsonOutput



//切换workspace所属stream
void SwitchStream(workspace, folder, dstStream){
	echo "=================================== switch workspace stream: ${workspace} to ${dstStream} at Folder: ${folder}"
	ws(folder) {
		sh """
		    p4 -c ${workspace} client -o
			p4 -c ${workspace} switch -Rn ${dstStream}
		"""
	}
}

//更新代码
void  SyncWorkspace(workspace, folder, changelist="1") {
	echo "=================================== SyncWorkspace workspace: ${workspace} at Folder: ${folder}"
	ws(folder) {
        if (changelist=="1") {
            sh """
			    p4 -c ${workspace} client -o
			    export P4CHARSET=utf8
			    p4 -c ${workspace} diff -sd |p4 -c ${workspace} -x - sync -f
			    p4 -c ${workspace} diff -se |p4 -c ${workspace} -x - sync -f
			    p4 -c ${workspace} sync #head
		    """
        } else{
            sh """
			    p4 -c ${workspace} client -o
			    export P4CHARSET=utf8
			    p4 -c ${workspace} diff -sd |p4 -c ${workspace} -x - sync -f
			    p4 -c ${workspace} diff -se |p4 -c ${workspace} -x - sync -f
			    p4 -c ${workspace} sync @${changelist}
		    """
        }
	}
}

void  MergeStream(stream_workspace, stream_srcStream, stream_dstStream, stream_fold) {
	echo "=================================== MergeStream ${stream_srcStream} to ${stream_dstStream} in workspace: ${stream_workspace} at Folder: ${stream_fold}"
	ws(stream_fold) {

        try
        {
            sh """
            export P4CHARSET=utf8
            p4 -c ${stream_workspace} switch -Rn ${stream_dstStream}
            p4 -c ${stream_workspace} client -o
            p4 -c ${stream_workspace} revert -a -c default
            p4 -c ${stream_workspace} merge -c default ${stream_srcStream}/... ${stream_dstStream}/...
            p4 -c ${stream_workspace} resolve -at
            p4 -c ${stream_workspace} submit -d "Jenkins merge ${stream_srcStream} to ${stream_dstStream}" |p4 -c ${stream_workspace} client -o
            """
        }
        catch(all){
            echo all
        }


	}
}

void  CopyStream(stream_workspace, stream_srcStream, stream_dstStream, stream_fold) {
	echo "=================================== CopyStream ${stream_srcStream} to ${stream_dstStream} in workspace: ${stream_workspace} at Folder: ${stream_fold}"
	ws(stream_fold) {

        try
        {
            sh """
            export P4CHARSET=utf8
            p4 -c ${stream_workspace} switch -Rn ${stream_dstStream}
            p4 -c ${stream_workspace} client -o
            p4 -c ${stream_workspace} revert -a -c default
            p4 -c ${stream_workspace} copy -c default ${stream_srcStream}/... ${stream_dstStream}/...
            p4 -c ${stream_workspace} resolve -at
            p4 -c ${stream_workspace} submit -d "Jenkins copying ${stream_srcStream} to ${stream_dstStream}" |p4 -c ${stream_workspace} client -o
            """
        }
        catch(all){
            echo all
        }




	}
}




//编译引擎,默认UE4,传参可设置UE5
void CompileEngineOn(osPlatform, engineFolder, engineVer="UE5", editor="UnrealEditor") {
	echo "=================================== Start CompileEngine at Folder: ${engineFolder}"
	ws(engineFolder) {

		switch(osPlatform) {
		case CommonConfig.OS_Windows:
			//sh './Setup.bat' //bat不需要执行. 更新版本手动去打包机上执行. 否则会出很多问题
			sh './GenerateProjectFiles.bat'
			sh "devenv.com ${engineVer}.sln /build \"Development Editor|Win64\" /Project UnrealHeaderTool"
			sh "devenv.com ${engineVer}.sln /build \"Development Editor|Win64\" /Project ${engineVer}"
			sh "Engine/Build/BatchFiles/build.bat ${editor} Win64 Development"
		break

		case CommonConfig.OS_Mac:
			sh './GenerateProjectFiles.sh'
			sh "xcodebuild ${engineVer}.xcworkspace -scheme ${engineVer}"
			sh "Engine/Build/BatchFiles/Mac/Build.sh ${editor} Mac Development"
		break

		case CommonConfig.OS_Linux:
		break

		default:
			echo "Error: input CompileEngineOn osPlatform not found " + osPlatform
		}
	}
}


//编译源码项目


void CompileGameOn(osPlatform, engineFolder, gameFolder, projName="XGame") {

	echo "=================================== Start Cimpile Game at Folder: ${gameFolder}"
	ws(gameFolder) {
		String projectPath = gameFolder + "/${projName}.uproject"
		String cmd = ""

		switch(osPlatform) {
			case CommonConfig.OS_Windows:

				sh "${engineFolder}/GenerateProjectFiles.bat -project=\"${projectPath}\" -game"

				//echo "Building BuildPatchTool"
    			//sh "devenv.com ${projName}.sln /build \"Shipping|Win64\" /Project BuildPatchTool"

				echo "Building ${projName}"
				//cmd = 'devenv.com XGame.sln /build "Development Editor|Win64" /Project XGame'
				//-Target="XGameEditor Linux Development
        		sh "${engineFolder}/Engine/Build/BatchFiles/Build.bat -Target=\"${projName}Editor Win64 Development -Project=${projectPath}\" -waitmutex -ForceHeaderGeneration -FromMsBuild"
			break

			case CommonConfig.OS_Mac:
				sh "${engineFolder}/GenerateProjectFiles.sh -project=\"${projectPath}\" -game"
        		sh "${engineFolder}/Engine/Build/BatchFiles/Mac/Build.sh -Target=\"${projName}Editor Mac Development -Project=${projectPath}\" -waitmutex -ForceHeaderGeneration -FromMsBuild"
			break

			case CommonConfig.OS_Linux:
			break
		}
	}
}

//编译Launcher项目
void CompileLauncherGameOn(osPlatform, engineFolder, gameFolder) {
	echo "=================================== Start Cimpile Game at Folder: ${gameFolder}"
	ws(gameFolder) {
		String projectName = gameFolder + "/XGame.uproject"
		String cmd = ""

		switch(osPlatform) {
			case CommonConfig.OS_Windows:
				sh """${engineFolder}/Engine/Binaries/DotNET/UnrealBuildTool.exe  -projectfiles -game -rocket -progress -project=\"${projectName}\""""

				//echo "Building BuildPatchTool"
    			//sh 'devenv.com XGame.sln /build "Shipping|Win64" /Project BuildPatchTool'

				echo "Building XGame"
				//cmd = 'devenv.com XGame.sln /build "Development Editor|Win64" /Project XGame'
				//-Target="XGameEditor Linux Development
        		sh """${engineFolder}/Engine/Build/BatchFiles/Build.bat -waitmutex -ForceHeaderGeneration -FromMsBuild -Target=\"XGameEditor Win64 Development -Project=${projectName}\""""
			break

			case CommonConfig.OS_Mac:
			break

			case CommonConfig.OS_Linux:
			break
		}
	}
}


void UploadFolderToFtp(String localFolder, String targetPlatform) {
	def time = new Date().format('yyyyMMdd_HHmmss')
	String ftpFolder = "Package/" + targetPlatform + "/ProjectX_Dev_" + time  + "/"
	echo "Uploading Ftp: " + ftpFolder + " from " + localFolder
	try {
		sh "python ./third_tool/PyFtp.py ${localFolder} ${ftpFolder}"
	} catch (any) {
		error "Upload Ftp failed: " + ftpFolder + " " + localFolder
	}
}


void UploadArtTestFolderToFtp(String localFolder, String targetPlatform) {
	def time = new Date().format('yyyyMMdd_HHmmss')
	String ftpFolder = "Art/" + targetPlatform + "/ProjectX_Dev_" + time  + "/"
	echo "Uploading Ftp: " + ftpFolder + " from " + localFolder
	try {
		sh "python ./third_tool/PyFtp.py ${localFolder} ${ftpFolder}"
	} catch (any) {
		error "Upload Ftp failed: " + ftpFolder + " " + localFolder
	}
}

void UploadLinuxServerFolderToFtp(String localFolder, String targetPlatform) {
	// def time = new Date().format('yyyyMMdd_HHmmss')
	// String ftpFolder = "server/" + targetPlatform + "/ProjectX_Dev_" + time  + "/"
	// echo "Uploading Ftp: " + ftpFolder + " from " + localFolder
	// try {
	// 	sh "python ./third_tool/PyLinuxFtp.py ${localFolder} ${ftpFolder}"
	// } catch (any) {
	// 	error "Upload Ftp failed: " + ftpFolder + " " + localFolder
	// }

	String ftpFolder = "server/LinuxServer/"
	echo "Uploading Ftp: " + ftpFolder + " from " + localFolder
	try {
		sh "python ./third_tool/PyLinuxFtp.py ${localFolder} ${ftpFolder}"
	} catch (any) {
		error "Upload Ftp failed: " + ftpFolder + " " + localFolder
	}
}


void UploadToSvn(String localFolder, String svnRoot, String osPlatform,boolean WindowsServer = true,boolean Windows = true,boolean LinuxServer = true){
	switch(osPlatform) {
		case CommonConfig.OS_Windows:
			//删除svn的内容
			String svnFolder = "${svnRoot}/win64"
			

			dir(svnRoot){
				sh 'svn cleanup --username caoyong --password caoyong_z6fd'
				sh 'svn update . --username caoyong --password caoyong_z6fd'
			}

			//拷贝打包内容到svn目录
			// sh "mkdir ${svnFolder}"

			if(WindowsServer)
			{
				echo "delete svn content ${svnFolder}/WindowsServer ..."
				dir("${svnFolder}/WindowsServer"){deleteDir()}

				echo "copy server package content to svn folder"
				sh "cp -r ${localFolder}/WindowsServer/ ${svnFolder}/"
			}
			if(Windows)
			{
				echo "delete svn content ${svnFolder}/Windows ..."
				dir("${svnFolder}/Windows"){deleteDir()}

				echo "copy client package content to svn folder"
				sh "cp -r ${localFolder}/Windows/ ${svnFolder}/"
			}
			if(LinuxServer)
			{
				echo "delete svn content ${svnFolder}/LinuxServer ..."
				dir("${svnFolder}/LinuxServer"){deleteDir()}
				
				echo "copy linuxserver package content to svn folder"
				sh "cp -r ${localFolder}/LinuxServer/ ${svnFolder}/"
			}
			
			
		break
		
		case CommonConfig.OS_Mac:
		break

		case CommonConfig.OS_Linux:
		break
	}


	dir(svnRoot){
		sh 'svn add . --force --no-ignore'
		echo ' finish . files. start to commit '
		sh 'svn commit -m "编译机提交打包内容" ./'
	}
	// try {
	// 	sh "python ./third_tool/SvnCommitDeleteFiles.py ${svnRoot}"
	// } catch (any) {
	// 	error "SvnCommitDeleteFiles failed: " + svnRoot
	// }

}


void ChangeConfig(String configPath, String changeKey,String changeValue) {
	try {
		sh "python ./third_tool/PyChangeConfig.py ${configPath} ${changeKey} ${changeValue}"
	} catch (any) {
		error "ChangeConfig failed: " + configPath + " " + changeKey
	}
}