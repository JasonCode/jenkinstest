@Library("PLPipelineLibrary") _

pipeline {
	//options {
		//timeout(time: 30, unit: 'SECONDS')
	//}

	parameters{
		choice(choices: ['master', 'WinBuild01'], description: '选择打包机', name: 'NODE')

		booleanParam(defaultValue: false, description: '使用Pak?', name: 'bPak')
		booleanParam(defaultValue: true, description: '是否打包服务器', name: 'bPackageServer')
		booleanParam(defaultValue: true, description: '是否包含Linux服务器', name: 'bIncludeLinux')

		string(name:'PackageLabel', defaultValue:'', description:'--------------------------------------------------------------------------------------------------------------')
		booleanParam(defaultValue: true, description: '更新引擎?', name: 'bUpdateEngine')
		booleanParam(defaultValue: true, description: '编译引擎?', name: 'bCompileEngine')
		booleanParam(defaultValue: true, description: '更新游戏?', name: 'bUpdateGame')
		booleanParam(defaultValue: true, description: '编译游戏?', name: 'bCompileGame')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package阶段?', name: 'bPackage')
		booleanParam(defaultValue: true, description: '上传Svn?', name: 'bUploadSvn')
		booleanParam(defaultValue: false, description: '上传FTP?', name: 'bUploadFtp')
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		EngineFolder = 'C:/UnrealEngine'
		GameFolder = 'C:/ProjectX_Linux_Server'
		EngineWorkSpace = 'XBuild01_UE'
		GameWorkSpace = 'XBuild01_ProjectX_Linux_Server'

		DeployFolder = 'C:/PLDeploy'
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
					packageUtil.Reset()
					packageUtil.SetGameFolder(GameFolder)
					packageUtil.SetEngineFolder(EngineFolder)
					packageUtil.SetTargetPlatform(CommonConfig.Plat_Win64)

					if (params.bIncludeLinux){
						packageUtil.SetServerTargetPlatform("${CommonConfig.Plat_Linux}+${CommonConfig.Plat_Win64}")
					}else{
						packageUtil.SetServerTargetPlatform(CommonConfig.Plat_Win64)
					}
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetPak(params.bPak)
					packageUtil.SetPackageServer(params.bPackageServer)
					packageUtil.SetAchive(false)
					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
					if(params.bUpdateEngine)
						commonUtil.SyncWorkspace(EngineWorkSpace, EngineFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompileEngine)
						commonUtil.CompileEngineOn(CommonConfig.OS_Windows, EngineFolder)
				}
			}
		}

		stage("Checkout Game"){
			steps{
				script{
					if(params.bUpdateGame)
						commonUtil.SyncWorkspace(GameWorkSpace, GameFolder)
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					if(params.bCompileGame)
						commonUtil.CompileGameOn(CommonConfig.OS_Windows, EngineFolder, GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackage)packageUtil.StartPackage()
				}
			}
		}

		

		stage("UploadLinuxFTP"){
			steps{
				script{
					if(params.bIncludeLinux)
						commonUtil.UploadLinuxServerFolderToFtp(packageUtil.GetPackagedPath() + "/LinuxServer", packageUtil.GetServerTargetPlatform())
				}
			}
		}

	
	}

	post{
		//success {script {messageUtil.SendWXMsg("打包", "main", "Success")}}
		failure {script {messageUtil.SendWXMsg("打包", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("打包", "main", "Unstable")}
		}
	}

//--------- end of pipeline	
}
