def VersionBit

pipeline{
    parameters{
        choice(choices: ['JXPortal'], description: '打包机', name: 'NODE')
    }
    agent{
        node{
            label "${NODE}"
        }
        
    }
    options{
        withCredentials([
            usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS'),
            usernamePassword(credentialsId: '092907a7-dcbf-425b-bc2d-c61715b73536', usernameVariable: 'SVN_CREDENTIALS_USR', passwordVariable: 'SVN_CREDENTIALS_PSW')
        ])
    }



    stages{
        stage('prepare'){
            steps {
                    
                script{
                    //第一次运行下载文件
                    if (!fileExists("${env.WORKSPACE}/SwordGame")){
                    //只下载单个文件
                        dir("${env.WORKSPACE}"){
                            sh """
                                git clone -n http://${GIT_CREDENTIALS}@git.jx2.bjxsj.site/pirate/SwordGame.git
                                cd SwordGame
                                git config core.sparsecheckout true
                                echo /Scripts/Common/ProtocolExport.json >> .git/info/sparse-checkout
                                git checkout master
                            """
                        }
                    }
                }
            }
        }
        stage('diff'){
            steps{
                script{
                    dir("${env.WORKSPACE}/SwordGame"){
                        sh "git fetch"
                        FileDiff = sh(returnStdout: true, script: "git diff --stat origin/master -- Scripts/Common/ProtocolExport.json")
                        echo FileDiff
                        if (FileDiff != ""){
                            VersionBit = "3"
                        }else{
                            VersionBit = "4"
                        }
                    }
                }
            }
        }
        stage('Uploadgit'){
            steps{
                script{
                    dir("${env.WORKSPACE}/SwordGame"){
                        sh """                        
                            git fetch --all
                            git reset --hard origin/master
                            git pull
                        """
                    }
                }
            }
        }
        stage('UpdateVersion'){
            steps{
               script{
                    def COMMON_DIR= "D:/UpdateProjectVersion"
                    dir ("${COMMON_DIR}"){
                        sh "svn cleanup "
                        sh "svn revert -R ."
                        sh "svn update --non-interactive --no-auth-cache --username '${SVN_CREDENTIALS_USR}' --password '${SVN_CREDENTIALS_PSW}'"
                    }
                    bat "D:/Tools/VersionChanger.exe ${COMMON_DIR}/CommonConfig.lua ${VersionBit} ${COMMON_DIR}/ShortVersion.txt ${COMMON_DIR}/LongVersion.txt"
                    LongVersion = readFile("${COMMON_DIR}/LongVersion.txt")
                    dir ("${COMMON_DIR}"){
                        sh "svn commit -m \"Update Project Version to ${LongVersion}\" * --username ${env.SVN_CREDENTIALS_USR} --password ${env.SVN_CREDENTIALS_PSW}"
                    }
               }
            }
        }
    }
} 