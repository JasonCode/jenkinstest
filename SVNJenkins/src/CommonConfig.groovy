import groovy.transform.Field
import java.text.*
import java.util.Date
import groovy.json.JsonOutput

class CommonConfig{
	static String OS_Windows = "Windows"
	static String OS_Mac = "Mac"
	static String OS_Linux = "Linux"

	static String Plat_Win64 = "Win64"
	static String Plat_Android = "Android"
	static String Plat_iOS = "iOS"
	static String Plat_Linux = "Linux" //linux服务器

	static String UE4_Cmd="UE4Editor-Cmd.exe"
	static String UE5_Cmd="UnrealEditor-Cmd.exe"
}

