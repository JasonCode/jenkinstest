@Library("packageLib") _

def GNewVersionNumber

pipeline {
    parameters{
        choice(choices: ['Freya'], description: '选择Asgard中需要发布的工程', name: 'ProjectName')
        choice(choices: ['JXPortal', 'PC_Build5'], description: '选择需要部署的服务器', name: 'DeployMachine')
        string(name:'IncrementVersionNumberIndex', defaultValue:'3', description:'版本号样式：1.3.6.7（Major.Minor.Revision.Build），升级采取指定位加1，后置位清零的规则。例如【IncrementVersionNumberIndex】为1，则版本升级为2.0.0.0；再如【IncrementVersionNumberIndex】为4，则版本升级为1.3.6.8。')
    }

    agent {
        node {
            label "${DeployMachine}"
        }
    }

    // options{
    //     withCredentials([
    //         usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS')
    //     ])
    // }
    environment {
        P4PORT = "tcp:perforce.rpg.bjxsj.site:1666"
        P4USER = "liuzhibin"
        P4PASSWD = "abc123456"
    }

    stages{
        

        stage("Download Git"){
            steps{
                dir("InternalProject")
                {
                    script{
                        buildName params.ProjectName + "@" + params.DeployMachine + "#${BUILD_NUMBER}"

                        p4sync charset: 'utf8', credential: '41907ee9-f707-41a4-aa03-b57c85feee36', format: "${DeployMachine}_Asgard", populate: syncOnly(force: true, have: true, modtime: false, parallel: [enable: false, minbytes: '1024', minfiles: '1', threads: '4'], pin: '', quiet: true, revert: true), source: streamSource("//Asgard/main")
                    }
                }
            }
        }

        stage("Nuget Restore"){
            steps{
                dir("${WORKSPACE}/InternalProject")
                {
                    powershell '''
                    chcp 850
                    ../3rdpartytools/nuget.exe restore Asgard.sln
                    '''
                }
            }
        }

        stage("Update Version Number"){
            steps{
                script{
                    echo "Update version number at ${IncrementVersionNumberIndex}"
                    
                    dir("${WORKSPACE}/InternalProject/${ProjectName}")
                    {
                        sh "p4 -c ${DeployMachine}_Asgard edit ${ProjectName}.csproj"
                        def UpdateVersionNumber = powershell returnStdout: true, script: """msbuild ${ProjectName}.csproj /t:UpdateVersionNumber /p:UpdateNumberIndex=${IncrementVersionNumberIndex}""", encoding: 'UTF-8'
                        println UpdateVersionNumber
                        x = /New Build Version:\s(\d+\.\d+\.\d+\.\d+)/
                        def matcher = UpdateVersionNumber =~x
                        if(matcher){
                            GNewVersionNumber = matcher[0][1]
                            println GNewVersionNumber
                        }
                    }
                }
            }
        }

        stage("PublishProject"){
            steps{
                dir("${WORKSPACE}/InternalProject/${ProjectName}")
                {
                    powershell(
                        script:"""
                            msbuild ${ProjectName}.csproj /t:publish /P:Configuration=Release /P:Platform=x64 /p:PublishDir=\"${WORKSPACE}/${ProjectName}/\"
                        """,
                        encoding: 'UTF-8'
                    )
                }
            }
        }

        stage("UploadFtp"){
            steps{
                dir("${WORKSPACE}")
                {
                    script {
                        packageLib.PutToFtp('AsgardFTP', 'Asgard', "${ProjectName}/**", false, false)
                    }
                }
            }
        }

        stage("CommitVersion"){
            steps{
                script {
                    dir("${WORKSPACE}/InternalProject"){
                        sh "p4 -c ${DeployMachine}_Asgard submit -d \"Asgard Auto Publish :${ProjectName} ${GNewVersionNumber}\" "
                        // p4publish credential: '41907ee9-f707-41a4-aa03-b57c85feee36', publish: submit(delete: false, description: "Asgard Auto Publish :${ProjectName} ${GNewVersionNumber}", modtime: false, onlyOnSuccess: true, paths: '', purge: '', reopen: false), workspace: staticSpec(charset: 'utf8', name: "${DeployMachine}_Asgard", pinHost: false)
                    }
                }
            }
        }
    }

    post{
        success {
            script {
                echo "---------------------- finish by success ---------------"
                String title = "Asgard 程序发布"
                String content = "${ProjectName} ${GNewVersionNumber}\\n发布结果：成功\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>\\n<a href=\\\"http://asgard.rpg.bjxsj.site/Asgard/${ProjectName}/publish.htm\\\">安装连接</a>"
                packageLib.SendMessageWithLark(content,title, env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
        failure {
            script {
                echo "---------------------- finish by failure ---------------"
                String title = "Asgard 程序发布"
                String content = "${ProjectName} ${GNewVersionNumber}\\n发布结果：失败\\n<a href=\\\"${env.BUILD_URL}/console\\\">发布log连接</a>\\n"
                packageLib.SendMessageWithLark(content,title, env.LARK_CUSTOMBOT_URL_OLYMPUS)
            }
        }
    }
}