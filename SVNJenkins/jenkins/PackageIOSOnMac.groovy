@Library("PLPipelineLibrary") _


def String GetEngineFolder() 
{
	if(params.buildBranch == "dev_stable")
		return '/Users/mac/Documents/XBuild01_mac_dev_stable'
	else if(params.buildBranch == "dev_ue5cb2")
		return '/Users/mac/Documents/XBuild01_Mac_UE5_CB2'
}

def String GetGameFolder() 
{
	if(params.buildBranch == "dev_stable")
		return '/Users/mac/Documents/XBuild01_mac_dev_stable/ProjectX'
	else if(params.buildBranch == "dev_ue5cb2")
		return '/Users/mac/Documents/XBuild01_Mac_UE5_CB2/ProjectX'
}

def String GetGameWorkSpace() 
{
	if(params.buildBranch == "dev_stable")
		return 'XBuild01_mac_dev_stable'
	else if(params.buildBranch == "dev_ue5cb2")
		return 'XBuild01_Mac_UE5_CB2'

}

pipeline {
	//options {
		//timeout(time: 30, unit: 'SECONDS')
	//}

	parameters{
		choice(choices: ['Mac01'], description: '选择打包机', name: 'NODE')

		string(name:'PackageLabel', defaultValue:'', description:'--------------------------------------------------------------------------------------------------------------')
		choice(choices: ['dev_stable','dev_ue5cb2'], description: '选择分支目录', name: 'buildBranch')
		booleanParam(defaultValue: true, description: '更新?', name: 'bUpdate')
		booleanParam(defaultValue: false, description: '编译引擎?', name: 'bCompileEngine')
		// booleanParam(defaultValue: false, description: '更新游戏?', name: 'bUpdateGame')
		booleanParam(defaultValue: true, description: '编译游戏?', name: 'bCompileGame')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package 阶段?', name: 'bPackage')
		booleanParam(defaultValue: true, description: '拷贝到share目录?', name: 'bCopyToShare')
		booleanParam(defaultValue: true, description: '打包集合导出？', name: 'bArchive')
		string(defaultValue: '/Users/mac/Documents/archive/', description: '打包集合导出路径', name: 'bArchiveFolder')
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		EngineFolder = GetEngineFolder()
		GameFolder = GetGameFolder()
		EngineWorkSpace = GetGameWorkSpace()
		// GameWorkSpace = 'ProjectX_Mac'
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
					packageUtil.Reset()
					packageUtil.SetGameFolder(GameFolder)
					packageUtil.SetEngineFolder(EngineFolder)

					packageUtil.SetTargetPlatform(CommonConfig.Plat_iOS)
					packageUtil.SetOSPlatform(CommonConfig.OS_Mac)
					packageUtil.SetPak(true)
					packageUtil.SetPackageServer(false)

					packageUtil.SetAchive(params.bArchive)
					packageUtil.SetAchiveFolder(params.bArchiveFolder)

					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
					if(params.bUpdate)
						commonUtil.SyncWorkspace(EngineWorkSpace, EngineFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompileEngine)
						commonUtil.CompileEngineOn(CommonConfig.OS_Mac, EngineFolder)
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					if(params.bCompileGame)
						commonUtil.CompileGameOn(CommonConfig.OS_Mac, EngineFolder, GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackage)packageUtil.StartPackage()
				}
			}
		}

		stage("CopyToShare"){
			steps{
				script{
					if(params.bCopyToShare)
					{
						def fromPath = packageUtil.GetPackagedPath()
						echo "${fromPath}"
						def toPath = '/Users/mac/Documents/Share_1234/package/'
						sh "cp -rf ${fromPath}* ${toPath}"
					}
						
				}
			}
		}

	}

	// post{
	// 	//success {script {messageUtil.SendWXMsg("Android打包", "main", "Success")}}
	// 	failure {script {messageUtil.SendWXMsg("ios打包", "main", "Failure")}}
	// 	unstable {script {messageUtil.SendWXMsg("ios打包", "main", "Unstable")}
	// 	}
	// }

//--------- end of pipeline	
}
