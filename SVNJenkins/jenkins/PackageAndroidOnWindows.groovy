@Library("PLPipelineLibrary") _


def String GetEngineFolder() 
{
	if(params.buildBranch == "stable")
		return 'C:/XBuild01_ProjectX_stable'
	else if(params.buildBranch == "dev")
		return 'C:/XBuild01_ProjectX_Dev'
}

def String GetGameFolder() 
{
	if(params.buildBranch == "stable")
		return 'C:/XBuild01_ProjectX_stable/ProjectX'
	else if(params.buildBranch == "dev")
		return 'C:/XBuild01_ProjectX_Dev/ProjectX'
}

def String GetGameWorkSpace() 
{
	if(params.buildBranch == "stable")
		return 'XBuild01_ProjectX_stable'
	else if(params.buildBranch == "dev")
		return 'XBuild01_ProjectX_Dev'

}

pipeline {
	//options {
		//timeout(time: 30, unit: 'SECONDS')
	//}

	parameters{
		choice(choices: ['WinBuild-11.152','WinBuild-13.37'], description: '选择打包机', name: 'NODE')

		booleanParam(defaultValue: true, description: '是否同步引擎的本地文件（默认勾选，取消勾选可用于快速打包）', name: 'bUpdate')
		choice(choices: ['dev','stable'], description: '选择分支目录', name: 'buildBranch')
		booleanParam(defaultValue: false, description: '编译引擎?', name: 'bCompileEngine')
		booleanParam(defaultValue: true, description: '编译游戏?', name: 'bCompileGame')
		booleanParam(defaultValue: true, description: 'Cook阶段?', name: 'bCook')
		booleanParam(defaultValue: true, description: 'Package Android阶段?', name: 'bPackageAndroid')
		booleanParam(defaultValue: true, description: '上传FTP?', name: 'bUploadFtp')
		// booleanParam(defaultValue: true, description: '打包集合导出？', name: 'bArchive')
		// string(defaultValue: "C:/packageArchive", description: '打包集合导出路径', name: 'ArchiveFolder')
	}

	agent{
		node{
			label "${params.NODE}"
		}

	}

	environment{
		P4PORT = "tcp:10.0.201.12:1666"
		P4USER = "XBuild01"
		P4PASSWD = "XBuild_01"

		EngineFolder = GetEngineFolder()
		GameFolder = GetGameFolder()
		EngineWorkSpace = GetGameWorkSpace()

		ArchiveFolder = "C:/packageArchive"
	}

	stages{
		stage("Initialize"){
			steps{
				script{
					localUtil.Reset()
					packageUtil.Reset()
					packageUtil.SetGameFolder(GameFolder)
					packageUtil.SetEngineFolder(EngineFolder)

					packageUtil.SetTargetPlatform(CommonConfig.Plat_Android)
					packageUtil.SetOSPlatform(CommonConfig.OS_Windows)
					packageUtil.SetPak(true)
					packageUtil.SetPackageServer(false)

					packageUtil.SetAchive(true)
					packageUtil.SetAchiveFolder(ArchiveFolder)

					packageUtil.OnInitialize()
				}
			}
		}

		stage("Checkout Engine"){
			steps{
				script{
				    sh(script: "p4 set P4PORT=${P4PORT}", returnStdout: true)
                    sh(script: "p4 set P4USER=${P4USER}", returnStdout: true)
                    sh(script: "p4 set P4PASSWD=${P4PASSWD}", returnStdout: true)
                    sh(script: "p4 set P4CLIENT=${EngineWorkSpace}", returnStdout: true)
                    sh(script: "cd ${EngineFolder}", returnStdout: true)
				    result = sh returnStdout: true ,script: "p4 changes -m 1 ${EngineFolder}/..."
                    result = result.trim()
                    echo "=================================== last submit changelist info :${result}"
					if(params.bUpdate)

						commonUtil.SyncWorkspace(EngineWorkSpace, EngineFolder)
				}
			}
		}

		stage("Compile Engine"){
			steps{
				script{
					if(params.bCompileEngine)
						commonUtil.CompileEngineOn(CommonConfig.OS_Windows, EngineFolder,"UE5", "UnrealEditor")
				}
			}
		}

		stage("Compile Game"){
			steps{
				script{
					if(params.bCompileGame)
						commonUtil.CompileGameOn(CommonConfig.OS_Windows, EngineFolder, GameFolder)
				}
			}
		}

		stage("Package Pre"){
			steps{
				script{
					echo "for place hold"
				}
			}
		}

		stage("Build/Cook"){
			steps{
				script{
					if(params.bCook)packageUtil.StartBuildCook()
				}
			}
		}

		stage("Package"){
			steps{
				script{
					if(params.bPackageAndroid)packageUtil.StartPackage()
				}
			}
		}

		stage("UploadFTP"){
			steps{
				script{
					if(params.bUploadFtp)
						commonUtil.UploadFolderToFtp(packageUtil.GetPackagedPath(), packageUtil.GetTargetPlatform())
				}
			}
		}

	}

	post{
		//success {script {messageUtil.SendWXMsg("Android打包", "main", "Success")}}
		failure {script {messageUtil.SendWXMsg("Android打包", "main", "Failure")}}
		unstable {script {messageUtil.SendWXMsg("Android打包", "main", "Unstable")}
		}
	}

//--------- end of pipeline	
}
