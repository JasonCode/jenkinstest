


import os
import sys
import shutil
import webbrowser
import os
import time
os.system('net use \\\\10.0.200.5\HeroFileServer "ZQcPE3QrDCYA4aE&" /user:YXHY\JgProPack')

import subprocess
site_package = r'\\10.0.200.5\HeroFileServer\ProjectX\Scripts\Python37\Lib\site-packages'
if site_package not in sys.path:
    sys.path.append(site_package)

def copy_tree(src, dst, symlinks=False, ignore=None):
    """
    Avoid the OSError raised by makedirs cmd in shutil.copytree
    symlinks will be ignore
    """
    names = os.listdir(src)
    if ignore:
        ignored_names = ignore(src, names)
    else:
        ignored_names = []
    try:
        if not os.path.exists(dst):
            os.makedirs(dst)
    except:
        print(dst)
        print(os.path.exists(dst))
        if not os.path.exists(dst):
            os.makedirs(dst)

    for item in names:
        if item in ignored_names:
            continue
        if symlinks:
            continue
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copy_tree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def removeTree(fold):
    for root, dirs, files in os.walk(fold):
        for file in files:
            os.remove(os.path.join(root, file))

def upBinaries(proj_dir):
    server_dir = r"\\10.0.200.5\HeroFileServer\ProjectX\COOP\ProjectX_Binaries"
    local_dir = proj_dir
    old_fold = os.path.join(os.path.dirname(server_dir), os.path.basename(server_dir)+"_old")
    old_binaries = os.path.join(old_fold, "Binaries")
    plugin_binaries = os.path.join(old_fold, "Plugins", "Binaries")
    for path in os.listdir(local_dir):
        if path == 'Binaries':
            srcPath = os.path.join(local_dir, path)
            dstPath = os.path.join(server_dir, path)
            if os.path.exists(dstPath):
                if os.path.exists(old_binaries):
                    try:
                        shutil.rmtree(old_binaries)
                    except:
                        removeTree(old_binaries)


                time.sleep(1)
                copy_tree(dstPath, old_binaries)
                print(old_binaries)
                time.sleep(1)
                try:
                    shutil.rmtree(dstPath)
                except:
                    removeTree(dstPath)
            if os.path.exists(srcPath):
                print('%s --> %s' % (srcPath, dstPath))
                time.sleep(1)
                copy_tree(srcPath, dstPath)
        elif path == 'Plugins':
            srcPluginsPath = os.path.join(local_dir, path)
            dstPluginsPath = os.path.join(server_dir, path)
            for plugin in os.listdir(srcPluginsPath):
                srcPlugin = os.path.join(srcPluginsPath, plugin, 'Binaries')
                dstPlugin = os.path.join(dstPluginsPath, plugin, 'Binaries')
                if os.path.exists(dstPlugin):
                    if os.path.exists(plugin_binaries):
                        try:
                            shutil.rmtree(plugin_binaries)
                        except:
                            removeTree(plugin_binaries)

                    print(plugin_binaries)
                    time.sleep(1)
                    copy_tree(dstPlugin, plugin_binaries)
                    try:
                        shutil.rmtree(dstPlugin)
                    except:
                        removeTree(dstPlugin)


                print('%s --> %s' % (srcPlugin, dstPlugin))
                if os.path.exists(srcPlugin):
                    time.sleep(1)
                    copy_tree(srcPlugin, dstPlugin)

if __name__ == '__main__':
    import sys
    upBinaries(sys.argv[1])
