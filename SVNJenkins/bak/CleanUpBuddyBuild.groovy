pipeline{
    agent{
        node{
            // 用 master 启动job
            label "master"
        }
    }

    parameters{
        string(name:'timeout', defaultValue:'1', description:'')
    }

    options{
        timeout(time:"${params.timeout}", unit: 'HOURS')
    }

    stages{
        stage('prepare'){
            //所有机器并行
            parallel{
                stage('PC_Build1'){
                    agent{ 
                        node {label "PC_Build1"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("AndroidBuddyBuildPipeline")
                        CleanUpBuddyBuild("WindowsBuddyBuild")
                        CleanUpDailyBuild("AndoridPipelineDailyBuild")
                    }
                }
                stage('PC_Build2'){
                    agent{ 
                        node {label "PC_Build2"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("AndroidBuddyBuildPipeline")
                        CleanUpBuddyBuild("WindowsBuddyBuild")
                        CleanUpDailyBuild("WindowsDailyBuild")
                    }
                }
                stage('PC_Build8'){
                    agent{ 
                        node {label "PC_Build8"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("AndroidBuddyBuildPipeline")
                        CleanUpBuddyBuild("WindowsBuddyBuild")
                    }
                }
                stage('PC_Build9'){
                    agent{ 
                        node {label "PC_Build9"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("AndroidBuddyBuildPipeline")
                        CleanUpBuddyBuild("WindowsBuddyBuild")
                    }
                }
                stage('Mac_JX2Designer'){
                    agent{ 
                        node {label "Mac_JX2Designer"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("IOSBuddyBuildPipeline")
                        CleanUpDailyBuild("IOSPipelineDailybuild")
                    }
                }
                stage('Mac_JX2'){
                    agent{ 
                        node {label "Mac_JX2"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("IOSBuddyBuildPipeline")
                    }
                }
                stage('Mac_WentaoXia'){
                    agent{ 
                        node {label "Mac_WentaoXia"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("IOSBuddyBuildPipeline")
                    }
                }
                stage('Mac_JGX'){
                    agent{ 
                        node {label "Mac_JGX"}
                    }
                    steps{
                        CleanUpAppointPath()
                        CleanUpBuddyBuild("IOSBuddyBuildPipeline")
                    }
                }
                stage('Linux_FT'){
                    agent{ 
                        node {label "Linux_FT"}
                    }
                    steps{
                        CleanUpAppointPath()
                    }
                }
            }
        }
    }
}

void CleanUpBuddyBuild(String jobName){
    try{
        //进入BuddyBuild目录
        dir ("../../BuddyBuild/${jobName}"){
            // BuddyBuild 为 7 天
            CleanUp('7')
        }
    }catch(any){
        currentBuild.result = 'UNSTABLE'
        println "CleanUp warning"
    }
}

void CleanUpDailyBuild(String jobName){
    try{
        if (jobName == "WindowsDailyBuild"){
            cleanuppath = ["Releases","ChunkInstall/WindowsNoEditor","Source/CMakeModules/WindowsInstaller"]
        }else if(jobName == "AndoridPipelineDailyBuild"){
            cleanuppath = ["Releases","ChunkInstall/Android_ASTC","Binaries/Android"]
        }else{
            cleanuppath = ["Releases","ChunkInstall/IOS","Binaries/IOS"]
        }
        for (i = 0; i < cleanuppath.size(); i++){
            dir ("../../DailyBuild/${jobName}/JXa2bcb5b6/${cleanuppath[i]}"){
                // DailyBuild 为14天
                CleanUp('14')
            }
        }
    }catch(any){
        currentBuild.result = 'UNSTABLE'
        println "CleanUpDailyBuild failed"
    }
}

void CleanUpAppointPath(){
    println "CLEANUP_FOLDER is: " + env.CLEANUP_FOLDER
    if (env.CLEANUP_FOLDER == null){
        return
    }
    PathList = env.CLEANUP_FOLDER.tokenize(';')

    try {
        for (int i=0; i < PathList.size(); ++i){
            path = PathList[i]
            PathConfig = path.tokenize('|')
            
            AbsolutePath = PathConfig[0]
            DeleteTime = PathConfig[1]
            // 判断路径是否存在
            
            def res = sh(script: "test -d ${AbsolutePath} && echo '1' || echo '0' ", returnStdout: true).trim()
            
            if ( res == '0'){
                println "No such file or directory: " + AbsolutePath 
                continue
            }
            println "DeleteTime "  + DeleteTime
            if (DeleteTime == null){
                println 'Please fill in the correct time'
                continue
            }

            dir (AbsolutePath){
                CleanUp(DeleteTime)
            }
        }
    }
    catch (any){
        currentBuild.result = 'UNSTABLE'
        println "CleanUp warning"
    }
}

void CleanUp(deleteTime){
    def File = sh(returnStdout: true, script: "ls")
    if (File == ''){
        println "The directory has no files"
        return
    }
    def FileList = File.split('\n')
    int timeThreshold
    
    try{
        deleteTimeNum = deleteTime.toInteger()
        timeThreshold = deleteTimeNum * 86400
    }catch(any){
        println "Please fill in the num, Time=" + deleteTime
        return
    }

    def NowTime = sh(returnStdout: true, script: "date +%s").toInteger()
    
    //查看运行平台
    def uname = sh script: 'uname', returnStdout: true
    boolean isMacOSX = uname.startsWith("Darwin")

    for (int i=0; i < FileList.size() ; ++i){
        def FileName = FileList[i]
        def ModifyTime
        println "FileName : " + FileName
        if (FileName == ""){
            println "Directory is empty"
            break
        }
        //判断是否为mac
        if(isMacOSX){
            def FileTime = sh(returnStdout: true, script: "stat -t%s ${FileName}")
            //获取文件信息转成数组
            FileTime = FileTime.tokenize()
            ModifyTime = FileTime[9][1..10].toInteger()
        }else{
            ModifyTime = sh(returnStdout: true, script: "stat -c %Y ${FileName}").toInteger()
        }
        //对比时间大于 timeThreshold 的删除
        if ( NowTime - ModifyTime > timeThreshold){
            sh "rm -rf ${FileName}"
            println FileName + " To Delete "
        }
    }
}