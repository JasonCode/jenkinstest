import groovy.transform.Field
import java.text.*
import java.util.Date
import groovy.json.JsonOutput

String _projectName = "XGame"
String _ueExeName = "UnrealEditor-Cmd.exe"//引擎exe的全地址
String _uatExe //uat.bat的全地址
String _gameFolder  //游戏目录
String _engineFolder //引擎目录
String _achiveFolder //归档的目录
String _clientTargetPlatform = 'Win64' //Android iOS
String _serverTargetPlatform = 'Linux+Win64'
String _osPlatform = 'Windows' //Linux Mac

String _cookMapParam = "-allmaps"

boolean _packageServer = true
boolean _packageClient = true
boolean _bPak = true 
boolean _bAchive = false

void SetGameFolder(String value){ _gameFolder = value }
void SetEngineFolder(String value){ _engineFolder = value }

//chenghuanhuan, add set diff engine, diff project
void SetProjectName(String value){ _projectName = value }
void SetUeExeName(String value){ _ueExeName = value }
//chenghuanhuan, end
void SetCookMapParam(String value){_cookMapParam = value }
void SetTargetPlatform(String value){ _clientTargetPlatform = value }
void SetServerTargetPlatform(String value){ _serverTargetPlatform = value }
void SetOSPlatform(String value){ _osPlatform = value }

void SetPak(boolean value){ _bPak = value }

void SetPackageClient(boolean value){ _packageClient = value }
void SetPackageServer(boolean value){ _packageServer = value }

void SetAchive(boolean value){ _bAchive = value }
void SetAchiveFolder(String value){ _achiveFolder = value }

String GetCookMapParam(){ return _cookMapParam }
String GetTargetPlatform(){ return _clientTargetPlatform }
String GetServerTargetPlatform(){ return _serverTargetPlatform }
String GetOSPlatform(){ return _osPlatform }


void Reset(){
	_packageServer = true
	_packageClient = true
	_bPak = true
	_bAchive = true
	_clientTargetPlatform = 'Win64'
	_serverTargetPlatform = 'Linux+Win64'
	_osPlatform = 'Windows'
	_achiveFolder = 'C:/packageDefault'
	_projectName = "XGame"
	_cookMapParam = "-allmaps"
    _ueExeName = CommonConfig.UE5_Cmd//引擎exe的全地址
}

//初始化判断基本的数据合法性
void OnInitialize(){
	try{
        sh "\"C:/Windows/System32/chcp.com\" 65001"
    }catch(Exception ex) {
        echo ex.toString()
    }
	//project名称写死的
	_projectPath = _gameFolder + "/${_projectName}.uproject"
	switch(_osPlatform) {
		case CommonConfig.OS_Windows:
		_ueExe = "\"${_engineFolder}/Engine/Binaries/Win64/${_ueExeName}\""
		_uatExe = "\"${_engineFolder}/Engine/Build/BatchFiles/RunUAT.bat\""
		break
		
		case CommonConfig.OS_Mac:
		_ueExe = "\"${_engineFolder}/Engine/Binaries/Mac/UnrealEditor.app/Contents/MacOS/UnrealEditor\""
		_uatExe = "\"${_engineFolder}/Engine/Build/BatchFiles/RunUAT.sh\""
		break

		case CommonConfig.OS_Linux:
		break
		default:
		error "input osPlatform not found " + _osPlatform
		return
	}
	echo "Project name is ${_projectPath}"
	echo "UE Editor Execute is ${_ueExe}"
	echo "Uat Path is ${_uatExe}"


	//做平台打包合法性判断
	switch(_osPlatform) {
		case CommonConfig.OS_Windows://可以打win64, android客户端, win64服务器
		if (_clientTargetPlatform == CommonConfig.Plat_iOS){
			error "Windows上不能打iOS包"
			return
		}
		break

		case CommonConfig.OS_Mac: //只能打ios客户端
		_clientTargetPlatform = CommonConfig.Plat_iOS
		_packageServer = false
		break

		case CommonConfig.OS_Linux: //只能打linux服务器
		_clientTargetPlatform = CommonConfig.Plat_Linux
		_packageServer = true
		break
	}

	echo "targetPlatform is ${_clientTargetPlatform}"
	echo "osPlatform is ${_osPlatform}"
}

void StartBuildOnly(){
	echo "==================================== Start Build Only"
	ws(_gameFolder) {
		switch(_osPlatform) {
			case CommonConfig.OS_Windows:
			_BuildOnWindows()
			break
			case CommonConfig.OS_Mac:
			_BuildnMac()
			break
			case CommonConfig.OS_Linux:
			_BuildLinux()
			break
		}
	}
}

void _BuildOnWindows(){
	String uatParam = _GetBaseUATParam() + "-skipcook -skipstage -nocompileeditor -allmaps -build -compile"
	//cheng
	 println(System.getProperty("file.encoding"))
	try{
        sh "\"C:/Windows/System32/chcp.com\" 65001"
    }catch(Exception ex) {
        echo ex.toString()
    }
    //cheng
	_RunUATCmd(uatParam)
}

void _BuildnMac(){
	String uatParam = _GetBaseUATParam() + "-skipcook -skipstage -nocompileeditor -allmaps -build -compile"
	_RunUATCmd(uatParam)
}

void _BuildLinux(){

}

void StartBuildCook(){
	echo "==================================== Start Build and Cook"
	ws(_gameFolder) {
		switch(_osPlatform) {
			case CommonConfig.OS_Windows:
			_CookOnWindows()
			break
			case CommonConfig.OS_Mac:
			_CookOnMac()
			break
			case CommonConfig.OS_Linux:
			_CookOnLinux()
			break
		}
	}
}

void _CookOnWindows(){
	String uatParam = _GetBaseUATParam() + "-cook -skipstage -nocompileeditor -build -compile " + GetCookMapParam()
	switch(_clientTargetPlatform) {
        case CommonConfig.Plat_Android:
        uatParam+=" -cookflavor=ASTC"
        break
	}
	switch(_ueExeName) {//UE5 use iterativecooking will cook error files under map _Generated_fold
        case CommonConfig.UE4_Cmd:
        uatParam+=" -iterativecooking"
        break
	}
	_RunUATCmd(uatParam)
}


void _CookOnLinux(){

}

void _CookOnMac(){
	String uatParam = _GetBaseUATParam() + "-cook -skipstage -nocompileeditor -build -compile " + GetCookMapParam()
	switch(_clientTargetPlatform) {
        case CommonConfig.Plat_Android:
        uatParam+=" -cookflavor=ASTC"
        break
	}
	switch(_ueExeName) {//UE5 use iterativecooking will cook error files under map _Generated_fold
        case CommonConfig.UE4_Cmd:
        uatParam+=" -iterativecooking"
        break
	}
	_RunUATCmd(uatParam)
}

void StartPackage() {
	echo "==================================== Start Package"
	ws(_gameFolder) {
		switch(_osPlatform) {
			case CommonConfig.OS_Windows:
			_PackageOnWindows()
			break
			case CommonConfig.OS_Mac:
			_PackageOnMac()
			break
			case CommonConfig.OS_Linux:
			_PackageOnLinux()
			break
		}
	}
}

void _PackageOnWindows(){
		//-compressed  -cmdline=" -Messaging"
		//-archive -archivedirectory=${archiveFolder}
		//-addcmdline="-SessionId=7F649CC648CEE30462D705986430D4E2 -SessionOwner='zhibin.liu' -SessionName='PackageWindow' "
	String uatParam = _GetBaseUATParam() + "-nocompileeditor -skipcook -stage -package -allmaps -prereqs"
	switch(_clientTargetPlatform) {
        case CommonConfig.Plat_Android:
        uatParam+=" -cookflavor=ASTC -compressed"
        break
	}
    if (_bAchive)uatParam += " -archive -archivedirectory=${_achiveFolder}/${_clientTargetPlatform}"
	_RunUATCmd(uatParam)
}


void PackageTest(){
		//-compressed  -cmdline=" -Messaging"
		//-archive -archivedirectory=${archiveFolder}
		//-addcmdline="-SessionId=7F649CC648CEE30462D705986430D4E2 -SessionOwner='zhibin.liu' -SessionName='PackageWindow' "
	String uatParam = _GetBaseUATParam() + "-nocompileeditor -skipcook -stage -package -map=TestMap -prereqs"
	switch(_clientTargetPlatform) {
        case CommonConfig.Plat_Android:
        uatParam+=" -cookflavor=ASTC -compressed"
        break
	}
    if (_bAchive)uatParam += " -archive -archivedirectory=${_achiveFolder}/${_clientTargetPlatform}"
	_RunUATCmd(uatParam)
}

void _PackageOnLinux(){

}

void _PackageOnMac(){
	String uatParam = _GetBaseUATParam() + "-nocompileeditor -skipcook -stage -package -allmaps -prereqs"
	switch(_clientTargetPlatform) {
        case CommonConfig.Plat_Android:
        uatParam+=" -cookflavor=ASTC -compressed"
        break
	}
    if (_bAchive)uatParam += " -archive -archivedirectory=${_achiveFolder}/${_clientTargetPlatform}"
	_RunUATCmd(uatParam)
}

//获取所有打包都需要的命令
String _GetBaseUATParam(){
	String param = "-ScriptsForProject=${_projectPath} BuildCookRun -project=${_projectPath} -nop4 -unrealexe=${_ueExe} -clientconfig=Development -culture=en -utf8output"
	if (_bPak)param += " -pak"

	if(_packageClient)
	{
		param += " -platform=${_clientTargetPlatform} -targetplatform=${_clientTargetPlatform}"
	}
	else
	{
		param += " -noclient"
	}

	if (_packageServer)param += " -serverconfig=Development -server -serverplatform=${_serverTargetPlatform}"

	return param + " "
}

void _RunUATCmd(String uatParam){
	String cmd = _uatExe + " " + uatParam
	echo cmd
	
    sh "${cmd}"
}

String GetPackagedPath(){
	String path = ""
	switch(_clientTargetPlatform) {
			case CommonConfig.Plat_Win64:
			path = _gameFolder + "/Saved/StagedBuilds"
			break
			case CommonConfig.Plat_Android:
			if (_bAchive)path = "${_achiveFolder}/${_clientTargetPlatform}"
			break
			case CommonConfig.Plat_iOS:
			if (_bAchive)path = "${_achiveFolder}/IOS"
			break
			case CommonConfig.Plat_Linux:
			break
		}
	return path
}