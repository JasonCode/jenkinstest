@Library("SwordGamePipelineLibrary") _

properties([
  parameters([
    string(defaultValue: 'https://svn.shiyou.kingsoft.com/repos/SwordGame/trunk/Program/Content', description: 'Content SVN地址', name: 'SVN_PATH', trim: true),
    string(defaultValue: 'release', description: '代码分支名', name: 'BRANCH_NAME', trim: true),
    string(defaultValue: 'release', description: '引擎分支名', name: 'GIT_ENGINE_BRANCH', trim: true),
    string(defaultValue: '', description: 'UE4CommandLine参数', name: 'AdditionCommandLine', trim: true),
    string(defaultValue: '',description:'PATCH下载地址',name:'PATCH_URL',trim:true),
    string(defaultValue: '',description:'要打patch请填写版本号并勾选PATCH选项',name: 'BASEVERSION',trim: true),
    booleanParam(defaultValue: false, description: '是否打patch', name: 'PATCH'),
    booleanParam(defaultValue: false, description: '跳过整包，与PATCH选项同时勾选', name: 'SKIP_Package'),
    string(defaultValue: '',description:'强制重写版本号，留空表示不使用此功能',name:'VERSION_OVERRIDE',trim:true),  
    
    booleanParam(defaultValue: true, description: '是否编译Shipping版本 Prerelease 不用修改 使用默认值', name: 'SHIPPING'),
    booleanParam(defaultValue: true, description: '打开Lua编译 Prerelease 不用修改 使用默认值', name: 'LUA_COMPILER'),
    booleanParam(defaultValue: true, description: '打开Lua的混淆 Prerelease 不用修改 使用默认值', name: 'LUA_CONFUSION'),
    booleanParam(defaultValue: true, description: '是否上传Bugly Prerelease 不用修改 使用默认值', name: 'BUGLY_UPLOAD_SYMBOL'),
    booleanParam(defaultValue: false, description: '重新打包 Prerelease 不用修改 使用默认值', name: 'REBUILD_PACKAGE'),
    booleanParam(defaultValue: false, description: '打开MALLOC性能调试器 Prerelease 不用修改 使用默认值', name: 'MALLOC_PROFILER'),
    booleanParam(defaultValue: false, description: '是否跳过更新引擎 Prerelease 不用修改 使用默认值', name: 'SKIP_UPDATE_ENGINE'),
    booleanParam(defaultValue: false, description: '是否跳过更新SWORDGAME Prerelease 不用修改 使用默认值', name: 'SKIP_UPDATE_SWORDGAME'),
    booleanParam(defaultValue: false, description: '是否跳过更新CONTENT Prerelease 不用修改 使用默认值', name: 'SKIP_UPDATE_CONTENT'),
    booleanParam(defaultValue: false, description: '是否跳过编译 NoahSDK, 引擎，SwordGame Prerelease 不用修改 使用默认值', name: 'SKIP_BUILD_CODE'),
    booleanParam(defaultValue: false, description: '是否跳过导表 Prerelease 不用修改 使用默认值', name: 'SKIP_EXPORT_TABLES'),
    booleanParam(defaultValue: false, description: '是否跳过cook Prerelease 不用修改 使用默认值', name: 'SKIP_COOK'),
    booleanParam(defaultValue: false, description: '在SHIPPING中打开log Prerelease 不用修改 使用默认值', name: 'LOG_INSHIPPING'),
    booleanParam(defaultValue: false, description: '在SHIPPING打开check Prerelease 不用修改 使用默认值', name: 'CHECK_INSHIPPING'),
    booleanParam(defaultValue: true, description: '是否跳过上传Tako Prerelease 不用修改 使用默认值', name: 'SKIP_USE_TAKO'),
    booleanParam(defaultValue: true, description: '是否上传包信息到Metabase Prerelease 不用修改 使用默认值', name: 'SKIP_UPLOADINFO'),
    booleanParam(defaultValue: false, description: '是否上传FTP Prerelease 不用修改 使用默认值', name: 'SKIP_UPLOADFTP'),
    booleanParam(defaultValue: false, description: '是否上传珠海FTP Prerelease 不用修改 使用默认值', name: 'UPLOADZHUHAIFTP'),
    booleanParam(defaultValue: false, description: '是否分包 Prerelease 不用修改 使用默认值', name: 'ADDRESOURCE_PACKAGE'),
    booleanParam(defaultValue: false, description: '是否使用Git公用库', name: 'FAST_GIT_CLONE'),
    booleanParam(defaultValue: false, description: '是否使用SVN公用库，在北京可以避免使用SVN镜像', name: 'FAST_SVN_CHECKOUT'),
    booleanParam(defaultValue: false, description: '使用Debug编译（调试用）Prerelease 不用修改 使用默认值', name: 'FORCEDEBUG'),
    booleanParam(defaultValue: true, description: '将热更资源和分包资源上传到腾讯云CDN 不用修改 使用默认值', name: 'UPLOAD_TENCENT_CLOUD'),
    booleanParam(defaultValue: false, description: '使用 WORK_NAME 作为腾讯云CDN部署时使用的额外路径，禁止使用特殊字符和中文', name: 'USE_EXTRA_CLOUD_PATH'),

    string(defaultValue: 'PreReleaseVersion', description: '上传FTP地址', name: 'FTP_PATH', trim: true),
    string(defaultValue: 'PreReleaseVersion',description:'上传symbol地址',name:'FTP_SYM'),
    string(defaultValue: '', description: '邮箱地址', name: 'MAIL_NAME', trim: true),
    string(defaultValue: '',description:'UBT额外参数',name:'ADDITIONAL_UBT_PARAM',trim:true),
    string(defaultValue: '',description:'UAT额外参数',name:'ADDITIONAL_UAT_PARAM',trim:true),
    string(defaultValue: 'PreRelease', description: 'Prerelease 不用修改 使用默认值', name: 'WORK_NAME', trim: true),
    choice(choices: ['PC_Build8', 'PC_Build2', 'PC_Build1', 'PC_Build9'], description: '打包机器，默认为build8', name: 'NODE'),
    choice(choices: ['ShenZhenFTP', 'WuHanFTP', 'BeiJingFTP'], description: '默认上传深圳', name: 'FTP'),
    choice(choices: ['PreReleaseBuild'], description: '版本类型 Prerelease 不用修改 使用默认值', name: 'BuildType'),
    choice(choices: ['Tencent','eFun','None'], description: '运营商', name: 'PUBLISHER'),
    
    string(defaultValue: 'https://svn.shiyou.kingsoft.com/repos/SwordGame/trunk/User/Lidi/PrereleaseBackup', description: 'Prerelease 备份svn地址', name: 'ReleasesSVN_Path', trim: true),

    ])
  ])

node("${params.NODE}") {

  withCredentials([
    usernamePassword(credentialsId: '092907a7-dcbf-425b-bc2d-c61715b73536', usernameVariable: 'SVN_CREDENTIALS_USR', passwordVariable: 'SVN_CREDENTIALS_PSW'),
    usernameColonPassword(credentialsId: 'bc1caeab-2ec5-46c7-b075-63a42124c645', variable: 'GIT_CREDENTIALS'),
    usernameColonPassword(credentialsId: 'c3ea3a70-d005-457a-acc1-80577336950c', variable: 'ZHUHAIFTP_CREDENTIALS'),
    usernameColonPassword(credentialsId: '3a51894f-d6f5-43a6-85be-e23daacc197b', variable: 'FTP_CREDENTIALS'),
    usernamePassword(credentialsId: 'c75afb1a-ba26-4649-98ba-158f3b28eb18', , usernameVariable: 'GITHUB_CREDENTIALS_USR', passwordVariable: 'GITHUB_CREDENTIALS_PSW')]) {

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    try{
      stage('Preparing Environment') {
        packageLib.PrepareEnv(params, packageLib.@PLATFORM_WINDOWS)
      }

      withEnv ([
          "SWORDGAME_HOME=${packageLib.SwordGameAbsPath}",
          "THIRDPARTY_PATH=${packageLib.SwordGameAbsPath}/Source/ThirdParty",
          "CUSTOM_CMAKE_MODULE=${packageLib.SwordGameCMakeModuleAbsPath}",
          "UE-CmdLineArgs=-BuildMachine -stdout"]){

            // 设置初始值
            currentBuild.result = 'SUCCESS'

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Update Engine') {
          packageLib.UpdateEngine()
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Update SwordGame') {
          packageLib.UpdateSwordGame()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Update Content') {

          packageLib.UpdateContent()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Build NoahSDK') {
          packageLib.BuildNoahSDK()
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Build SwordGame') {
          packageLib.BuildSwordGame()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Export DataTable & Maps') {
          packageLib.ExportDTandMaps()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Package') {
          packageLib.PreparePackage()
          packageLib.PackagePatch()
          packageLib.Package()
          packageLib.CleanupPackage()

        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('UploadInfo') {
          packageLib.UploadInfo()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Make Windows Installer') {
          packageLib.MakeWinInstaller()
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('UploadPackageInfo'){
          packageLib.UploadPackageInfo()
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        stage('Upload To FTP') {
          packageLib.UploadToFTP()
        }

        stage('Upload Releases'){
          packageLib.UpLoadReleases()
        }

        stage('Cleanup'){
          packageLib.Cleanup()
        }
      } 
    } catch (any) {
      
      currentBuild.result = packageLib.GetBuildResultValue(any)
      println currentBuild.result
      throw any

    } finally {
      script {
        packageLib.SendBuildNotify(currentBuild.result, true, true)
      }
    }
  } 
}
